import glob
import os, sys, shutil, zipfile

exit_value = os.system("pyinstaller_buildexe.bat")
if exit_value != 0: sys.exit(exit_value)

yn = input("Does the game appear to work?")
if yn.lower()[0] != 'y': sys.exit(1)

#It'd be better to just get it out of main.py, but that can't be done right now because of circular dependency issues
version = input("Enter game version number: ")

#Build windows archive
win_archive_name = f"INJECTION {version}"

os.rename("./dist", win_archive_name)
win_archive_path = shutil.make_archive(win_archive_name, "zip", win_archive_name)
os.rename("./" + win_archive_name, "dist")

print(f"Created {win_archive_path}")

#Build linux/source code archive
src_archive_name = f"injection_{version.replace('.', '_')}_src"

f = lambda name: (name, f"{src_archive_name}/{name}")

required_files = [
    "main.py",
    "gameobjs.py",
    "levels.py",
    "setup.py",
    "README.txt",
    "LICENSE",
    "fx.pyx",
    "FSEX300.ttf"
] + list(glob.glob("music\\*.ogg"))

with zipfile.ZipFile(src_archive_name+".zip", "w", zipfile.ZIP_LZMA) as src_archive:
    src_archive_path = src_archive.filename
    for the_fname in required_files:
        src_archive.write(the_fname, f"{src_archive_name}/{the_fname}")

os.system(f"butler\\butler.exe push \"{win_archive_path}\" toastengineer/INJECTION:windows-exe --userversion {version}")
os.system(f"butler\\butler.exe push {src_archive_path} toastengineer/INJECTION:linux-src --userversion {version}")