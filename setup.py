from distutils.core import setup
from Cython.Build import cythonize
import numpy

setup(options={'build_ext': {},
      "compiler_directives":{"language_level": "3"}},
      ext_modules=cythonize("fx.pyx"),
	  include_dirs=[numpy.get_include()])