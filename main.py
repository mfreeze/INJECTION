"""
INJECTION
By Schilcote
For Pyweek 19
Main Program Source File
Monday, Oct. 6, 2014

Please don't take any of this as an example of how to write good code. It's a mess of ugly hacks and last-minute
fixes, based on broken libraries and whose core was written in 6 hours, then barely touched in the five years since.
The framerate is coupled to the game update rate - never do that. Timings are all based on literals themselves
based on an assumed 15-fps frame rate - never do ANY of those things.

And finally, most importantly, in the future, when you're about to base your entire project on a library that'd
take as long to rewrite yourself as it would to finish the project... make sure it isn't complete garbage first.


The MIT License (MIT)

Copyright (c) 2015 Schilcote

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
#mingw_setup_args={'options': {'build_ext': {'compiler': 'mingw32'}}}
import pathlib
import uuid
import glob
import shutil
import datetime
import io
import os
import socket
import sys
import time
import traceback
import warnings
import zipfile
import atexit
import functools
import json
import random
import re
import threading
import _thread
import collections

import pyconsolegraphics as pcg
from pyconsolegraphics.ezmode import EZMode
from pyconsolegraphics import ui
import pyximport
pyximport.install(language_level=3)

try:
    import dill as pickle
    import dill #we want to have it as pickle but also as dill
except ImportError:
    import pickle
    dill=None


import rsa
import pymsgbox

os.environ["PYGAME_FREETYPE"]=""
os.environ["PYGAME_BACKEND"] = "directx"
os.environ['PYGAME_HIDE_SUPPORT_PROMPT'] = "hide"

import pygame
import pygame.locals as pl

import levels

import fx #This is the compiled 'fx.pyx/fx.pyd' module that comes with INJECTION - NOT THE ONE THAT IS ON PYPI!!! Try python setup.py build_ext and if that doesn't work, ask for help on reddit.com/r/INJECTION

import pyerrorreport

#Our actual version number for autoupdate purposes will be prefixed with our platform and/or freeze status
_version="0.10.2"
#Set this to true before distributing. Disables auto-updates if false.
_distributable=False
#Some of the module vars below this line can be altered from command line args
#This can be set to true by the arg parser to disable automatic updates.
_disable_update=False
#This is the address/URL of the update server
_update_addr="iju.404.mn"
pyerrorreport.server_address = _update_addr
#This is the port number the update system works on
_update_port=7363
#this is the port number the reporting system works on
_telemetry_port=1235
pyerrorreport.server_port = _telemetry_port
#This is the font the game will use for rendering; should be a Pygame-compatible, unicode-supporting font.
_font_path="FSEX300.ttf"
#Can you guess what this one does?
_music_disabled=False
#Makes the dialogue black on white instead of character's color on whatever.
_high_contrast=False
#If this is true, GameState.save_and_load() sends a short description of the game so far to _update_addr:_telemetry_port in plaintext
#No personally identifying info is included, of course - I don't even log your IP.
_telemetry=True
#Different OSes want you to save in different places. And sometimes you just have to dump it in the program directory and hope.
#This is overridden by the -save_directory switch.
#We str() the result of os.getenv 'cos it'll return None on non-Windows
_savedirs={"win32": os.path.join(str(os.getenv('APPDATA')),"INJECTION"),
           "linux": os.path.join(os.path.expanduser("~"),".local","share","INJECTION"),
           "darwin": os.path.join(os.path.expanduser("~"),"Library","Application Support","INJECTION"),
           "default": ""}
_extensions_disabled=False
_autoupdate_public_key=rsa.PublicKey.load_pkcs1(b"""
-----BEGIN RSA PUBLIC KEY-----
MIICCAKCAgEAoC8a8+BpNr/sv8r7Yf5+dVbyH1GUoAUHJf6ppbgCbDDVSDHj3fyW
hhpcKk62UsydE5vKsyGruz3Z79ObhmvG2T8pDgm7HMjKm/OHJpL/4nXvKD5jzR84
buIWdcQ+WUU6RV0OzbPT8v0PAnyo6eRHAAYLBHWxCAD5e/dL40YMy9h624izZeet
dg6clyevyOv1ertLQDtIaZv/MjsfuPl2oMVk+7a0nCcjAkGWBLDlkUJregk0YD3d
Or1s/A4honYXwD5WtVb5NtFTw0JX044BSOPj15x4HbhuaA9C/OSjB2H9KTnijkz8
8E5gSdhca3yO0yaoczTBOuV+2ls4jlNPpJmxZrNg4TKbdTwKpqEAgYThTm6yEfva
FP2mdIX8qcSExjeXpU7oLY66uyUJN0cq2IBwXcD+ZxIeFttBVYL3d/Um6BwWMT3A
1FWIIys59jr2TU36f2dx7L3C+VJ3PDvWnDbsvffy5dQM4sU72kU3IX+L5fV6ooc7
GD0wXqVdMW2dtwfzt0saCFLdUW3ijXsoZixaaPobmvMVvlNt+NJmvNu20MWlwrb1
/ROjOVwdj+ItsrX2A2zQ7jnRZXhdndpo8bJ8LggclRdDGBBXLhdTHTvIqgngzDsI
ziH9zOl1uGVdE1mWXwTa82dF7vQ0YG1cax0heLDLM0BuVh8kJXuwSZ0CASU=
-----END RSA PUBLIC KEY-----
""")
_pangrams=[
    "Bawds jog, flick quartz, vex nymphs.",
    "Glib jocks quiz nymph to vex dwarf.",
    "Bright vixens jump; dozy fowl quack.",
    "Quick zephyrs blow, vexing daft Jim.",
    "Sphinx of black quartz, judge my vow!",
    "Both fickle dwarves jinx my pig quiz.",
    "Fat hag dwarves quickly zap jinx mob.",
    "Public junk dwarves quiz mighty fox.",
    "How quickly daft jumping zebras vex.",
    "Two driven jocks help fax my big quiz.",
    "Jack, love my big wad of sphinx quartz!",
    "Do wafting zephyrs quickly vex Jumbo?",
    "Go, lazy fat vixen; be shrewd, jump quick.",
    "Jackdaws love my big sphinx of quartz",
    "Fickle jinx bog dwarves spy math quiz.",
    "Five hexing wizard bots jump quickly.",
    "Quick fox jumps nightly above wizard.",
    "Vamp fox held quartz duck just by wing.",
    "Five quacking zephyrs jolt my wax bed.",
    "The five boxing wizards jump quickly.",
    "Jackdaws love my big sphinx of quartz.",
    "Cozy sphinx waves quart jug of bad milk.",
    "My ex pub quiz crowd gave joyful thanks.",
    "A very bad quack might jinx zippy fowls.",
    "Pack my box with five dozen liquor jugs.",
    "Few quips galvanized the mock jury box.",
    "The jay, pig, fox, zebra and my wolves quack!",
    "Sex prof gives back no quiz with mild joy.",
    "The quick brown fox jumps over a lazy dog.",
    "Fix problem quickly with galvanized jets.",
    "Waxy and quivering, jocks fumble the pizza.",
    "When zombies arrive, quickly fax judge Pat.",
    "A wizard’s job is to vex chumps quickly in fog.",
    "Heavy boxes perform quick waltzes and jigs.",
    "My faxed joke won a pager in the cable TV quiz show.",
    "Amazingly few discotheques provide jukeboxes.",
    "The lazy major was fixing Cupid’s broken quiver.",
    "Foxy diva Jennifer Lopez wasn’t baking my quiche.",
    "By Jove, my quick study of lexicography won a prize.",
    "As quirky joke, chefs won’t pay devil magic zebra tax.",
    "My girl wove six dozen plaid jackets before she quit.",
    "Six big devils from Japan quickly forgot how to waltz.",
    "Foxy parsons quiz and cajole the lovably dim wiki-girl.",
    "A very big box sailed up then whizzed quickly from Japan.",
    "Jack quietly moved up front and seized the big ball of wax.",
    "The wizard quickly jinxed the gnomes before they vaporized."
]
_credits="""
INJECTION {0}
Originally created for Pyweek 19
By TOASTEngineer

Written in Python 3.7
Python is:
{1}

Font is Fixedsys Excelsior 3.01 (FSEX300.ttf)
http://www.fixedsysexcelsior.com
Darien Gavin Valentine

Uses Pygame SDL wrapper:
http://www.pygame.org

Uses Pyperclip clipboard handling library:
http://coffeeghost.net/2010/10/09/pyperclip-a-cross-platform-clipboard-module-for-python/

Uses Jedi Python code autocompletion library:
http://jedi.jedidjah.ch/en/latest/

Uses Dill extended serializer:
http://trac.mystic.cacr.caltech.edu/project/pathos/wiki/dill

Soundtrack:
Sara De Laek - ET24
Arbietshield - Gormandize
Mop Cortex - Bleeding Brain
Arbietshield - Square
Seazo - Flush
DJ Mar S Aka One Milk - Malfunction
Atsub - Afterdark
Lukal - Glitch
Nocturnes & dreamscapes - Reflection
Lalo - Crackle.ar
Brainstem - Badchoice
Brainstem - Mixdown
Brainstem - Infoleak
kiyo - Brown Deepsea Flight
Extrapool - Antherosclerosis at 30
XNDL - extraterrestrisch
ATW - Git 'n Glitch
MUPPETMO - TEMA 9
Noqturne - Science (Hell yeah!)
Peter Saar - CornkroncsF4
Jonas Niemann - Fressmaschinen (Prolog)
Retrospecter - In The Rainforest

All music licensed under CC BY-NC-SA 4.0, CC BY-SA 4.0, or CC BY 4.0
Public domain music acquired from http://jamendo.com


Special thanks to everyone who reported bugs, offered criticism, or contributed to the code!


This game is licensed under the MIT license.

The MIT License (MIT)

Copyright (c) 2015 TOASTEngineer

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


Thanks for playing!
""".format(_version,sys.copyright)
#This was a hack to get around the issues with international keyboard layouts that was never finished - leaving it in
#because I might still need it. See build_keycode_translation_table.
#This is why we have that list of pangrams if you're still wondering about that.
keycode_translation_table = None

try:
    _savedir=_savedirs[sys.platform]
except KeyError:
    _savedir=_savedirs["default"]


class Menu(collections.OrderedDict):
    """Implements a selectable menu of options, given names and callbacks. The menu items can then be selected with
    keyboard or mouse. Acts as a dict mapping labels to callbacks."""

    def __init__(self,*args,repeat=False):
        super().__init__(*args)
        self.selection=None
        self.itemys={}
        self.repeat=repeat
        self.clock=pygame.time.Clock()

    def option(self,name):
        """Intended to be used as a decorator. Use like this:
        @mymenu.option("Engage self-destruct!")
        def selfdestruct():
            ..."""
        def inner(func):
            self[name]=func
        return inner

    def increment_selection(self):
        """Move the selector down one option. Wraps around to the first option."""
        optionlist=list(self.keys())
        idx=optionlist.index(self.selection)
        if idx+1 >= len(self):
            self.selection = optionlist[0]
        else:
            self.selection=optionlist[idx+1]

    def decrement_selection(self):
        """Move the selector up one option. Wraps around to the last option."""
        optionlist=list(self.keys())
        idx=optionlist.index(self.selection)
        if idx-1 < 0:
            self.selection=optionlist[-1]
        else:
            self.selection=optionlist[idx-1]

    def draw(self,window,predrawcallback,postdrawcallback):
        self.clock.tick(15)
        window.cursor=(5,5)
        x=0
        window.terminal.blank()

        if predrawcallback:
            predrawcallback(window)
            window.cursorx=5

        for theoption in self.keys():
            if theoption==self.selection:
                bgcolor="white"
                fgcolor="black"
                window.cursorx=6
            else:
                bgcolor=fgcolor=None

            self.itemys[window.cursory]=theoption
            window.write(theoption+"\n",bgcolor=bgcolor,fgcolor=fgcolor)
            window.cursorx=5

        window.update()

        if postdrawcallback:
            postdrawcallback(window)

    def select(self,**kwargs):
        """If the currently selected option is callable, call it and return its return value. Otherwise just return it."""
        selected=self[self.selection]
        #Quick hack to make input()-ing functions not immediately register an ENTER.
        pygame.event.clear()
        if callable(selected):
            return selected(**kwargs)
        else:
            return selected

    def __call__(self,window,defaultselection=None,predrawcallback=None,postdrawcallback=None,**kwargs):
        """Make this menu active and allow it to intercept all user input. Does not exit until the menu is exited by
        pressing escape. If the object mapped to the selected option is callable, call it and return its return value;
        otherwise, return it.

        window is a pygcurse window. predrawcallback is a function that takes a pygcurse window; it gets called right after
        the menu blanks the screen and before it draws the options. postdrawcallback is similar except it gets called right after
        the options are drawn and Pygcurse is asked to update the screen (so call window.update() after your changes).
        Any given keyword arguments are passed into contained callables."""
        if len(self) == 0:
            raise ValueError("Tried to activate an empty menu")

        self.selection=defaultselection
        if self.selection is None:
            self.selection=list(self.keys())[0]
        pygame.key.set_repeat(100,50)
        while True:

            self.draw(window,predrawcallback,postdrawcallback)

            for event in pygame.event.get():
                if event.type==pl.QUIT:
                    pygame.event.post(pygame.event.Event(pl.QUIT))
                    return

                elif event.type==pl.KEYDOWN:
                    if event.key==pl.K_ESCAPE:
                        return

                    elif event.key==pl.K_DOWN:
                        self.increment_selection()

                    elif event.key==pl.K_UP:
                        self.decrement_selection()

                    elif event.key in (pl.K_KP_ENTER,pl.K_RETURN,pl.K_SPACE):
                        result=self.select(**kwargs)
                        if not self.repeat:
                            return result

                elif event.type==pl.MOUSEMOTION:
                    x, y=window.getcoordinatesatpixel(event.pos)
                    if y in self.itemys:
                        self.selection=self.itemys[y]

                elif event.type==pl.MOUSEBUTTONDOWN:
                    x,y=window.getcoordinatesatpixel(event.pos)
                    if y in self.itemys:
                        self.selection=self.itemys[y]
                    result=self.select(**kwargs)
                    if not self.repeat:
                        return result

class GameState:
    """Holds the game's global state. Also holds saving/loading functions."""

    def __init__(self,mainwin):
        """Screen: Pygame surf. obj from display.set_mode
        Mainwin: PygcurseWindow encompassing the entire Pygame screen"""

        self.mainwin=mainwin #type: EZMode
        self.current_level=None #type: levels.Level
        self.current_level_name=""
        self.lastmusic=""
        self.controls_locked=False
        self.created_by_version=_version
        self.current_playlist=None
        self.fxstack=[] #A list of tuples of (timeout, function). function is called on the screen surface each draw. It is removed after timeout draws.
        self.storyflags=collections.defaultdict(bool) #A defaultdict that defaults to False; this is used for storing plot notes about this playthrough
        self.currentsong="" #Levels use this to keep track of what song is playing so they can show the song name to the user
        self.currentsongreadablename=""
        #Turns out that when you're using pyinstaller you can't importlib.reload(), which means our old way of resetting
        #levels (by reloading the `levels` module and changing level) no longer works. So we store a deepcopy of the level
        #we're headed to and go to it when we're resetting instead.
        self.reset_target=None

    def add_fx(self,func,timeout,**kwargs):
        """Add the given function to the FX stack. Specifically, this adds a functools.partial object that calls func with
        the given keyword arguments. These functions are called with the screen surface as their first parameter. After
        timeout draws, the effect is removed. This is a helper function; it's okay to manipulate fxstack directly."""
        if kwargs:
            func=functools.partial(func,**kwargs)
        self.fxstack.append((timeout,func))

    def change_level(self,level):
        """Called to change the current level and do the necessary things to that level before actually playing it.
        level can be a string levelcode (or END which ends the game) or a Level object."""
        pump_events(self,True) #Flush the event queue so we don't just flash the level name because there's keypresses in the pipeline
        if _telemetry and _distributable:
            self.dump_telemetry("Level change (to levelcode {0})".format(level))
        if level=="END":
            self.mainwin.blank()
            self.mainwin.put_cursor((0,0))
            self.mainwin.put_line("""
            YOU'RE WINNER

            Oh dear, we seem to have run out of levels-
            for now! This game is still in development;
            new levels are added every so often,  and
            the game automatically updates itself, so
            check back in a week or so!

            Drop me a line at
            toastengineer@gmail.com
            if you enjoyed the game, and would
            like to see it finished.

            Thanks for playing!
                                """)
            self.mainwin.update()
            pump_events(self,True)
            time.sleep(1)
            pump_events(self,True)
            self.mainwin.wait()
            sys.exit()
        if self.current_level:
            self.current_level.active=False
            prevplyobj = self.current_level.plyobj
        else:
            prevplyobj = None

        try:
            self.current_level=levels.levels[level]
            self.current_level_name=level
        except KeyError:
            self.current_level=level
            self.current_level_name="" #Apparently this is never used. Why did I add it then?

        self.reset_target=pickle.copy(self.current_level)

        self.current_level.start(self, prevplyobj)

    def reset_level(self):
        """Confirm that the user actually wants to reset; if so, switch to the copy of the current level we made when we
        first entered it."""
        self.mainwin.blank()
        self.mainwin.put_line_at("Are you SURE you want to reset?",(5,0))
        self.mainwin.put_line_at("Type 'yes' to confirm; anything else to "
                              "cancel.",(5,1))
        pump_events(self, True)
        self.mainwin.terminal.backend.get_characters()
        self.mainwin.put_cursor(self.mainwin.terminal.center)
        yesno=self.mainwin.get_line(">")
        if yesno.lower()=="yes":
            self.change_level(self.reset_target)
        else:
            return

    def update(self):
        """Tell the level to process a game tick."""
        self.mainwin.terminal.process()

        self.current_level.update(self)

    def do_post_processing(self,thesurf):
        """Given a pygame surface object representing the main game window, perform any FX on the FX stack. This also
        clears FX that have run out of time."""
        newfxstack=[]
        for timeout,thefx in self.fxstack:
            thefx(thesurf)
            timeout-=1
            if timeout>0:
                newfxstack.append((timeout,thefx))
        self.fxstack=newfxstack

    def draw(self, draw_level=True):
        """"Draw the level and the post-processing effects to self.mainwin.
        If draw_level is false, just do the post-processing stack."""
        if draw_level:
            self.current_level.draw(self.mainwin)
        self.mainwin.terminal.draw(noflip=True)

        thesurf=pygame.display.get_surface()

        #Check if the active display surface has changed; this'll happen if the player toggles fullscreen.
        if self.mainwin.terminal.backend.surface != thesurf:
            self.mainwin.terminal.backend.surface=thesurf

        if draw_level:
            self.current_level.p_draw(thesurf, self.mainwin)

        self.do_post_processing(thesurf)

        pygame.display.flip()

    def save(self):
        """Save the game."""
        #Just pickle gamestate. What could go wrong?
        save_path=os.path.join(_savedir,"save.sav")
        temporary_save_path=os.path.join(_savedir,"new_save.sav")
        #Basically, we save to a temporary save, then if we succeed we copy it over the old one.
        #If anything goes wrong, we just give up and the old save is untouched. Either way we delete the temp save.
        if not os.path.exists(_savedir):
            os.makedirs(_savedir)
        try:
            self.created_by_version=_version #this needs to be done in the case where we load an old save and save it again
            pickle.dump(self,open(temporary_save_path,"wb"),protocol=pickle.HIGHEST_PROTOCOL)
        except Exception as e:
            display_alert("INJECTION has encountered an error while trying to save your game. \nThe game can continue, and your previous save is still intact.")
            send_error_report("Save failure")
        else:
            shutil.move(temporary_save_path,save_path)
        finally:
            try:
                os.remove(temporary_save_path)
            except Exception:
                pass

    def __getstate__(self):
        """This gets called by Pickle/Dill when it tries to pack up GameState's attributes; the dict it returns is used
        in place of self.__dict__."""
        mydict=self.__dict__.copy()
        #Pygcurse windows don't keep well - let's just destroy the thing entirely and make it anew when we load
        del mydict["mainwin"]
        mydict["current_playlist"]=None
        return mydict


    def save_and_quit(self):
        """Save the game and terminate execution."""
        if self.current_level:
            self.save()
            #Make sure to shut down the music properly...
            self.current_level.music.stop()
            self.current_playlist=None
        if _telemetry and _distributable:
            self.dump_telemetry("Game quit")
        atexit.unregister(final_failsafe_dump)
        sys.exit()

    def dump_telemetry(self,dumptrig):
        """Dump a textual report of how the player completed the last level, including current time, time taken to finish, and a console transcript."""
        if not _telemetry: return

        #no point sending telemetry on the previous level when we're coming from no actual level
        if not self.current_level:
            return

        send_error_report(context="Level finished", trigger=dumptrig, level_started=str(self.current_level.starttime))

    def get_revisitable_levels(self):
        """Retrieve the dict mapping the proper displayable level names (the level's actual .name) to level identifier
        strings (keys to levels.levels) for the levels that the player has completed across all game sessions. Returns
        an empty dict (representing no completed levels) if the levelselect.json is nonexistent or otherwise
        unreadable."""
        try:
            with open(os.path.join(_savedir,"levelselect.json")) as completedlevels:
                completedlevelsdict=json.load(completedlevels)
                #quick validation just to make sure the ids actually point to extant levels...
                return {name: id for name, id in completedlevelsdict.items() if id in levels.levels}
        except FileNotFoundError:
            #if it's not there, it's not there - no need for an error report!
            return {}
        except Exception:
            send_error_report("getting levelselect.json")
            display_alert("There was an error getting the list of already-completed levels from the game's save directory. It might be corrupted.")
            return {}

    def save_revisitable_levels(self,completedlevelsdict):
        """Save the given dict into levelselect.json. This is intended as an internal function; call
        mark_current_level_revisitable instead."""
        try:
            with open(os.path.join(_savedir,"levelselect.json"),"w") as completedlevels:
                json.dump(completedlevelsdict,completedlevels)
        except Exception:
            send_error_report("updating levelselect.json")

    def mark_current_level_revisitable(self):
        """Update levelselect.json so that the current level can be returned to from the 'level select' screen."""
        levelname=self.current_level.name
        try:
            levelid=levels.levelkeys[self.current_level.name]
        except KeyError:
            #it must be an extension-added level; revisit system doesn't handle those yet
            return
        completed=self.get_revisitable_levels()
        completed[levelname]=levelid
        self.save_revisitable_levels(completed)

    def get_custom_bindings(self):
        """If there is a keybinds.json in the save directory, load it (as a dict of scancodes mapping to strings)
        and parse it into a dict mapping scancodes to string values present in gameobjs.Player.action_codes."""
        try:
            with open(os.path.join(_savedir,"customkeybinds.dat")) as bindings:
                newbindings = {}

                for line in bindings.readlines():
                    newbind=KeyBinding.deserialize(line)
                    newbindings[newbind] = newbind.action

            return newbindings

        except FileNotFoundError:
            #If there is no binds file, there must be no custom binds; return None to represent this.
            return None
        except Exception:
            send_error_report("reading customkeybinds.dat")

    def set_custom_bindings(self,bindingdict):
        """Update or create keybinds.dat to represent the given keybinds dict, mapping Pygame scancodes to strings,
        specifically only those strings listed in gameobjs.Player.action_codes."""
        try:
            with open(os.path.join(_savedir,"customkeybinds.dat"),"w") as bindings:
                bindings.writelines(map(lambda x:x.serialize(), bindingdict))
        except Exception:
            send_error_report("updating customkeybinds.dat")

class IJQuitter:
    """Does the same job as the Quitter ('exit') in the standard REPL."""

    def __repr__(self):
        return "Type ; and hit ENTER to return to the main game screen. Press ESC from there to save and quit."

    def __str__(self):
        return "Type ; and hit ENTER to return to the main game screen. Press ESC from there to save and quit."

    def __call__(self):
        print("Type ; and hit ENTER to return to the main game screen. Press ESC from there to save and quit.")

class UpdateInProgressWindow:
    """When created this object creates a window designed to show the progress of an automatic update routine. When called
    it updates the progress indicator."""

    def __init__(self):
        pygame.init()
        #self.mainwin=pygcurse.PygcurseWindow(40,3,"INJECTION",font=gamefont)
        term = pcg.Terminal((40, 3), "FSEX300.ttf")
        self.mainwin = pcg.ezmode.EZMode(term)
        self.mainwin.put_line("INJECTION is updating, please wait...")
        self.kbrecieved=0
        self.abort=False
        self.done=False

    def __call__(self):
        while True:
            self.mainwin.put_line("{0} kilobytes received".format(self.kbrecieved),0,1)
            self.mainwin.terminal.process()
            self.mainwin.terminal.draw()
            for theevent in pygame.event.get():
                if theevent.type==pl.QUIT:
                    self.mainwin.put_line("\nUpdate cancelled.")
                    self.abort=True
                    break
            if self.done:
                break
            time.sleep(0.25)

class UserTimeoutError(Exception):
    def __init__(self,funcname,timeout):
        super().__init__("call to {0} took more than {1:.3f} seconds".format(funcname,timeout))

def call_with_timeout(func,timeout,*args,**kwargs):
    """Call func - if it takes more than timeout secs, raise TimeoutError, otherwise return whatever func returns.
    The timer responsible for the exception is another thread; func is called in the thread that called this.
    Args/kwargs past timeout are passed to func."""
    try:
        timer=threading.Timer(timeout,_thread.interrupt_main)
        timer.start()
        return func(*args,**kwargs)
    except KeyboardInterrupt: #Yes, this will actually cause a keyboardinterrupt - that's the only way for a spawned thread to interrupt the main thread.
        raise UserTimeoutError(func.__qualname__,timeout)
    finally:
        timer.cancel()

def natural_sort_key(s, _nsre=re.compile('([0-9]+)')):
    """Designed to be passed as the key= keyword argument in functions like sort; implements "natural sort" i.e.
    blabla12 comes _after_ blabla2, not before as it would with default string comparison.

    From Claudiu on StackOverflow."""
    return [int(text) if text.isdigit() else text.lower()
            for text in re.split(_nsre, s)]

def display_alert(message):
    """Display an important informational message to the user."""
    pymsgbox.alert(message,"INJECTION",button="OK")

def autoupdate():
    """Do the auto-update procedure. Quits and restarts the program if an update is necessary."""

    #If we're an in-progress version, we damn well better not overwrite ourself
    if not _distributable:
        return

    #If autoupdates have been turned off at the command line, well...
    if _disable_update:
        return

    #This'll evaluate true if we're a cx_freeze frozen distro
    if getattr(sys,'frozen',False):
        #If we're frozen, OS matters (maybe...). So our plat_id is just the return value of sys.platform
        #This'll be win32 for windows, linux2 for linux, darwin for MacOS
        plat_id=sys.platform
    else:
        #If we're not frozen, we must be a source distro
        #and there's only one version of the source; OS is irrelevant
        plat_id="src"

    #Otherwise, begin the auto-update procedure.
    #iju.404.mn is set to point to the autoupdate server.
    with socket.create_connection((_update_addr,_update_port),timeout=1) as connection:
        try:
            #INJECTION 0.6 and up use the revision 2 standard - we can request the server sign with a public key.
            connection.send("__enable_signing__\n".encode())

            if connection.recv(1)!=b"\x06":
                raise ValueError("Protocol noncompliance (server did not ACK signing request)")
            connection.send(("INJECTION"+plat_id+_version+"\n").encode())
            filehash=connection.recv(512)

            if not connection:
                #If the server just killed the connection after hearing our version, we're up to date
                return
            connection.send(b"\x06")

            #create a "bro i'm updating chill a minute" window with threading
            uipwindow=UpdateInProgressWindow()
            uipthread=threading.Thread(target=uipwindow,daemon=True)
            uipthread.start()

            file=bytes()
            while True:
                stuff=connection.recv((2**16)*10)
                uipwindow.kbrecieved+=10
                pygame.event.pump() #gotta keep windows from deciding we're stalled
                if uipwindow.abort:
                    uipthread.join()
                    return
                if stuff:
                    file+=stuff
                else:
                    break
        finally:
            uipwindow.done=True
            uipthread.join()


        #It'll fail here if we're at the latest version (because the last read gives us nothing, which obviously doesn't hash the same as
    #our archive
    if not file:
        return

    #This raises an exception if the file does not verify
    rsa.verify(file,filehash,_autoupdate_public_key)

    if plat_id != "src":
        try:
            execbakpath, executable = os.path.split(sys.executable)
            #Just to make double sure...
            if "python" not in execbakpath:
                shutil.move(sys.executable, os.path.join(execbakpath, "old_executable.exe"))
        except Exception:
            pass

    archive=io.BytesIO(file)
    zipdata=zipfile.ZipFile(archive)
    zipdata.extractall(os.path.curdir)
    args=sys.argv[:]
    args.insert(0,sys.executable)
    os.execv(sys.executable,args)

def do_cmdline_args():
    """Do our own goddamn command line processing. Screw argparse. Ugh."""
    argv=sys.argv
    for k, arg in enumerate(argv):

        if arg=="-force_backend":
            print("Forcing Pygame backend to",argv[k+1])
            os.environ["SDL_VIDEODRIVER"]=argv[k+1]
        elif arg=="-disable_update":
            print("Disabing automatic updates for this session.")
            global _disable_update
            _disable_update=True
        elif arg=="-update_addr":
            print("Using alternate update server at",argv[k+1])
            global _update_addr
            _update_addr=argv[k+1]
        elif arg=="-font":
            print("Using alternative font",argv[k+1])
            global _font_path
            _font_path=argv[k+1]
        elif arg=="-disable_music":
            print("Disabling music for this session.")
            global _music_disabled
            _music_disabled=True
        elif arg=="-disable_telemetry":
            print("Disabling telemetry for this session.")
            global _telemetry
            _telemetry=False
        elif arg=="-save_directory":
            global _savedir
            _savedir=argv[k+1]
            print("Savegames will be placed in {0}".format(_savedir))
        elif arg=="-force_standard_pickle":
            import pickle
        elif arg=="-disable_extensions":
            global _extensions_disabled
            _extensions_disabled=True
        elif arg=="-help" or arg=="-h" or arg=="-?" or arg=="/?" or arg=="-v":
            print("INJECTION Version",_version)
            print("By TOASTEngineer")
            print("")
            print("-help : Display this text")
            print("-disable_music : Disable all in-game music.")
            print("-force_backend [backend] : Force the game to to use a specific rendering backend. There shouldn't be any real reason to use this anymore.")
            print("-disable_update : Disable the auto-update functionality.")
            print("-update_addr [addr]: Grab auto-updates from the given server.")
            print("-font [path]: Force INJECTION to use the specified font. Should be a full path to a font file.")
            print("-disable_telemetry: Disable sending of telemetry data to the auto-update server.")
            print("-save_directory: Put game saves in the given directory path.")
            print("-force_standard_pickle: Force the use of the standard Python serializer instead of Dill.")
            print("-disable_extensions: Disable the importing of .py files in ./extensions/.")
            sys.exit()

def build_keycode_translation_table():
    translationtable=dict()
    while True:
        thepangram=random.choice(_pangrams)
        print("""
INJECTION has detected that you're using a keyboard layout it doesn't
know how to deal with. It's okay, though: I just need you to type a few
sentences so I can learn how your keyboard wants to talk to me!

[PRESS ENTER TO CONTINUE]
""")
        input("")
        print("""
What you type will look like nonsense (it looks like nonsense to me too,
which is why I'm having you type back what I say);
just try not to make any mistakes.

Type this sentence:\n\n {0}""".format())
        garbledinput=input("")

        if len(garbledinput)!=len(thepangram):
            print("Hmm. There's the wrong number of letters in there. Try another one.")
            continue

        for pgchar, inputchar in zip(thepangram,garbledinput):
            translationtable[ord(inputchar)]=ord(pgchar)

        global keycode_translation_table
        keycode_translation_table=translationtable

        print("Okay, now I'm gonna have you type another sentence to make sure I did this right.")
        print("Type this sentence:\n\n")

        confirmationpangram=random.choice(_pangrams)
        fixedinput=input("")

        if fixedinput==confirmationpangram:
            print("Great! That worked perfectly!")
            print("You should be able to play the game normally now.")
            print("Sorry for the inconvenience!")
            return thepangram
        else:
            print("Hmm. I still can't make sense of what you're typing.")
            print("Let's try again.")
            continue

def earlier_than(a,b):
    """Returns True if a is a later version than b."""
    return a.split(".") > b.split(".")

def detect_wine():
    """WINE is a compatibility layer for Windows applications on Linux. Basically, it's a program running under Linux
    that offers the Windows API to client programs, effectively allowing you to run Windows programs under Linux or
    OSX. The recommended way to run INJECTION on Linux is to use the Windows binaries under Wine. As such, it's
    worth checking for WINE and the Linux version it's running on when making reports.

    Detection works by trying to launch a *NIX utility that reports OS details via the virtual drive WINE by default
    offers to WINE applications representing the actual system drive. Obviously this detection isn't perfect; WINE can
    be configured not to offer Z: or a Windows user can create a Z drive and put a fake uname application in there, but
    barring deliberate interference this should always work.

    Returns False if (probably) not WINE, or a string containing details about the Linux version we're (probably)
    running under."""

    import subprocess
    try:
        return subprocess.check_output(os.path.join("Z:","bin","uname -srv")).decode()
    except Exception:
        return False

def fix_072_save(gamestate):
    """Sticks .color attributes on all Wall() objects in the currently loaded level."""
    current_level=gamestate.current_level
    for theobj in current_level.objlist+current_level.safe_objlist:
        if isinstance(theobj,levels.gameobjs.Wall):
            theobj.color=(127,127,127)

def fix_073_save(gamestate):
    gamestate.current_level.plyobj.injectionconsole.wrappedinputtext=None

def fix_keybinds_json_to_new_keybinds(gamestate):
    """Convert a 0.9.2 and earlier keybinds.json to the keybinds.dat format introduced in 0.9.3."""
    #Read the old binds...
    bindingsjson=os.path.join(_savedir, "customkeybinds.json")
    try:
        with open(bindingsjson) as bindings:
            bindingdict = json.load(bindings)
            # We turned the tuples in to (12,34) style strings, so now we turn them back
            fixeddict = dict()
            for mangledkey, value in bindingdict.items():
                key = mangledkey.replace("(", "").replace(")", "").split()
                key = (int(key[0].replace(",", "")), int(key[1]))
                fixeddict[key] = value
    except FileNotFoundError:
        #Unless they don't exist of course
        return None
    except Exception:
        send_error_report("reading customkeybinds.json for compatibility purposes")
        return

    #Convert the tuple keys to KeyBinding objs
    fixeddict={KeyBinding(*k, action=v) : v for k, v in fixeddict.items()}

    #Then save the binding dict the new way
    gamestate.set_custom_bindings(fixeddict)

    #Then nix out the old one
    shutil.move(bindingsjson,bindingsjson+".obsolete")

def fix_094_music_filenames():
    """In... uh, some version or other we started showing names of songs based on the song filenames. Problem with
    this was that the filenames weren't quite right, i.e. some were artistname - artistname - songtitle. This
    fixes that."""
    specific_fixes = {
        "arbeitsheld_-_arbeitsheld_-_gormandize.ogg" : "Arbeitsheld - Gormandize.ogg",
        "arbeitsheld_-_arbeitsheld_-_square.ogg"     : "Arbeitsheld - Square.ogg",
        "atw_-_git_n__glitch.ogg"                    : "Atw - Git n' Glitch.ogg",
        "crazy_clan_-_crazy_clan_-_atmosphere_of_mad_reason.ogg" : "Crazy Clan - Atmosphere of Mad Reason.ogg",
        "muepetmo_-__--__tema_9-.ogg" : "muepetmo - _-- TEMA 9 -.ogg",
        "noqturne_-_science__hell_yeah__.ogg" : "Noqturne - Science - Hell Yeah.ogg",
        "paul_dee__laek_-_e2t4.ogg" : "Sara Dee Laek - E2T4.ogg",
        "retrospecter_-_retrospecter_-_in_the_rainforest.ogg" : "Retrospecter - In the Rainforest.ogg",
        "sij_-_sij_-_crystal_oscillator__feat._thetou___remix_by_deception_cost_.ogg" : "Sij - Crystal Oscillator feat. thetou (Remix by Deception Cost).ogg",
        "xndl_-_xndl_extraterrestrisch.ogg" : "Xndl - Extraterrestrisch.ogg",
        "jonas_niemann_-_fressmaschinen__prolog_.ogg" : "Jonas Niemann - Fressmaschinen Prolog.ogg",
        "nebulist_-_drillin__the_difference.ogg" : "Nebulist - Drillin' the Difference.ogg",
        "nocturnes___dreamscapes_-_reflection.ogg" : "Nocturnes & Dreamscapes - Reflection.ogg"
    }

    music = pathlib.Path("music")

    for oldfname in music.glob("*.ogg"):
        oldfname = oldfname.name
        newfname = (specific_fixes[oldfname] if oldfname in specific_fixes
                    else oldfname.replace("_", " ").title()[:-4] + ".ogg")

        if oldfname != newfname:
            shutil.move(music / oldfname, music / newfname)

def load_extensions():
    if _extensions_disabled: return
    from importlib.machinery import SourceFileLoader
    for thefname in glob.iglob(os.path.join(".","extensions","*")):
        if os.path.basename(thefname) == "__pycache__": continue

        try:
            SourceFileLoader( os.path.splitext( os.path.basename(thefname) )[0],thefname).load_module()
        except Exception:
            display_alert("Failed to load extension {0}:\n{1}".format(thefname,traceback.format_exc()))

def game_init():
    """Performs primary initialization. Returns a GameState object, which'll be this sessions global state manager."""

    #logging.basicConfig(filename='example.log',level=logging.DEBUG)

    #Since directx mode doesn't appear to do anything bad, it's now the default on Windows.
    if sys.platform=="win32":
        os.environ["SDL_VIDEODRIVER"]="directx"

    if dill:
        if not _distributable:
            print("Dill loaded successfully.")
        #dill.detect.trace(True)
        dill.settings['byref']=True
        dill.settings['recurse']=False

    do_cmdline_args()

    #yeah, this will also alert when run with python 4, but that's actually desired behavior; "hey man this script is
    #several years out of date so don't be upset if it doesn't work." Makes the language of the message a little weird though.
    if sys.version_info.major != 3 or sys.version_info.minor < 7:
        display_alert("INJECTION is designed to run with Python 3.7.1 or greater. You are running {0}.".format(sys.version[:5]))

    #check for/install updates. This'll raise various types of exceptions if it fails; we just report them and carry on.
    try:
        autoupdate()
    except Exception as e:
        if not isinstance(e, ConnectionAbortedError):
            try:
                send_error_report("Update failed",noijlog=True, nosendexception = isinstance(e, rsa.VerificationError))
                display_alert("Automatic update failed. Hit OK to continue.")
            except socket.timeout:
                if not _distributable:
                    traceback.print_exc()

    term = pcg.Terminal((80, 30), _font_path, 16)
    term.stdiocursor = pcg.InputCursor(term, cursorchar="")
    mainwin = EZMode(term)
    mainwin._cursor_predicate()
    mainwin.terminal.ezmodecursor.cursorchar=""

    #This is a little evil, but only a little. It's so that the player gets a notification on how to quit the injection console
    #when he types exit, instead of being given instructions on how to hard-quit the whole damn game (yes, this change propogates
    #to the injection console's version of __builtins__)
    __builtins__.exit=IJQuitter()
    __builtins__.quit=__builtins__.exit

    # Delete `help` because it causes confusion.
    try :
        del __builtins__.help
    except AttributeError:
        pass

    pygame.key.set_repeat(50,100)

    #We ought to make sure that the directory that we use to store crap exists.
    os.makedirs(_savedir,exist_ok=True)

    #finally, let the extensions have a go at all this stuff.
    #though we want to load them before trying to load a save because saves that reference them will fail otherwise
    load_extensions()

    fix_094_music_filenames()

    try:
        #if not _distributable:
        #raise Exception
        gamestate=pickle.load(open(os.path.join(_savedir,"save.sav"),"rb"))
        gamestate.mainwin=mainwin
        #We've got to start the music here when we load a save; the level
        #init doesn't get called.
        gamestate.current_level.music=levels.playlists[gamestate.current_level.music.name]
        gamestate.current_level.start_music(gamestate,True)
        #We ALSO need to reset the injection console's version of mainwin
        gamestate.current_level.plyobj.injectionconsole.replace_window(mainwin)
        #AND make the plyobj reset its keybind info so changes made in the keybinds menu are reflected
        gamestate.current_level.plyobj.binds=None
        #if not _distributable:
        #    raise FileNotFoundError
        if not hasattr(gamestate.current_level.plyobj.injectionconsole, "textwrap"):
            import textwrap
            gamestate.current_level.plyobj.injectionconsole.textwrap = textwrap.TextWrapper(replace_whitespace=False, drop_whitespace=False, width=term.width-1)
    except Exception as e:
        if not isinstance(e,FileNotFoundError):
            #if it's just FileNotFound we just don't have a save to load, don't report that
            send_error_report("Failed to load saved game",noijlog=True)
        #If anything goes wrong in that entire block we'll just continue with a new game
        gamestate=GameState(mainwin)
        pygame.mixer.music.set_volume(0.2)
    else:
        if gamestate.created_by_version!=_version:
            display_alert("This save is from a different version of INEJCTION. This can cause issues; you may want to press R to reload and reset the level.")
            #There's a nasty issue with loading saves from 0.7.2 and earlier; this function call fixes it
            if not earlier_than(gamestate.created_by_version,"0.7.2"):
                fix_072_save(gamestate)
            if not earlier_than(gamestate.created_by_version,"0.7.3"):
                fix_073_save(gamestate)

    mainwin.terminal.backend.os_quit_button_callback = gamestate.save_and_quit

    fix_keybinds_json_to_new_keybinds(gamestate)

    return gamestate

def display_attract_screen(gamestate):
    """Displays the 'attract mode' screen with title, basic instructions. Blocks until user presses a key."""
    bindings=gamestate.get_custom_bindings()

    if not bindings:
        keynames = ("ARROWKEYS",";", "R", "ESCAPE")
    else:
        invertedbindings = {bind.action:bind for bind in bindings}
        if invertedbindings["Move Up"].key=="up" and invertedbindings["Move Down"].key=="down" and \
           invertedbindings["Move Left"].key=="left" and invertedbindings["Move Right"].key=="right":
            movekeys = "ARROWKEYS"
        else:
            movekeys = "{0:i}{1:i}{2:i}{3:i}".format(invertedbindings["Move Up"], invertedbindings["Move Left"],
                                             invertedbindings["Move Down"], invertedbindings["Move Right"])
        inject = format(invertedbindings["Injection Console"], 'i') #@TODO: THIS IS WRONG - NEED JUST THE NAME OF THE KEYS
        reset = format(invertedbindings["Reset Level"], 'i')
        quit = format(invertedbindings["Save & Quit"], 'i')
        keynames = (movekeys, inject, reset, quit)

    mainwin=gamestate.mainwin
    mainwin.put_cursor("topleft")
    mainwin.put_line(
                     """
                     INJECTION v{0}

                     {1} to move
                     Walk into objects to interact with them
                     {2} to inject
                     {3} to reset the level
                     {4} saves and quits.

                     Game auto-saves after every level.

                     PRESS ANY KEY TO CONTINUE""".format(_version, *keynames)
                     )
    if _telemetry:
        mainwin.put_line("""
        \n\n\n\n\n\n\n\n\n\n\n\n\nINJECTION sends basic telemetry data to a central server at the completion of
        each level. No personally identifiable data is sent. To disable this,
        launch INJECTION with the -disable_telemetry switch.""".replace("    ", ""))
    else:
        mainwin.put_line("\n\n\n\n\n\n\n\n\n\n\n\n\n                     Telemetry disabled.")
    mainwin.update()
    mainwin.wait(15)

def add_menu_glitch_fx(gamestate,glitchiness=40):
    """This function should be added to a Menu's predrawcallback callback; it sticks some glitch effects on the screen.
    'glitchiness' controls the... glitchiness; each tick has a 1/glitchiness chance of adding an effect.
    So higher values mean LESS intense effects, not more.
    This also gets called by the credits routine for its effects."""
    if random.randint(0,glitchiness-1)==0:
        if random.randint(0,max(5,glitchiness//3)):
            gamestate.add_fx(fx.chromatic_aberration,random.randint(1,5),intensity=random.randint(1,6))
        else:
            gamestate.add_fx(fx.random_chromatic_aberration,random.randint(3,8),intensity=random.randint(1,8))

def display_credits(mainwin):
    """Roll credits! Scrolls the contents of _credits"""
    clock=pygame.time.Clock()
    mainwin.blank()
    mainwin.terminal.clickzonemanager.deactivate_group("mainmenu")
    #@TODO: Perfect application for the RichTextCursor!
    c = pcg.Cursor(mainwin.terminal)
    for thechar in _credits:
        c.smart_writechar(thechar)

        for theevent in pygame.event.get():
            if theevent.type == pl.KEYDOWN:
                if theevent.key == pl.K_ESCAPE:
                    return
            if theevent.type == pl.QUIT:
                return

        mainwin.terminal.draw()
        clock.tick(60)
    mainwin.write_line("\nPRESS ENTER TO RETURN TO MENU")
    mainwin.update()
    mainwin.wait()
    mainwin.terminal.clickzonemanager.activate_group("mainmenu")

#mainmenu=Menu()
mainmenuoptions = collections.OrderedDict()

def start_or_continue_game(gamestate):
    """The save has actually already been loaded; "start/continue" actually just translates to "continue on as normal",
    or to be more cynical, "continue on as if this menu weren't crappily hacked into the game." """
    gamestate.mainwin.terminal.clickzonemanager.deactivate_group("mainmenu")
mainmenuoptions["Start/Continue Game"] = start_or_continue_game

def level_select(gamestate):
    #The "completed levels" (levelselect.json) data file is a JSON file, produced by serializing a dict mapping
    #proper level names (i.e. the level's actual .name) to level identifier strings (the keys to levels.levels)
    #gamestate handles updating and retrieving this data - we just take the dict it gives
    completedlevels=gamestate.get_revisitable_levels()
    if not _distributable:
        completedlevels={k: v for k,v in levels.levelkeys.items()}

    if not completedlevels:
        gamestate.mainwin.center_line_at("You haven't finished any levels yet.", "center")
        gamestate.mainwin.center_line_at("You can only return to levels you've already completed.", "center", (0,1))
        gamestate.mainwin.update()
        gamestate.mainwin.wait()
        return

    gamestate.mainwin.terminal.clickzonemanager.deactivate_group("mainmenu")

    menudict = collections.OrderedDict()
    def goback():
        gamestate.mainwin.terminal.clickzonemanager.deactivate_group("levelselect")
        gamestate.mainwin.terminal.clickzonemanager.activate_group("mainmenu")
        del gamestate.mainwin.terminal.clickzonemanager.groups["levelselect"]
    menudict["Back"] = goback

    #completedlevels is a dictionary mapping names of levels to what we put in to gamestate.change_level
    #to go to that level, so we create a menu of buttons whose label is the name of the level and whose action
    #is to call gamestate.change_level on the corresponding levelcode.
    for label, identifier in sorted(completedlevels.items(), key=lambda x: natural_sort_key(x[0])):
        def onclick(id = identifier):
            gamestate.mainwin.terminal.clickzonemanager.deactivate_group("levelselect")
            gamestate.change_level(id)
            #For ludonarrative reasons, we clear the console command buffer when using the level select.
            #(This is unecessary for now, because we don't carry plyobj accross levels, but we should.)
            gamestate.current_level.plyobj.injectionconsole.cursor.command_buffer.clear()
        menudict[label]=onclick

    ui.VerticalMenu(menudict, gamestate.mainwin.terminal, group="levelselect")
    gamestate.mainwin.terminal.clickzonemanager.activate_group("levelselect")

mainmenuoptions["Level Select"]=level_select

def keycode_tuple_name(thetuple):
    """Given a tuple of (scancode, mod) like the keys to Player.binds, return a string representing it in a human
    readable format."""
    scancode, mods = thetuple
    if mods:
        return "{0} {1}".format(thetuple.modstr(),thetuple.key)
    else:
        return thetuple.key

#This belongs inside KeyBinding, but there's a bug in Dill.
#https://github.com/uqfoundation/dill/issues/288
#Watch this never get fixed.
class KeyBindModTuple(collections.namedtuple("ModTuple", ["shift", "ctrl", "alt", "meta"])):
    def __bool__(self):
        return any(self)

class KeyBinding:
    """Represents the mapping between an individual keypress plus any active modifiers and an action, which can be a
    'magic string' that Player understands or a callable that it will call. A KeyBinding with .action == None represents
    that keypress itself. KeyBindings hash and compare based on their key+mod combo, so two KeyBinding objects that
    correspond to the same combo will be considered equal to each other and will be considered the same object by a Dict.

    Note that you CAN'T just stuff one of these in a dict and expect it to override the one that was there; the dict
    will see that it already has an object with that hash and figure it's done."""

    meta_name = "Windows" if sys.platform == "win32" else "Meta"

    def __init__(self, key, shift=False, ctrl=False, alt=False, meta=False, action=None):
        #self.key, self.mods = KeyBinding.transform_key(key, mod)
        self.key = key
        self.mods = KeyBindModTuple(shift, ctrl, alt, meta)
        self.action = action

    def __hash__(self):
        #Note that a KeyBinding's hash is only dependent on the inputs it matches; so putting one in a dict where
        #there's already a binding to the same keys will do nothing. What you should do is like this:
        #del binds[mynewbinding]
        #binds[mynewbinding]=mynewbinding.action
        #Since the dict doesn't see a difference between mynewbinding and the binding object it has, it deletes the old
        #one.
        return hash((self.key, self.mods))

    def __eq__(self, other):
        return hash(self) == hash(other)

    def modstr(self):
        modnames = []
        if self.mods[0]: modnames.append("Shift")
        if self.mods[1]: modnames.append("Ctrl")
        if self.mods[2]: modnames.append("Alt")
        if self.mods[3]: modnames.append(self.meta_name)

        return '+'.join(modnames)

    def __str__(self):
        return self.__format__("")

    def __format__(self, format_spec):
        """If 'i' is in the format spec, acts as if this KeyBinding has no .action, returning just
        the key name and modifiers."""
        if self.mods:
            modnames=self.modstr()+" "
        else:
            modnames=""

        if self.action and 'i' not in format_spec and isinstance(self.action, str):
            actionstr = " : " + self.action
        else:
            actionstr=""

        #If the key is just a letter, capitalise it if shift is a modifier to it
        #otherwise we always want it to be titlecase
        if len(self.key)!=1 or self.mods.shift:
            keystr=self.key.title()
        else:
            keystr = self.key

        return "{}{}{}".format(modnames, keystr, actionstr)

    def __repr__(self):
        return "<KeyBinding ({0})>".format(self)

    def serialize(self):
        if not isinstance(self.action, str):
            raise TypeError("Binds whose .action is not a string should never end up getting serialized")
        return "{0} {1.shift} {1.ctrl} {1.alt} {1.meta} {2}\n".format(self.key, self.mods, self.action)

    @classmethod
    def deserialize(cls, serialized_keybinding):
        key, shift, control, alt, meta, action = serialized_keybinding.split(" ", 5)

        shift = True if shift == "True" else False
        control = True if control == "True" else False
        alt = True if alt == "True" else False
        meta = True if meta == "True" else False

        action = action.rstrip()
        return cls( key, shift, control, alt, meta, action)
    
    def iskey(self, key, mod):
        """Check wheter given key, mod is binded to this KeyBinding"""
        return KeyBinding.transform_key(key, mod) == (self.key, self.mods)

def key_bindings(gamestate):
    import gameobjs #We have to import gameobjs down here 'cos if it's done top-level it causes a circular import and everything goes to shit
    bindingdict=gamestate.get_custom_bindings()
    if bindingdict is None:
        bindingdict=gameobjs.Player.default_binds.copy()

    def menutext(mainwin):
        mainwin.put_line("Select an action to change the key associated with it\n\n")
        #add_menu_glitch_fx(gamestate)

    def changebinding(mainwin,oldbind,actioncode):
        mainwin.put_line_at("Press the key you wish to map to {0},".format(actioncode), "bottomleft", (0, -5))
        mainwin.put_line_at("or press ENTER to leave it as it is.", "bottomleft", (0, -4))
        mainwin.update()
        mainwin.terminal.backend.get_keypresses()
        while True:
            for key, shift, ctrl, alt, meta in mainwin.terminal.backend.get_keypresses():
                if key=="\n":
                    return True
                elif key == "shift" or key== "ctrl" or key=="alt" or key=="meta":
                    pass
                else:
                    del bindingdict[oldbind]
                    bindingdict[KeyBinding(key, shift, ctrl, alt, meta, actioncode)] = actioncode
                    gamestate.set_custom_bindings(bindingdict)
                    return True

    bindmenu=collections.OrderedDict()
    for bind in sorted(bindingdict, key=lambda x: gameobjs.Player.action_codes.index(x.action)):
        itemlabel=str(bind)
        itemfunc=functools.partial(changebinding,gamestate.mainwin,bind,bind.action)
        bindmenu[itemlabel]=itemfunc

    def resettodefaults():
        gamestate.set_custom_bindings(gameobjs.Player.default_binds.copy())
        gamestate.mainwin.put_line("Bindings reset.")
        return True
    bindmenu["Reset to defaults"]=resettodefaults

    def goback():
        gamestate.mainwin.terminal.clickzonemanager.deactivate_group("keybinds")
        gamestate.mainwin.terminal.clickzonemanager.activate_group("mainmenu")
        del gamestate.mainwin.terminal.clickzonemanager.groups["keybinds"]
    bindmenu["Back to Main Menu"]=goback

    ui.VerticalMenu(bindmenu, gamestate.mainwin.terminal, group="keybinds",
                    topcenter=gamestate.mainwin.terminal.topcenter + (0, 2))
    gamestate.mainwin.terminal.clickzonemanager.deactivate_group("mainmenu")
    gamestate.mainwin.terminal.clickzonemanager.activate_group("keybinds")

mainmenuoptions["Rebind Keys"]=key_bindings

def credits_option(gamestate):
    display_credits(gamestate.mainwin)
    return True
mainmenuoptions["Credits"]=credits_option

mainmenuoptions["Quit"]=lambda x: sys.exit()

def display_main_menu(gamestate):
    """Enter the main menu. This function will loop until the player selects an option that will exit the main menu,
    at which point it returns and we continue with the game. Option functions that want to quit return False or None to
    signify this, causing the menu to itself return True."""
    term = gamestate.mainwin.terminal

    gamestate.mainwin.blank()
    gamestate.mainwin.move_cursor((0, 1))

    for k, v in list(mainmenuoptions.items()):
        mainmenuoptions[k] = functools.partial(v, gamestate)

    topcenter = term.topcenter + (0, 2)
    pcg.ui.VerticalMenu(mainmenuoptions, term, topcenter=topcenter, group="mainmenu")
    term.clickzonemanager.activate_group("mainmenu")

    clock = pygame.time.Clock()
    cell_alive, cell_color, step_gol_bg = fx.init_main_menu_background(term.size)

    #Spawn an r-pentomino in the middle, it's a pattern well known for making lots of stuff happen
    x, y = term.center
    cell_alive[x : x+3, y : y+3] = [
        [False, True, True ],
        [True,  True, False],
        [False, True, False],
    ]

    #While there is any menu open...
    while term.clickzonemanager.any_groups_active():
        gamestate.mainwin.blank()

        gamestate.mainwin.center_line_at("INJECTION {0}".format(_version), "topcenter")
        gamestate.mainwin.center_line_at("MAIN MENU", "topcenter", (0,1))

        add_menu_glitch_fx(gamestate, 15)

        step_gol_bg()

        if pygame.mouse.get_pressed()[0]:
            x, y = term.backend.get_mouse()
            if 0 < x < term.width and 0 < y < term.height:
                cell_alive[x, y] = True
                cell_color[x, y, :] = random.choice((127, 191, 255)), random.choice((127, 191, 255)), random.choice((127, 191, 255))

        for x, y in zip(*cell_alive.nonzero()):
            if cell_alive[x, y]:
                try:
                    term[x, y].character = '∙'
                    term[x, y].fgcolor = cell_color[x, y]
                except IndexError: pass

        term.process()
        gamestate.draw(draw_level=False)
        clock.tick(15)


def pump_events(gamestate,flush=False):
    """This actually mostly exists just to empty out the Pygame event queue; without it it'll fill with events the
     plyobj doesn't care about. It also handles the game being quit through the OS."""
    if not flush: get = functools.partial(pygame.event.get,pl.QUIT)
    else: get = pygame.event.get
    for event in get():
        if event.type==pl.QUIT:
            gamestate.save_and_quit()
        if event.type==pl.KEYDOWN:
            pass

def get_installation_id():
    """Return a string uniquely identifying this installation of the game, that contains no information about the
    system or user (it's just a random UUID4.)"""
    useridpath = pathlib.Path(_savedir) / "installation_id.txt"
    if useridpath.exists():
        return useridpath.read_text()
    else:
        installation_id = str(uuid.uuid4())
        useridpath.write_text(installation_id)
        return installation_id

def send_error_report(context="Unspecified",noijlog=False,message="", nosendexception=False, **kwargs):
    """Send a crash report to the telemetry server, unless telemetry is disabled."""
    global gamestate
    try:
        if not _distributable:
            print(traceback.format_exc(), file=sys.__stdout__)
            if getattr(sys, "frozen", False):
                display_alert(traceback.format_exc(limit=6))

        if not _telemetry: return

        try:
            level_name = gamestate.current_level.name
        except (NameError, AttributeError):
            level_name = "No level"

        try:
            console = '\n'.join(gamestate.current_level.plyobj.injectionconsole.transcript)
        except Exception:
            console = f"Error getting console transcript:\n{traceback.format_exc()}"

        #People tend to name their home dir their actual name, and we don't want to leak that
        console = console.replace(str(pathlib.Path.home().name), "~")

        pyerrorreport.send(version=_version, level=level_name, platform = sys.platform, context=context, message=message,
                           frozen = getattr(sys,"frozen",False), wine = detect_wine(), traceback = traceback.format_exc(),
                           installation_id = get_installation_id(), console = console, **kwargs)
    except Exception:
        #What can we do about an error in the error reporter?
        try:
            print(traceback.format_exc(), file=sys.__stdout__)
            if getattr(sys, "frozen", False):
                printpath = pathlib.Path("error.log").resolve()
                display_alert("There was an error, and another error reporting that error."
                              f"\nA file with the name 'error.txt' has appeared at {printpath}."
                              "Please EMail it to toastengineer@gmail.com.")
                with open("error.log", "at") as f:
                    traceback.print_exc(file=f)
        except Exception:
            #What can we do about an error in the error reporter's error reporter?!?!?
            print("There was an error, an error reporting that error, "
                  "and an error writing out the details of those errors. All hope is lost.", file=sys.__stderr__)
            return

def final_failsafe_dump():
    """The game keeps exiting under mysterious circumstances that aren't a crash but aren't triggering the normal on-exit
    functionality. This is a 'last resort' dump function to hopefully figure out what the hell is happening."""
    send_error_report("Improper exit")

def report_warning(message, category, filename, lineno, file, line):
    text=warnings.formatwarning(message, category, filename, lineno, line)
    send_error_report("Warning",noijlog=True,message=text)

if __name__ == '__main__':
    if _distributable:
        atexit.register(final_failsafe_dump)
        warnings.showwarning = report_warning

    gamestate=game_init()
    display_attract_screen(gamestate)
    display_main_menu(gamestate)
    #gamestate.current_level will be None if we've just started; if it's not, we loaded a save.
    if not gamestate.current_level:
        gamestate.change_level("Start")
    clock=pygame.time.Clock()

    #Main loop!
    try:
        while True:
            #Blank the screen
            gamestate.mainwin.blank()
            #The gamestate obj. relays the update order to the level and Pygcurse
            gamestate.update()
            gamestate.draw()
            #Do global-level event stuff
            pump_events(gamestate)
            #Only burn as much CPU as we need
            clock.tick(15)
            #print(clock.get_fps())
            #Yeah, all the tickrate-related stuff is done with magic numbers.
            # It's more-or-less indefensible, honestly. I mean, it _was_ a game jam game...
    except Exception as e:
        if _distributable:
            send_error_report("Unhandled exception")
            display_alert("INJECTION has malfunctioned and will now close. Your save file is probably still intact.")
            atexit.unregister(final_failsafe_dump)
            sys.exit()
        else:
            raise
