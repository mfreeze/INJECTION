# cython: language_level=3
import pygame
cimport numpy
import numpy.random
import random
from libc.stdlib cimport abs
from libc.math cimport sqrt

#I added this because some soverflo answer suggested it but it doesn't appear to help
numpy.import_array() 

cpdef chromatic_aberration(surface,int intensity=5):
    cdef int x,y,z,maxx,maxy
    cdef numpy.ndarray[unsigned char,ndim=3] array
    cdef numpy.ndarray[unsigned char,ndim=2] r,g,b
    r=pygame.surfarray.pixels_red(surface)
    g=pygame.surfarray.pixels_green(surface)
    b=pygame.surfarray.pixels_blue(surface)
    array=pygame.surfarray.pixels3d(surface)

    maxx,maxy=surface.get_rect().bottomright

    for x in range(maxx):
        for y in range(maxy):
            try:
                pass
                array[x,y,0]=r[x+intensity,y]
                array[x,y,1]=g[x,y+intensity]
                array[x,y,2]=b[x+intensity,y-intensity]
            except IndexError:
                pass

cpdef mapped_chromatic_aberration(surface, numpy.ndarray[long,ndim=2] intensitymap):
    cdef int x,y,z,maxx,maxy,intensity
    cdef numpy.ndarray[unsigned char,ndim=3] array
    cdef numpy.ndarray[unsigned char,ndim=2] r,g,b
    r=pygame.surfarray.pixels_red(surface)
    g=pygame.surfarray.pixels_green(surface)
    b=pygame.surfarray.pixels_blue(surface)
    array=pygame.surfarray.pixels3d(surface)

    maxx,maxy=surface.get_rect().bottomright

    for x in range(maxx):
        for y in range(maxy):
            try:
                pass
                intensity=intensitymap[x,y]
                array[x,y,0]=r[x+intensity,y]
                array[x,y,1]=g[x,y+intensity]
                array[x,y,2]=b[x+intensity,y-intensity]
            except IndexError:
                pass

cpdef random_chromatic_aberration(surface,int intensity=5):
    mapped_chromatic_aberration(surface,numpy.random.randint(-intensity,intensity,surface.get_rect().bottomright))

cpdef chroma_warp(surface,int x, int y, int radius, int power):
    cdef numpy.ndarray[long,ndim=2] warpmap
    warpmap=numpy.zeros(surface.get_size(), dtype=numpy.long)
    cdef int maxdist = radius ** 2
    cdef int maxx, minx, maxy, miny, warpx, warpy

    for xofst in range(-radius, radius):
        for yofst in range(-radius, radius):
            warpx=x+xofst
            warpy=y+yofst
            warpmap[warpx,warpy] = (<int> sqrt(xofst**2 + yofst**2) * power) // 50

    mapped_chromatic_aberration(surface, warpmap)

def screenshake(surf,intensity=3):
    surf.scroll(random.randint(-intensity,intensity),random.randint(-intensity,intensity))

def init_main_menu_background(worldsize):
    """Return a reference to a w x h array of booleans where True represents a cell being alive, 
    a w x h x 3 array of colors, and a function that will step the simulation forward."""
    w, h = worldsize
    w += 2; h += 2

    world  =                 numpy.zeros(                     (w, h),    dtype = numpy.bool)
    colors = numpy.ma.array( numpy.random.randint(256, size = (w, h, 3), dtype = numpy.uint8), mask=False)

    world[0,  :] = False
    world[-1, :] = False
    world[:,  0] = False
    world[:, -1] = False

    w, h = worldsize

    slice_identity  = slice(1, w),     slice(1, h)
    slice_up        = slice(1, w),     slice(0, h - 1)
    slice_down      = slice(1, w),     slice(2, h + 1)
    slice_left      = slice(0, w - 1), slice(1, h)
    slice_right     = slice(2, w + 1), slice(1, h)
    slice_upleft    = slice(0, w - 1), slice(0, h - 1)
    slice_upright   = slice(2, w + 1), slice(0, h - 1)
    slice_downleft  = slice(0, w - 1), slice(2, h + 1)
    slice_downright = slice(2, w + 1), slice(2, h + 1)

    def step():
        cells_alive = numpy.sum(world[slice_identity])
        for t in range(max(2, 20 - cells_alive)):
            x, y = random.randint(4, world.shape[0]-4), random.randint(4,world.shape[1]-4)
            world[x, y] = True
            colors[x, y] = random.randint(127,255), random.randint(127,255), random.randint(127,255)
    
        neighbor_counts = numpy.sum((
            world[slice_up],     world[slice_down],    world[slice_left],     world[slice_right],
            world[slice_upleft], world[slice_upright], world[slice_downleft], world[slice_downright],
        ), axis=0)
    
        #4 = 4 neighbors (cell becomes alive) or 3 neighbors + self already alive (cell remains alive)
        #cells with any other local count are (remain) dead
        world[slice_identity] = (neighbor_counts == 3) ^ ((neighbor_counts == 2) & world[slice_identity])
    
        colors.mask[:, :, 0] = ~world
        colors.mask[:, :, 1] = ~world
        colors.mask[:, :, 2] = ~world
    
        colors[slice_identity] = numpy.ma.mean((
            colors[slice_identity],
            colors[slice_up],     colors[slice_down],    colors[slice_left],     colors[slice_right],
            colors[slice_upleft], colors[slice_upright], colors[slice_downleft], colors[slice_downright],
        ), axis=0, dtype=numpy.uint8)
        
    return world, colors, step