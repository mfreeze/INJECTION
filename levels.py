if __name__ == '__main__':
    print("This file is not meant to be executed. Please try main.py.")
    exit()

import code
import datetime
import inspect
import os.path
import random
import sys
import textwrap
import threading
import traceback
import functools
import time
import math
import warnings

import collections
import dill
import pygame

try:
    import pyperclip
except ImportError:
    pyperclip=None

#try:
#    import jedi
#except ImportError:
#    jedi=None
jedi=None #jedi is disabled by default

import gameobjs
import main
import fx

import pyconsolegraphics as pc

class InfoObj:
    """Print some basic information about the given object to the console. Specifically this pretty-prints the object's docstring,
        all its properties, and the docstrings of any methods it has."""
    def __init__(self):
        self.enforcedattrs={}

    def __repr__(self):
        return "Type info(obj), where obj is a name of an object, for information on that object"

    def __call__(self,obj,printspecial=False):
        """Print some basic information about the given object to the console. Specifically this pretty-prints the object's docstring,
        all its properties, and the docstrings of any methods it has."""

        if obj == __builtins__:
            print("__builtins__ is the object that holds everything that's built-in to Python.\nYou don't need to worry about it. Trust me.")
            return

        if not isinstance(printspecial,bool):
            raise ValueError("printspecial flag should be True or False, not {0}".format(printspecial))

        #Instead of using print() we stick things into a list which we then .join and print in one block;
        #this works with the InjectionConsole's pagination system and gets around some bugs in same.
        prints=[]

        prints.append(repr(obj))
        objstr=str(obj)
        if objstr!=repr(obj):
            prints.append(objstr)

        prints.append("")

        classname=obj.__class__.__name__
        if classname:
            prints.append("Object is an instance of the class '{}'.".format(obj.__class__.__name__))

        prints.append("")

        if isinstance(obj,list):
            prints.append("Lists are sequences, and are accessed by putting an index in square brackets after their name.")
            if obj:
                prints.append("For example, your_list[0] is {}".format(obj[0]))
            else:
                prints.append("I'd show you an example, but this list is empty.")
        elif isinstance(obj,dict):
            prints.append("A dict is a mapping; its contents are accessed by putting a key in square brackets after its name.")
            if obj:
                thekey=random.choice(obj.keys())
                prints.append("For example, your_dict[{}] is {}".format(thekey,obj[thekey]))
            else:
                prints.append("I'd show you an example, but this dict is empty.")
        elif hasattr(obj,"__getitem__"):
            prints.append("This object is a sequence; access its contents by putting an index enclosed in square brackets after its name.")

        if callable(obj):
            prints.append("This object is callable; put () after it to make it do something.")
            prints.append("The docstring below will probably contain more information on")
            prints.append("how to use it and what it does.")

        if prints[-1] != "":
            prints.append("")

        doc=inspect.getdoc(obj)
        if doc:
            if "    " in doc:
                #If there's "    " in the docstring, it contains code snippets, so don't break the formatting.
                #This is dumb, but it works.
                prints.append(doc)

            else:
                #Fix up the line breaks by breaking the string in to paragraphs (denoted by a double space,) removing the
                #line breaks inside the paragraphs, and then re-adding linebreaks with the textwrap lib
                #This doesn't handle paragraphs broken with more than 2 \n-s right, but fuck it
                paragraphs = [theParagraph.replace("\n", " ") for theParagraph in doc.split("\n\n")]
                paragraphs = [textwrap.fill(theParagraph, width = 78) for theParagraph in paragraphs]

                prints.append("\n\n".join(paragraphs))
        else:
            prints.append("Object has no docstring.")
        prints.append("")

        if not printspecial:
            prints.append("Not printing 'special' attributes or methods (those book-ended with '__'s;")
            prints.append("call info(your_obj, printspecial=True) to print those.")
            prints.append("")
        prints.append("Attributes:")
        attribs=inspect.getmembers(obj, lambda x: not inspect.isroutine(x))
        attribstrs=[]
        for thename, thevalue in attribs:
            if printspecial or thename[:2]!="__":
                valstr=str(thevalue)
                valstr=valstr.replace("\n"," ")
                if len(valstr) > 45:
                    valstr=valstr[:45]
                    valstr=valstr+"..."

                attribstrs.append("{} : {}".format(thename,valstr))

                if self.enforcedattrs:
                    try:
                        #Enforcedattrs is an attribute that is set by the level; it maps hashes of objects to tuples of
                        #attribute names; these are all attributes that are enforced on that object by an AttributeEnforcer.
                        if thename in self.enforcedattrs[hash(obj)]:
                            attribstrs.append("^ ALTERATION BLOCKED BY ATTRIBUTEENFORCER")
                    except KeyError:
                        pass

        if attribstrs:
            prints.extend(attribstrs)
        else:
            prints.append("Object has no attributes.")

        prints.append("")
        prints.append("Methods:")
        methods=inspect.getmembers(obj, inspect.isroutine)
        methodstrs=[]
        for thename, thevalue in methods:
            if (printspecial or thename[:2]!="__"):
                docstr=inspect.getdoc(thevalue)
                if not docstr:
                    docstr="(method has no docstring)"
                docstr=docstr.replace("\n"," ")
                if len(docstr) > 55:
                    docstr=docstr[:55]
                    docstr=docstr+"..."
                methodstrs.append("{} : {}".format(thename,docstr))
        if methodstrs:
            prints.extend(methodstrs)
        else:
            prints.append("Object has no methods.")

        print("\n".join(prints))
info=InfoObj()

class HintObj:
    "Callable that delivers Invisiclues-style hints to the player."

    def __init__(self,hintlist):
        self.hintlist=hintlist
        self.hintindex=1

    def __call__(self):
        if not self.hintlist:
            print("Sorry, there's no hints yet for this level.")
            return

        for t in range(0,min(len(self.hintlist),self.hintindex)):
            print(self.hintlist[t].replace("            ",""), "\n")

        if self.hintindex == len(self.hintlist):
            print("No more hints.")
        else:
            self.hintindex+=1

    def __repr__(self):
        return "Enter hint() to get a hint on how to solve this level."

class OptionsObj:
    """
    An object that gets instantiated by the Level's start() and is inserted into
    injection console namepsace at play-time. It's used by the player to
    configure the game, i.e. turn the music on and off, change the save dir, all that.

    .version is simply the version string of the game.
    .telemetry_enabled controls the sending of game transcripts.
    .music_disabled controls whether or not the music is playing.
    .music_volume is a number from 0 to 1 that controls the music volume.
    .save_location is the full path to the directory you want your save files in
    .fullscreen makes the game fullscreen when True, and windowed when False.
    .high_contrast_dialogue being True makes NPC dialogue display in black on white
    text; handy if the normal colors are too hard to see.


    Note that saves in a different directory won't actually _load_;
    you'll have to use the -save_directory command line flag.
    """
    def __init__(self):
        pass

    def __repr__(self):
        return "Type info(options) to see the list of options. Change settings by setting this object's attributes."

    def __getstate__(self):
        state = self.__dict__
        state["_music_volume"] = self.music_volume
        return state

    def __setstate__(self, state):
        self.music_volume = state["_music_volume"]
        del state["_music_volume"]
        self.__dict__ = state

    @property
    def version(self):
        return main._version
    @version.setter
    def version(self,x):
        raise AttributeError("Not configurable")

    @property
    def telemetry_enabled(self):
        return main._telemetry
    @telemetry_enabled.setter
    def telemetry_enabled(self,x):
        if not isinstance(x,bool):
            raise ValueError("Non-boolean (not true or false) value {0} provided to boolean option".format(x))
        main._telemetry=x

    @property
    def music_volume(self):
        return pygame.mixer.music.get_volume()
    @music_volume.setter
    def music_volume(self, x):
        pygame.mixer.music.set_volume(x)

    @property
    def save_location(self):
        return main._savedir
    @save_location.setter
    def save_location(self,x):
        if not isinstance(x,str):
            raise ValueError("Non-string value {0} provided to string option".format(x))
        main._save_location=x
        print("Please note that you'll have to use the -save_location command line flag to specify the new save directory in order to load games saved in that directory.")

    @property
    def fullscreen(self):
        return bool(pygame.display.get_surface().get_flags() & pygame.FULLSCREEN)
    @fullscreen.setter
    def fullscreen(self,x):
        if x != self.fullscreen:
            #Safe fullscreen toggle code lifted directly from philhassey, illume, and Duoas of pygame.org
            #http://pygame.org/wiki/toggle_fullscreen
            screen = pygame.display.get_surface()
            tmp = screen.convert()
            caption = pygame.display.get_caption()
            cursor = pygame.mouse.get_cursor()  # Duoas 16-04-2007

            w, h = screen.get_width(), screen.get_height()
            flags = screen.get_flags()
            bits = screen.get_bitsize()

            pygame.display.quit()
            pygame.display.init()

            screen = pygame.display.set_mode((w, h), flags ^ pygame.locals.FULLSCREEN, bits)
            screen.blit(tmp, (0, 0))
            pygame.display.set_caption(*caption)

            pygame.key.set_mods(0) #HACK: work-a-round for a SDL bug??

            pygame.mouse.set_cursor(*cursor)  # Duoas 16-04-2007

    @property
    def high_contrast_dialogue(self):
        return main._high_contrast
    @high_contrast_dialogue.setter
    def high_contrast_dialogue(self,x):
        main._high_contrast=x
        if main._high_contrast:
            print("NPC dialogue will now be in black-on-white text.")
        else:
            print("Regular NPC dialogue colors restored.")

class InjectionConsoleCursor(pc.InputCursor):

    def __getstate__(self):
        mydict = self.__dict__.copy()
        del mydict["terminal"]
        return mydict

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.command_buffer = [""]
        self.command_buffer_pos = 0
        self.drawn_at = None
        self.terminal.hooks.register_all_on(self)
        self.glyph = ""
        self.fgcolor="grey95"
        self.old_bgcolor = None
        
    def on_scroll(self, scrollvec):
        super().on_scroll(scrollvec)

    def _process_keys(self, keypresses):
        super()._process_keys(keypresses)
        #@TODO: Figure out why command_buffer gets deleted...
        if not self.command_buffer:
            self.command_buffer = [""]
            #This print statement was causing problems, getting printed to the console somehow.
            #This code is a cluster.
            #print("Command buffer not initialized?!?", file=sys.__stdout__)

        oldbufflen = len(self.buffer)
        command_buffer_changed = False

        for thepress in keypresses:
            command_buffer_changed = True #we set it to false later if an arrow key isn't pressed
            if thepress.key == "down":
                self.command_buffer_pos += 1
                if self.command_buffer_pos >= len(self.command_buffer):
                    self.command_buffer_pos = 0
                self.buffer = list(self.command_buffer[self.command_buffer_pos])
                self.bufferposition = len(self.buffer)

            elif thepress.key == "up":
                self.command_buffer_pos -= 1
                if self.command_buffer_pos <= -len(self.command_buffer):
                    self.command_buffer_pos = len(self.command_buffer) - 1
                self.buffer = list(self.command_buffer[self.command_buffer_pos])
                self.bufferposition = len(self.buffer)

            elif thepress.ctrl and thepress.key == "v":
                self.command_buffer_pos = 0
                self.buffer = list(pyperclip.paste())
                self.bufferposition = len(self.buffer)

            elif thepress.ctrl and thepress.key == "x":
                pyperclip.copy(''.join(self.buffer))
                self.buffer = list()
                self.bufferposition = len(self.buffer)

            elif thepress.ctrl and thepress.key == "c":
                pyperclip.copy(''.join(self.buffer))
                command_buffer_changed = False

            else:
                command_buffer_changed = False

        if command_buffer_changed:
            newbufflen = len(self.buffer)
            difference = oldbufflen - newbufflen
            if difference > 0:
                self.cleanupcount = difference
            self.pos = self.start_pos
            self.advance(newbufflen)

    def _newline(self):
        if self.currently_getting_line:
            command = ''.join(self.buffer).strip("\n")
            if not self.command_buffer or self.command_buffer[-1] != command and\
                    command and command != ";":
                self.command_buffer.append(command)
            self.command_buffer_pos = 0
        super()._newline()

    def before_draw(self):
        if self.currently_getting_line:
            try:
                cell = self.terminal[self.x, self.y]
            except IndexError:
                return
            if cell.character in  (' ', '', None):
                cell.character = self.cursorchar
                self.old_bgcolor = None
            else:
                cell.bgcolor = "grey50"
                self.old_bgcolor = cell.bgcolor
            self.drawn_at = self.pos

    def after_draw(self):
        if self.old_bgcolor:
            self.terminal[self.drawn_at].bgcolor = self.old_bgcolor
            self.old_bgcolor = None
        else:
            if self.currently_getting_line:
                try:
                    self.terminal[self.drawn_at].character=" "
                except IndexError:
                    #it went off the screen, so no need to wipe it
                    pass

    def delete(self):
        self.terminal.cursors.remove(self)
        self.terminal.hooks.unregister_all_on(self)

    def replace_window(self, term):
        self.terminal = term
        self.terminal.cursors.append(self)
        self.terminal.hooks.register_all_on(self)

class InjectionConsole(code.InteractiveConsole):
    """Modified version of code.interactiveconsole's interactive mode emulator; uses pygcurse IO. This gets stuck to a gameobjs.Player
    when the level starts so the player can have a nice interface to 'hack' the level with."""

    def __init__(self,mylocals,term):
        self.term=term.terminal #type: pc.Terminal
        self.cursor = InjectionConsoleCursor(term.terminal, cursorchar="")
        self.asyncupdatethread=None
        self.processing=False #This is true while the asyncupdate thread is needed.
        super().__init__(mylocals)
        self.combuff=[]
        self.combuffpos=0
        self.backlog=""
        self.active=0
        self.transcript=[]
        self.translationtable=None
        self.dokeyfix=False
        self.wrappedinputtext=None
        self.textwrap = textwrap.TextWrapper(replace_whitespace=False, drop_whitespace=False, width=self.term.width-1)

        self.encoding = "UTF-8"

    def runcode(self, code):
        #We redirect stdout to... ourself, so that the code's output will go into the pygcurse window.
        #We have a write() function, which is apparently all you actually need to be a stdio replacement.
        oldstdout=sys.stdout
        oldstdin=sys.stdin
        oldstderr=sys.stderr
        sys.stdout=self
        sys.stdin=self
        sys.stderr=self
        super().runcode(code)
        #Then reset it. This isn't strictly necessary, but it's good form.
        sys.stdout=oldstdout
        sys.stdin=oldstdin
        sys.stderr=oldstderr

    def write(self, data):
        if self.active:
            self.cursor.blink = False
            cursorchar = self.cursor.cursorchar
            self.cursor.cursorchar = None
            #If the text is too long, we'll have to paginate it
            linecount=(len(data)//self.term.width)+data.count("\n")
            if linecount >= self.term.height:
                #If it really is too long, we first split it by line-break - we do this before wrapping to try to preserve
                #the original formatting as well as possible
                #Then we use textwrap to break up those lines; otherwise we might end up with more lines than we expected because of wrapping
                lines=[]
                linestoprocess=[]
                for theline in data.splitlines(keepends=True):

                    linestoprocess.append(theline)
                for theline in linestoprocess:
                    lines+=self.textwrap.wrap(theline)

                #Then we print out all the lines, a screen at a time
                linenum=0
                for theline in lines:
                    self.transcript.append(theline)
                    for thechar in theline:
                        self.cursor.smart_writechar(thechar)
                    linenum+=1+theline.count("\n")
                    #Once a screenful has been printed, we wait for an ENTER before printing the next
                    if linenum >= self.term.height-1:
                        for thechar in "\n---more---":
                            self.cursor.smart_writechar(thechar)
                        while not self.term.backend.get_keypresses(False):
                            self.term.process()
                            self.term.draw()
                            time.sleep(1/15)
                        for t in range(10):
                            self.cursor.advance(-1)
                        self.cursor.y -= 1
                        linenum=0
            else:
                for thechar in data:
                    self.cursor.smart_writechar(thechar)
                self.transcript.append(data)
                self.term.draw()
                self.term.process()
            self.cursor.cursorchar = cursorchar
            self.cursor.blink = True
        else:
            self.backlog += data

    #Compatibility stuff so that we can pretend to be an input object so input() and such work
    #Why ugly append \n thing? Because trailing newline character *must* be kept. according to python's document.
    def readline(self):
        return self.raw_input()+"\n"

    def raw_input(self,prompt=""):
        self.write(prompt)
        line = self.cursor.get_line().strip("\n")
        self.transcript.append(line)
        return line

    def flush(self,**params):
        "Just here to keep python happy if we quit while we're stdio."
        pass

    def interact(self, banner=None):
        """Closely emulate the interactive Python console.

        The optional banner argument specifies the banner to print
        before the first interaction; by default it prints a banner
        similar to the one printed by the real Python interpreter,
        followed by the current class name in parentheses (so as not
        to confuse this with the real interpreter -- since it's so
        close!).


        I had to copy-n-edit this whole thing so there was a way to exit
        the shell; pygcurse input doesn't support ^Z, so I just made
        semicolon-enter the break command instead.
        """
        self.active=True
        #We massively increase the delay before key repetition starts to make slow button-pressers happy
        pygame.key.set_repeat(300,75)
        self.cursor.blink=True
        self.cursor.cursorchar="▁" #This doesn't render in Fira Code for some reason, but it's U+13C6 LOWER ONE EIGHTH BLOCK
        self.term.blank()
        #Dump out the keypress that activated us
        self.term.backend.get_characters()

        #If we've been instructed to, call out to the non-US-ASCII key mapping fixer code
        #if self.dokeyfix:
            #main.build_keycode_translation_table()

        #We immediately flush any text we were given while inactive to the screen
        if self.backlog:
            self.write(self.backlog)
            self.backlog=""
        try:
            sys.ps1
        except AttributeError:
            sys.ps1 = ">>> "
        try:
            sys.ps2
        except AttributeError:
            sys.ps2 = "... "
        cprt = 'Type "info" for more information.\nTyping a statement or variable into the console and pressing ENTER\nwill print out its result or value.'
        if banner is None:
            self.write("Python %s on %s\n%s\nSemicolon then Enter to return to game\n" %
                       (sys.version, sys.platform, cprt))
        elif banner:
            self.write("%s\n" % str(banner))
        more = 0
        while 1:
            try:
                if more:
                    prompt = sys.ps2
                else:
                    prompt = sys.ps1
                try:
                    line = self.raw_input(prompt)
                except EOFError:
                    self.write("\n")
                else:
                    if line and line.strip()[-1]==";" and not more:
                        #Semicolon is the "get out of this shell" command.
                        self.active=False
                        self.more=0
                        self.cursor.blink=False
                        self.cursor.cursorchar=""
                        #And then set the repeat rate back when we're done
                        pygame.key.set_repeat(50,100)
                        return
                    else:
                        #And fire up the asynchronous update machinery (which stops the game from "stalling" when
                        #something the player entered takes a while)
                        self.asyncupdatethread=threading.Thread(target=self.asynchronous_update,daemon=True)
                        self.processing=True
                        self.asyncupdatethread.start()
                        more = self.push(line)
                        self.processing=False
                        self.asyncupdatethread.join()
                        self.asyncupdatethread=None
            except KeyboardInterrupt:
                self.write("\nKeyboardInterrupt\n")
                self.resetbuffer()
                more = 0

    def asynchronous_update(self):
        """Keep updating the display while the main game thread is working. This keeps the game from 'stalling' when
        the player puts in a command that will take some time to run (because otherwise it would stop pumping events),
        as well as making it so that prints get put on the IJ console screen as they're printed instead of once
        the prompt comes back."""
        while self.processing:
            self.term.draw()
            #pygame.display.update()
            #pygame.event.pump()
            time.sleep(0.066) #~1/15th of a second :)

    def delete(self):
        self.cursor.delete()

    def __getstate__(self):
        """See Pickle docs. We need this to get rid of our reference to the game window; they don't keep well. We also
        need to clear any functions or lambdas that are kicking around in console namespace, as well as modules the
        player imported.

        main.game_init puts all this back right after loading the save."""
        mydict=self.__dict__.copy()
        del mydict["term"]
        #The shadow dict's copy of locals needs to be a copy and not a reference too 'cos we'll be deleting stuff from it
        mydict["locals"]=self.locals.copy()
        for name, obj in self.locals.items():
            try:
                if inspect.ismodule(obj):
                    del mydict["locals"][name]
            except (RuntimeError, MemoryError):
                #Because Dill is slightly poopy, we may encounter recursion errors upon calling dill.pickles().
                #This error could attack at any time. So ve must deel with eet.
                del mydict["locals"][name]
        return mydict

    def replace_window(self, window):
        self.term = window.terminal
        self.cursor.replace_window(self.term)

class Playlist:
    """The object that manages level music. Each one holds a list of filenames of music files. Calling .play() on a
    Playlist causes it to continually randomly select music from its list. Calling .stop() causes it to stop the music.
    .update() needs to be called regularly, though normally this is handled by a separate thread. Calling .play() while
    a Playlist is active does nothing, but make sure to stop the last Playlist before starting another.

    The filenames in songlist should be just filenames; they are assumed to be in ./music/"""

    def __init__(self,songlist,name):
        #This is the base list of songs
        self.songlist=[os.path.join("music",fname) for fname in songlist]
        #This is set to a shuffled copy of songlist whenever it is empty; songs are popped from it without being replaced
        self.active_songlist=[]
        #We use the threading module to provide "fire and forget" music management.
        self.name=name
        self.thread=None
        self._active=False
        self.current_song=""

    def update(self):
        """This needs to be called periodically to keep the music running. Normally this gets called by a separate
        thread, spawned by .play()"""
        if not self.active_songlist:
            self.active_songlist=self.songlist.copy()
            random.shuffle(self.active_songlist)
            #Make sure we don't play the last song we played again
            try:
                self.active_songlist.remove(self.current_song)
            except ValueError:
                #If we get ValueError current_song wasn't in the list in the first place - no big deal
                pass
        if not pygame.mixer.music.get_busy():
            self.current_song=self.active_songlist.pop()
            if not main._distributable:
                print(self.current_song,file=sys.__stdout__)
                print(self.name,file=sys.__stdout__)
            pygame.mixer.music.load(self.current_song)
            try:
                pygame.mixer.music.play()
            except pygame.error:
                pass

    def _music_loop(self):
        while self._active:
            try:
                self.update()
            except Exception as e:
                warnings.warn(''.join(traceback.format_exception(*sys.exc_info())))
            time.sleep(0.5)
        pygame.mixer.music.fadeout(2000)

    def play(self):
        if self._active:
            return
        self._active=True
        try:
            self.thread=threading.Thread(name="Music ({0})".format(self.name),target=self._music_loop,daemon=True)
            self.thread.start()
        except RuntimeError:
            warnings.warn("Tried to .play() a playlist that was already running.", RuntimeWarning)

    def stop(self):
        if not self._active:
            warnings.warn("Tried to .stop() a playlist that was already stopped.", RuntimeWarning)
            return
        self._active=False
        self.thread.join()
        self.thread=None

    def __getstate__(self):
        #playlists shouldn't be allowed to persist across saves anyway,
        #but we need to kill our link to our thread so we don't gum up the pickling
        #We also set ourself to be inactive just in case
        serializable_dict=self.__dict__.copy()
        del serializable_dict["thread"]
        serializable_dict["_active"]=False
        return serializable_dict

playlists={"Calm": Playlist(["Seazo - Flush.ogg","Sara Dee Laek - E2T4.ogg",
                             "Arbeitsheld - Gormandize.ogg",
                             "Mop Cortex - Bleeding Brain.ogg","Nebbiolo - Come With Us.ogg",
                             "Atw - Git n' Glitch.ogg","Lukal - Glitch.ogg",
                             "Brainstem - Mixdown.ogg","Atsub - Afterdark.ogg","Brainstem - Infoleak.ogg"],"Calm"),

           "Intense": Playlist(["Arbeitsheld - Square.ogg","Crazy Clan - Atmosphere of Mad Reason.ogg",
                                "Dj Mar.S. Aka One Milk - Malfunction.ogg",
                                "Brainstem - Badchoice.ogg","Brainstem - Mixdown.ogg","Jonas Niemann - Fressmaschinen Prolog.ogg",
                                "Noqturne - Science - Hell Yeah.ogg","Peter Saar - Cornkroncsf4.ogg","Retrospecter - In the Rainforest.ogg",
                                "Xndl - Extraterrestrisch.ogg","Clozee - Silver Wound.ogg", "Extrapool - Antherosclerosis At 30.ogg", "Subterrestrial - Capacitor.ogg"],"Intense"),

           "Super-intense": Playlist(["Nebulist - Drillin' the Difference.ogg",
                                      "Sij - Crystal Oscillator feat. thetou (Remix by Deception Cost).ogg",
                                      "Kiyo - Brown Deepsea Flight.ogg","Lalo - Crackle.Ar.ogg","Muepetmo -  -- Tema 9 -.ogg",
                                      "Sam Dudley - Glitch In The Wheel.ogg",
									  "Nocturnes & Dreamscapes - Reflection.ogg"],"Super-intense")}

class Hooks:
    """A simple event dispatch system for Levels-s. """

    def __init__(self):
        self.backing = collections.defaultdict(list)

    def __repr__(self):
        lines=[]
        for thehook in self.backing:
            lines.append(f"{thehook}:")
            for thecback in self.backing[thehook]:
                lines.append(f"\t{thecback}")

    def fire(self, hook, *args):
        """Call all registered callbacks for the given hook."""
        for theobj, methodname in self.backing[hook]:
            getattr(theobj, methodname)(*args)

    def clear(self):
        """Unregister all hooks."""
        for thelist in self.backing.values():
            thelist.clear()

    def register(self, object, hook, methodname = None):
        """Call the method on object when hook is fired. If methodname is not provided, it defaults to the name of
        the hook."""
        if methodname is None: methodname = hook
        if (object, methodname) not in self.backing[hook]:
            self.backing[hook].append((object, methodname))

    def unregister(self, object, hook):
        """Stop receiving calls for the given hook on the given object."""
        try:
            self.backing[hook] = [(o, h) for o, h in self.backing[hook] if o is not object]
        except ValueError:
            pass

class Level:
    "A level of the game. Contains all the objects on a level."

    def __init__(self,name,objlist,plyobj,music=None,extralocals={},hints=[],unimportant=None):
        self.name=name
        self.objlist=objlist
        self.safe_objlist=[]
        self.plyobj=plyobj #type: gameobjs.Player
        self.plystartpos=plyobj.pos
        self.music=music
        self.extralocals=extralocals #Extralocals maps variable names to objects; these are pushed into the ij console's namespace
        self.starttime=None
        self.hints=hints
        if unimportant is None:
            unimportant=set()
        self.unimportant=unimportant #This is a hint to check_winnable that objects contained here are okay to destroy
        self.important_obj_destroyed=False
        self.active=False
        self.unwinnable_hint_countdown=75 #We wait 5 seconds (5s * 15fps = 75 frames) before showing the reset hint
        self.to_cleanup=[] #list of objs that need their .cleanup() called 'cos they've been .remove_obj()ed
        #We have a 3-frame "cooldown" after we send a graphics error report that gets reset every time we encounter a
        #graphics error, so hopefully one incidence of error will result in 1 report
        self.gfxerrorcooldown=0
        self.displaynowplayingtimer=0 #This ticks down while the "now playing" text is on screen, when it is 0 it clears
        self.autosavecountdown = -1
        self.consolelockedtimer=0 #Ticks down while "[CONSOLE LOCKED]" is on screen, and stops drawing it when its 0
        self.consolelockedmessage="[INJECTION CONSOLE LOCKED BY ADMINISTRATOR]"
        # TODO: rework all the above crap to work on the timers system
        #list of tuples of (frames_remaining, call_when_time_out)
        self.timers = [] #type: list[list[int, callable]]
        self.hooks = Hooks()
        self.currentsongreadablename = ""

    def draw(self,pygcursewindow):
        "Call obj.draw on all objects in objlist and the plyobj."
        if self.gfxerrorcooldown != 0: #This ticks down every time draw() is called and is set to 3 whenever a graphics
            self.gfxerrorcooldown-=1 #error report is sent. If it's nonzero another report isn't sent.

        for theobj in self.safe_objlist:
            try:
                theobj.draw(pygcursewindow)
            except Exception:
                try:
                    pygcursewindow.terminal[theobj.int_pos()].character = "‼"
                    pygcursewindow.terminal[theobj.int_pos()].fgcolor   = "red"
                except AttributeError:
                    #If the user puts something invalid in objlist then obviously it won't HAVE int_pos
                    pass
                finally:
                    if main._distributable:
                        if not self.gfxerrorcooldown:
                            main.send_error_report("Graphics error")
                        self.gfxerrorcooldown=3
                    else:
                        traceback.print_exc(file=sys.__stderr__)


        for theobj in self.objlist:
            try:
                theobj.draw(pygcursewindow)
            except Exception:
                try:
                    pygcursewindow.terminal[theobj.int_pos()].character = "‼"
                    pygcursewindow.terminal[theobj.int_pos()].fgcolor   = "red"
                except AttributeError:
                    pass
                finally:
                    if main._distributable:
                        if not self.gfxerrorcooldown:
                            main.send_error_report("Graphics error")
                        self.gfxerrorcooldown=3
                    else:
                        traceback.print_exc(file=sys.__stderr__)

        self.plyobj.draw(pygcursewindow)

        if not self.unwinnable_hint_countdown:
            pygcursewindow.put_line_at("You can press R at any time to restart a level",(15,29),bgcolor="white",
                                       fgcolor="black")

        if self.consolelockedtimer > 0:
            term = pygcursewindow.terminal
            for cell in term[(0,6):(term.width, 7)]:
                cell.bgcolor="red3"
                cell.character=" "
            pygcursewindow.center_line_at(" "+self.consolelockedmessage+" ",
                             (0,6), "topcenter", bgcolor="red4" if (
                        self.consolelockedtimer % 15 < 15//2) else "red3", fgcolor="white" )
            self.consolelockedtimer-=1

        if self.displaynowplayingtimer and not main._music_disabled:
            green = min(255, int(math.log10(self.displaynowplayingtimer) * 100))
            self.displaynowplayingtimer-=1
            #This character is the "now playing:" in Fixedsys Excelsior
            pygcursewindow.put_line_at("", (1,27), fgcolor=(0,green,0))
            pygcursewindow.put_line_at("{0}".format(self.currentsongreadablename), (1,28),  fgcolor=(0,green,0))

    def p_draw(self,surface, mainwin):
        "Call obj.p_draw on all objects in objlist and the plyobj. This does the pixel-based (Pygame) graphics effects."
        for theobj in self.safe_objlist + self.objlist:
            try:
                theobj.p_draw(surface, mainwin)
            except Exception:
                try:
                    mainwin.terminal[theobj.int_pos()].character = "‼"
                    mainwin.terminal[theobj.int_pos()].fgcolor   = "red"
                except Exception:
                    pass
                finally:
                    if not self.gfxerrorcooldown:
                        main.send_error_report("Pixel graphics error")
                        if not main._distributable:
                            traceback.print_exc(file=sys.__stderr__)
                    self.gfxerrorcooldown = 3

    def update(self,gamestate):
        "Call obj.update on all objects in oblist and the plyobj."

        #Gameobj updates do the stdout redirect trick too now
        oldstdout=sys.stdout
        sys.stdout=self.plyobj.injectionconsole

        #Oh, and before we do anything else we should check to see if the player has turned the music off.
        if main._music_disabled and gamestate.current_playlist: #if the music IS disabled also check if there's actually music...
            self.stop_music(gamestate)

        for theobj in [self.plyobj]+self.objlist+self.safe_objlist:
            try:
                theobj.update(gamestate)
                #If a change_level has happened, we don't want to continue updating objects!
                if not self.active:
                    return
            except Exception:
                #Exceptions in a gameobj's code shouldn't crash the game; they should be printed to the injection console.
                #We only want the first two parts of exc_info printed; the traceback is meaningless
                #to the player. format_exception_only returns a list for god knows what reason, so that's why there's a [0]
                excstr=traceback.format_exception_only(*sys.exc_info()[:2])[-1]
                #We also don't want to spam the console with the same exception over and over, so we check
                #to see if it's already there first
                if excstr not in self.plyobj.injectionconsole.backlog:
                    print(excstr)
                #and an alert printed at the bottom of the screen
                gamestate.mainwin.put_line_at("Exception detected - See injection console for details.", (3, 29), fgcolor="red")
                if not main._distributable:
                    traceback.print_exc(file=sys.__stderr__)

        while self.to_cleanup:
            theobj=self.to_cleanup.pop()
            theobj.cleanup(gamestate)

        if self.check_unwinnable():
            if self.unwinnable_hint_countdown:
                self.unwinnable_hint_countdown-=1


        #If the song the playlist is currently playing is not the song it was playing last time we checked,
        #it must've changed, and so we should display the "now playing" message for some number of seconds
        if (not main._music_disabled) and self.music and (gamestate.currentsong != self.music.current_song or not self.currentsongreadablename):
            gamestate.currentsong=self.music.current_song
            songname=os.path.split(gamestate.currentsong)[1][:-4]
            gamestate.currentsongreadablename=self.currentsongreadablename=songname
            self.displaynowplayingtimer=max(self.displaynowplayingtimer, 150) #10s

        #Update the info() object's knowledge of Enforcer-monitored attributes.
        info.enforcedattrs=self.get_enforced_attributes()

        #And save if we have to.
        if self.autosavecountdown == 0:
            gamestate.save()
            #Also autosave every 15 minutes.
            self.autosavecountdown = 13500
        else:
            self.autosavecountdown-=1

        #And finally, process timers.
        timed_out = []
        for idx, entry in enumerate(self.timers):
            frames_left, cback = entry
            frames_left -= 1
            if frames_left:
                entry[0] = frames_left
            else:
                timed_out.append(idx)
                cback()

        for idx in reversed(timed_out):
            del self.timers[idx]

        sys.stdout=oldstdout

    def start(self,gamestate, prevplyobj = None):
        "Called when this level becomes the current level, before its first update()"
        consolelocals={"objs":self.objlist,"info":info,"hint":HintObj(self.hints),"options":OptionsObj()}
        consolelocals.update(self.extralocals)
        #TODO: There's got to be a better way to do this. I think I want one contiguous injection console
        #(and probably one contiguous Player); that'll solve this.
        #it would be better to have a Level.stop() that the gamestate calls, but this is good enough for now.
        if prevplyobj: prevplyobj.injectionconsole.delete()
        self.plyobj.injectionconsole=InjectionConsole(consolelocals,gamestate.mainwin)
        self.active=True

        self.start_music(gamestate)

        gamestate.mainwin.blank()
        gamestate.mainwin.center_line_at(self.name, "topcenter", (0,5))
        gamestate.mainwin.wait()
        self.starttime=datetime.datetime.now()

        #Do an "ascii codes counting down" effect on the level name
        namel = [ord(c) for c in self.name]
        subtractors = [(c - 32) // 5 for c in namel]
        for t in range(6):
            namel = [max(32, c - x) for c, x in zip(namel, subtractors)]
            animatedname = ''.join([chr(c) for c in namel])
            gamestate.mainwin.center_line_at(animatedname, "topcenter", (0, 5))
            gamestate.mainwin.terminal.draw()
            time.sleep(1 / 30)

        gamestate.mark_current_level_revisitable()

        gamestate.add_fx(fx.chromatic_aberration,4,intensity=3)

        #There are issues with saving inside the change_level so we delay until a few updates in.
        self.autosavecountdown=15

    def start_music(self,gamestate,loadedsave=False):
        """Start the music playing (and update the associated bookkeeping.)
        Usually called by start() (the "level is about to be played" method.) Also gets called by main.game_init()
        when a save gets loaded. If main._music_disabled is True this returns immediately."""
        #If music is disabled, well...
        if main._music_disabled:
            return
        #If we've got the same playlist as last level, and we haven't just loaded a save nothing needs doing.
        #If we HAVE just loaded a save then we still need to start the jams
        if (not loadedsave) and gamestate.current_playlist == self.music:
            return
        #Otherwise, kill the old music (if there is any)...
        if gamestate.current_playlist:
            gamestate.current_playlist.stop()
        #And start the new one!
        self.music.play()
        self.currentsong=None
        #And make sure to update gamestate's reference
        gamestate.current_playlist=self.music

    def stop_music(self,gamestate):
        """Stop the music playing (and update the associated bookkeeping.)"""
        if gamestate.current_playlist:
            gamestate.current_playlist.stop()
        gamestate.current_playlist=None

    def get_at(self,pos,include_player=False,exclude=()):
        """Return the gameobj at a given position, unless it's the player and include_player isn't true.
        Returns None if there is no obj there. WILL screw up if there are multiple non-player objects in a square.
        Exclude is a tuple of objects or object classes that will be ignored - for example, the teleporter object doesn't
        want to teleport itself."""
        assert isinstance(include_player,bool),"include_player has been set to a non-bool. Did you forget the parentheses around the pos argument?"

        if include_player:
            if self.plyobj.pos == pos:
                return self.plyobj

        for theobj in self.objlist + self.safe_objlist:
            #isinstnace will get pissed if exclude isn't a tuple of types so we check that first
            try:
                if isinstance(theobj,tuple(x for x in exclude if isinstance(x, type))):
                    continue
            except TypeError:
                pass

            if getattr(theobj, 'int_pos', None) and theobj.int_pos() == pos and theobj not in exclude:
                return theobj

    def remove_obj(self,theobj):
        """Remove the selected object from our lists. It's okay to pass this function objects that aren't actually
        in the level, though that's a stupid thing to do so don't do it anyway. WILL raise TypeError if you try
        to pass in the player, though."""
        if self.active and (theobj not in self.unimportant and not isinstance(theobj,gameobjs.incidental_objs)):
            self.important_obj_destroyed=True
            if not main._distributable:
                print(theobj,"was destroyed! Level may be unwinnable!",file=sys.__stdout__)

        try:
            self.objlist.remove(theobj)
        except ValueError:
            pass
        else:
            self.to_cleanup.append(theobj)
            return

        try:
            self.safe_objlist.remove(theobj)
        except ValueError:
            pass
        else:
            self.to_cleanup.append(theobj)
            return

        if theobj is self.plyobj:
            raise TypeError("Tried to remove_obj() the player. Don't do that.")

    def check_unwinnable(self):
        """First off, this is NOT intended to actually determine if a level is winnable or not.
        What it IS for is to take some educated guesses about whether or not the level has been ruined by sending an
        object off the screen or destroying it somehow. Returns true if it is. Update() uses this to draw a tooltip
        telling the player how to reset the level."""
        #If any important objects have been destroyed then yeah, something's wrong
        if self.important_obj_destroyed:
            return True

        #If any important objects that the player doesn't have access to have left the screen, that's also bad.
        for obj in (obj for obj in self.safe_objlist if (not  0 < obj.pos[0] < 80) and (not  0 < obj.pos[1] < 40)):
            if obj not in self.unimportant:
                return True

        return False

    def get_enforced_attributes(self):
        """
        Return a dict mapping hashes of GameObjects to string attribute names; these hash-attribute pairs represent all
        the attributes and the objects they're on that are currently monitored by an AttributeEnforcer. This is pretty
        much only meant to be used for the InfoObj's functionality where it warns you when an attribute is being watched
        by an AttributeEnforcer. The Level calls this function and then gives the dict to the InfoObj, where it can be
        used to identify objects but not actually reveal the object itself to the player.
        """
        enforcers=[theobj for theobj in self.objlist+self.safe_objlist if isinstance(theobj,gameobjs.AttributeEnforcer)]
        attribdicts=[theobj.attribdict for theobj in enforcers]
        result=collections.defaultdict(list)
        for thedict in attribdicts:
            for k, v in thedict.items():
                #The .attribdict maps 2-tuples of (GameObj, enforced_attribute) to the enforced value.
                result[hash(k[0])].append(k[1])

        return {k: tuple(v) for k, v in result.items()}

    def timer(self, frames, cback, *args, **kwargs):
        """Call cback frames .update() calls from now, after all objects get their .update(), with the given
        positional & keyword arguments."""
        self.timers.append([frames, functools.partial(cback, *args, **kwargs)])

    def __getstate__(self):
        retdict=self.__dict__.copy()
        #We want to reference the actual Playlist objects that are initialized at import-time and not an unpickled one
        #so we just replace the playlist with its name...
        try:
            retdict["music"]=self.music.name
        except AttributeError:
            retdict["music"]=None
        return retdict

    def __setstate__(self, statedict):
        self.__dict__.update(statedict)
        #... And then look up the actual playlist object when we're unpickled.
        if self.music:
            self.music=playlists[self.music]

def wall_line(start,direction,count):
    """Return a list of Wall objects. The first will be in a position determined by the start parameter (as an x,y tuple)
    The rest will be in start + ( dir * 0 to count)"""
    startx, starty = start
    dirx, diry = direction
    retlist=[]
    for t in range(0,count):
        ofstx=startx+(dirx*t)
        ofsty=starty+(diry*t)
        retlist.append(gameobjs.Wall((ofstx,ofsty)))
    return retlist

def wall_room(start, size):
    "Return a list of Wall objects forming a square box. The upper left corner will be at start, and the walls will be size walls long"
    startx, starty = start
    retlist=(
             wall_line(start,(0,1),size)+
             wall_line(start,(1,0),size)+
             wall_line((startx+size,starty),(0,1),size)+
             wall_line((startx,starty+size),(1,0),size+1)
             )
    return retlist

#LEVEL 0 : SLEEPING IN THE DIR(T)
#Player hopefully learns how to use dir() to find locals and attributes
#Either set the keypad's code to something known, set the gate to be open,
#move the gate, or just move the levelporter inside the room. A wealth of options.
level0=Level("LEVEL 0: SLEEPING IN THE DIR()T",[],gameobjs.Player((10,10)),playlists["Calm"])
#We show the now playing for 45s instead of 10s on lv 1 so the player doesn't get freaked out by seeing a UI element
#pop up and then disappear
level0.displaynowplayingtimer = 675
gate=gameobjs.Gate((14,12))
level0.objlist= [gameobjs.Lvl1Keypad((13,10),gate)] #,gameobjs.Water((11,11))
level0.safe_objlist=wall_room((9,9),5)
level0.safe_objlist.remove(level0.get_at((14,12)))
level0.safe_objlist.append(gate)
level0.safe_objlist.append(gameobjs.Levelporter((15,12),"Shortest"))
tutorial=gameobjs.TutorialDisplaySystem()
level0.safe_objlist.append(tutorial)
level0.extralocals={"tutorial":tutorial}
level0.hints=["""You know about dir() and info() already, right?
              Calling dir() gives you a list full of all the names you can use.
              Calling info(name) gives you a bunch of information about whatever the given
              name means. And just entering the name of something on its own tells you what
              it is, and if it's a sequence it tells you what's in it.

              Oh, and you can get stuff out of a list by putting a number in square brackets after it.
              Like this:
              >>> mylist
              [1, 2, 3]

              >>> mylist[0]
              1

              >>> mylist [2]
              3

              Don't be afraid to use hints; this game is hard! You can enter hint() again for another hint.
              \nGenerally the hints give more and more specific advice as you go down the list.""",
              "= makes the name on the left side of it mean whatever's on the right side of it.",
              "And putting double-quotes (\") around some text means 'literally this text'. So mytext = \"bla bla\" means 'make mytext a name for the words \"bla bla\"'",
              "So enter objs to see what's in it...",
              "Then enter info(objs[0]) to learn what a 'Lvl0Keypad' is...",
              "And then those 'attributes' are names you put after the main name and a dot. So you'd talk about objs[0]'s keyphrase attribute by entering objs[0].keyphrase",
              "So the keypad, when you bump into it, asks you for a 'keyphrase'. And the keypad itself has a .keyphrase attribute. Maybe try changing that?",
              "The next hint is the complete solution.",
              """Enter objs[0].keyphrase="open says me" . Exit the injection console by entering ;.
              Walk into the little square in the corner of the room.
              Type:
                  open says me
              and press Enter. Note that the door has opened. Walk through it into the levelporter."""
              ]

#LEVEL 1 : A POCKET FULL OF .POS IES
#Player realizes objects can be moved by setting their pos
#Move the levelporter a few squares over to where the player is. Simple.
level1=Level("LEVEL 1: A POCKET FULL OF .POS IES",[],gameobjs.Player((11,11)),playlists["Calm"])
level1.objlist=[gameobjs.Levelporter((16,11),"Shmokoban")]
level1.safe_objlist=wall_room((10,10),5) + wall_room((15,10),5)
redasgoldenrod=gameobjs.NonPlayerCharacter("G",(16,-2),"goldenrod")
redasgoldenrod.queue_many_actions([
    (redasgoldenrod.move,(0,1),level1),
    (redasgoldenrod.move,(0,1),level1),
    (redasgoldenrod.move,(0,1),level1),
    (redasgoldenrod.move,(0,1),level1),
    (redasgoldenrod.move,(0,1),level1),
    (redasgoldenrod.move,(0,1),level1),
    (redasgoldenrod.move,(0,1),level1),
    (redasgoldenrod.move,(0,1),level1),
    (redasgoldenrod.move,(0,1),level1),
    (redasgoldenrod.say,"SO! You have escaped my inescapable prison!"),
    (redasgoldenrod.move,(1,1),level1),
    (redasgoldenrod.say,"Harrumph!",1.5),
    (redasgoldenrod.say,"You'll not escape this one. There shall be no breaching that wall!",5,False),
    (redasgoldenrod.wait,3),
    (redasgoldenrod.move,(-1,0),level1), (redasgoldenrod.move,(-1,0),level1), (redasgoldenrod.move,(-1,0),level1), (redasgoldenrod.move,(-1,0),level1),
    (redasgoldenrod.move,(-1,0),level1), (redasgoldenrod.move,(-1,0),level1), (redasgoldenrod.move,(-1,0),level1), (redasgoldenrod.move,(-1,0),level1),
    (redasgoldenrod.move,(-1,0),level1), (redasgoldenrod.move,(-1,0),level1), (redasgoldenrod.move,(-1,0),level1), (redasgoldenrod.move,(-1,0),level1),
    (redasgoldenrod.move,(-1,0),level1), (redasgoldenrod.move,(-1,0),level1), (redasgoldenrod.move,(-1,0),level1), (redasgoldenrod.move,(-1,0),level1),
    (redasgoldenrod.move,(-1,0),level1),(redasgoldenrod.move,(-1,0),level1),(redasgoldenrod.move,(-1,0),level1),
    (redasgoldenrod.move,(-1,0),level1),
])
level1.safe_objlist.append(redasgoldenrod)
level1.hints=["Boy, what's THAT guy's problem? Remember to start off with dir() and objs to see what's available.",
              "The objective is to get into that triangle-looking thing, the levelporter.",
              "But you can't get to it, cos there's a wall in the way. Maybe it can come to you?",
              "Don't forget to info() it either.",
              "It has a .pos attribute.",
              "'pos' is short for position.",
              "And those numbers represent how far from the upper left corner the object should be; the first number represents left and right, the second represents up and down.",
              "The next hint is the complete solution.",
              "Enter objs[0].pos=(12,12). Walk into the levelporter."]

#LEVEL 2 : A HARSH MISTRESS
#Player learns how boulders work; they move according to their gravity attribute
#Player also learns how pressureplates work; they trip whenever something is on them.

class Level2(Level):
    def earlycompletioncheck(self):
        return self.plyobj.pos[1] > 10

level2=Level2("LEVEL 2: SHMOKOBAN / A HARSH MISTRESS",[],gameobjs.Player((15,5)),playlists["Calm"])
level2.objlist=[gameobjs.Boulder((21,12))]
gate1=gameobjs.Gate((15,8))
gate2=gameobjs.Gate((15,22))
level2.safe_objlist=(
                     wall_room((13,22),4)+
                     wall_room((13,4),4)+
                     wall_line((14,8),(0,1),14)+
                     wall_line((16,8),(0,1),14)+
                     wall_line((20,8),(0,1),14)+
                     wall_line((22,8),(0,1),14)+
                     [gameobjs.Wall((21,7)),gameobjs.Wall((21,22))]+
                     [gameobjs.PressurePlate((21,8),gate1),gameobjs.PressurePlate((21,21),gate2)]+
                     [gameobjs.Levelporter((15,24),"Grabitty")]
                     )
level2.safe_objlist.remove(level2.get_at((15,8)))
level2.safe_objlist.remove(level2.get_at((15,22)))
redasgoldenrod=gameobjs.NonPlayerCharacter("G",(26,-2),"goldenrod")
redasgoldenrod.queue_many_actions([
    (redasgoldenrod.move,(0,1),level2),
    (redasgoldenrod.move,(0,1),level2),
    (redasgoldenrod.move,(0,1),level2),
    (redasgoldenrod.move,(0,1),level2),
    (redasgoldenrod.move,(0,1),level2),
    (redasgoldenrod.move,(0,1),level2),
    (redasgoldenrod.move,(0,1),level2),
    (redasgoldenrod.move,(0,1),level2),
    (redasgoldenrod.move,(0,1),level2),
    (redasgoldenrod.say,"AHA! You see, each exit merely leads to ANOTHER CELL!"),
    (redasgoldenrod.say,"HOW INGENIOUS!!!"),
    (redasgoldenrod.say,"And there is truly NO way to escape this one!"),
    (redasgoldenrod.say,"What will you do - ",5,False),
    (redasgoldenrod.wait,1),
    (redasgoldenrod.say,"What will you do - turn the world up-side-down?!?",5,False),
    (redasgoldenrod.unset_interrupt,"earlycompletion")
])
for t in range(70):
    redasgoldenrod.queue_action(redasgoldenrod.move,(1,0),level2)

redasgoldenrod.set_interrupt("earlycompletion",level2.earlycompletioncheck,[
    (redasgoldenrod.say,"Hey!"),
    (redasgoldenrod.say,"I wasn't- ",6,False),
    (redasgoldenrod.wait,1),
    (redasgoldenrod.say,"I wasn't- ugh...",6,False)
]+([(redasgoldenrod.move,(1,0),level2)]*70))

level2.safe_objlist.extend((gate1,gate2,redasgoldenrod))
level2.hints=["Three exclamation marks? That's the sure sign of a madman... Anyway. Once again, once you use info() on everything in objs, the solution will be pretty obvious.",
              "And remember that if you do mess up you can hit R to reset the level.",
              "Those ♁s are pressure plates, and the ○ is a boulder. If the boulder is on the pressure plate, it'll make something happen somewhere else in the level. In this case it'll probably make one of those doors open.",
              "The boulder has a .momentum attribute. That sounds like it should make the boulder move somehow, right?",
              "And we've already seen that two numbers inside ()s has something to do with where things are...",
              "Again, the first number is left and right, the second number is up and down. Except this time it's a direction instead of a position.",
              "Namely, the direction the boulder's momentum takes it. So if you change the 'up-down' number to -1 instead of 1, it will go up.",
              "The next hint is the complete solution.",
              "Enter objs[0].momentum=(0,-1). Walk through the first door. Enter objs[0].momentum=(0,1). Walk into the levelporter."]

#LEVEL 3 : ELECTRIC S(L)IDEWAYS
#Player learns how gravplates work, and learns it can force sideways as well.
level3=Level("LEVEL 3: ELECTRIC S(L)IDEWAYS",[],gameobjs.Player((12,12)),playlists["Calm"])
level3.objlist=[gameobjs.GravPad((21,22),(0,-1),1), gameobjs.GravPad((21,9),(0,1),1)]
gate1=gameobjs.Gate((15,13))
gate2=gameobjs.Gate((17,15))
level3.safe_objlist=(
                     [gameobjs.Boulder((21,12))]+
                     wall_line((20,8),(0,1),14)+
                     wall_line((22,9),(0,1),13)+
                     wall_room((10,10),5) + wall_room((15,10),5) + wall_room((15,15),5)+
                     wall_line((21,7),(1,0),5)+
                     [gameobjs.PressurePlate((21,18),gate1), gameobjs.PressurePlate((25,8),gate2)]+
                     [gameobjs.Wall((26,8))]+
                     [gameobjs.Levelporter((19,18),"Dejavu")]
                     )
level3.safe_objlist.remove(level3.get_at((15,13)))
level3.safe_objlist.remove(level3.get_at((15,13)))
level3.safe_objlist.remove(level3.get_at((17,15)))
level3.safe_objlist.remove(level3.get_at((17,15)))
level3.safe_objlist.extend((gate1,gate2))
level3.hints=["I assume you've already used info() to find out what a gravpad (♨) does.",
              "The blue arrows represent what direction it'll send the boulder when it touches them.",
              "So what if it sent it in no direction at all?",
              "No movement being represented by (0,0). Move 0 left-right, move 0 up-down.",
              "Ah, but it stops it just a little bit further than it needs to be, doesn't it? I guess the gravpad just doesn't have enough range...",
              "But it does have a .range attribute. Which you can set.",
              "Horizontal movement is, again, represented by the first number of a direction vector.",
              "And of course a gravpad can start a boulder moving just as easily as it stops it..",
              "The next hint is the complete solution.",
              "Enter objs[0].gravity_direction=(0,0). Enter objs[0].range=4. Walk through the first door. Enter objs[0].gravity_direction=(0,-1). Enter gravity_direction=(1,0). Walk into the levelporter."]

level4=Level("LEVEL 4: TIME CUBE 2419",[],gameobjs.Player((24, 16)),playlists["Calm"])
gate1=gameobjs.Gate(startpos=(24, 13))
level4.objlist=[gameobjs.GravPad(startpos=(49, 19), gravity_direction=(0, -1), range=2)]
level4.safe_objlist=[gameobjs.Wall(startpos=(28, 10)),
                   gameobjs.Wall(startpos=(21, 10)),
                   gameobjs.Wall(startpos=(29, 10)),
                   gameobjs.Wall(startpos=(20, 10)),
                   gameobjs.Wall(startpos=(19, 10)),
                   gameobjs.Wall(startpos=(19, 11)),
                   gameobjs.Wall(startpos=(19, 12)),
                   gameobjs.Wall(startpos=(19, 14)),
                   gameobjs.Wall(startpos=(19, 15)),
                   gameobjs.Wall(startpos=(29, 15)),
                   gameobjs.Wall(startpos=(29, 14)),
                   gameobjs.Wall(startpos=(29, 12)),
                   gameobjs.Wall(startpos=(29, 11)),
                   gameobjs.Wall(startpos=(60, 12)),
                   gameobjs.Wall(startpos=(60, 13)),
                   gameobjs.Wall(startpos=(60, 14)),
                   gameobjs.Wall(startpos=(51, 14)),
                   gameobjs.Wall(startpos=(51, 13)),
                   gameobjs.Wall(startpos=(51, 12)),
                   gameobjs.Wall(startpos=(28, 16)),
                   gameobjs.Wall(startpos=(21, 16)),
                   gameobjs.Wall(startpos=(20, 16)),
                   gameobjs.Wall(startpos=(19, 16)),
                   gameobjs.Wall(startpos=(54, 16)),
                   gameobjs.Wall(startpos=(55, 16)),
                   gameobjs.Wall(startpos=(56, 16)),
                   gameobjs.Wall(startpos=(57, 16)),
                   gameobjs.Wall(startpos=(58, 16)),
                   gameobjs.Wall(startpos=(20, 11)),
                   gameobjs.Wall(startpos=(28, 15)),
                   gameobjs.Wall(startpos=(20, 15)),
                   gameobjs.Wall(startpos=(28, 11)),
                   gameobjs.Wall(startpos=(27, 10)),
                   gameobjs.Wall(startpos=(18, 10)),
                   gameobjs.Wall(startpos=(18, 11)),
                   gameobjs.Wall(startpos=(18, 12)),
                   gameobjs.Wall(startpos=(18, 13)),
                   gameobjs.Wall(startpos=(18, 14)),
                   gameobjs.Wall(startpos=(18, 15)),
                   gameobjs.Wall(startpos=(18, 16)),
                   gameobjs.Wall(startpos=(18, 17)),
                   gameobjs.Wall(startpos=(19, 17)),
                   gameobjs.Wall(startpos=(20, 17)),
                   gameobjs.Wall(startpos=(21, 17)),
                   gameobjs.Wall(startpos=(22, 17)),
                   gameobjs.Wall(startpos=(23, 17)),
                   gameobjs.Wall(startpos=(24, 17)),
                   gameobjs.Wall(startpos=(25, 17)),
                   gameobjs.Wall(startpos=(26, 17)),
                   gameobjs.Wall(startpos=(27, 17)),
                   gameobjs.Wall(startpos=(28, 17)),
                   gameobjs.Wall(startpos=(29, 17)),
                   gameobjs.Wall(startpos=(29, 16)),
                   gameobjs.Wall(startpos=(30, 17)),
                   gameobjs.Wall(startpos=(30, 16)),
                   gameobjs.Wall(startpos=(30, 15)),
                   gameobjs.Wall(startpos=(30, 14)),
                   gameobjs.Wall(startpos=(30, 13)),
                   gameobjs.Wall(startpos=(30, 12)),
                   gameobjs.Wall(startpos=(30, 11)),
                   gameobjs.Wall(startpos=(30, 10)),
                   gameobjs.Wall(startpos=(30, 9)),
                   gameobjs.Wall(startpos=(29, 9)),
                   gameobjs.Wall(startpos=(28, 9)),
                   gameobjs.Wall(startpos=(27, 9)),
                   gameobjs.Wall(startpos=(26, 9)),
                   gameobjs.Wall(startpos=(25, 9)),
                   gameobjs.Wall(startpos=(24, 9)),
                   gameobjs.Wall(startpos=(23, 9)),
                   gameobjs.Wall(startpos=(22, 9)),
                   gameobjs.Wall(startpos=(21, 9)),
                   gameobjs.Wall(startpos=(20, 9)),
                   gameobjs.Wall(startpos=(19, 9)),
                   gameobjs.Wall(startpos=(18, 9)),
                   gameobjs.Wall(startpos=(27, 16)),
                   gameobjs.Wall(startpos=(19, 13)),
                   gameobjs.Wall(startpos=(20, 13)),
                   gameobjs.Wall(startpos=(21, 13)),
                   gameobjs.Wall(startpos=(22, 13)),
                   gameobjs.Wall(startpos=(23, 13)),
                   gameobjs.Wall(startpos=(25, 13)),
                   gameobjs.Wall(startpos=(26, 13)),
                   gameobjs.Wall(startpos=(27, 13)),
                   gameobjs.Wall(startpos=(28, 13)),
                   gameobjs.Wall(startpos=(29, 13)),
                   gameobjs.Wall(startpos=(50, 13)),
                   gameobjs.Wall(startpos=(61, 13)),
                   gameobjs.Wall(startpos=(59, 12)),
                   gameobjs.Wall(startpos=(58, 12)),
                   gameobjs.Wall(startpos=(57, 12)),
                   gameobjs.Wall(startpos=(56, 12)),
                   gameobjs.Wall(startpos=(55, 12)),
                   gameobjs.Wall(startpos=(54, 12)),
                   gameobjs.Wall(startpos=(53, 12)),
                   gameobjs.Wall(startpos=(52, 12)),
                   gameobjs.Wall(startpos=(52, 13)),
                   gameobjs.Wall(startpos=(53, 13)),
                   gameobjs.Wall(startpos=(54, 13)),
                   gameobjs.Wall(startpos=(55, 13)),
                   gameobjs.Wall(startpos=(56, 13)),
                   gameobjs.Wall(startpos=(57, 13)),
                   gameobjs.Wall(startpos=(58, 13)),
                   gameobjs.Wall(startpos=(58, 14)),
                   gameobjs.Wall(startpos=(59, 13)),
                   gameobjs.Wall(startpos=(59, 14)),
                   gameobjs.Wall(startpos=(59, 15)),
                   gameobjs.Wall(startpos=(58, 15)),
                   gameobjs.Wall(startpos=(57, 15)),
                   gameobjs.Wall(startpos=(57, 14)),
                   gameobjs.Wall(startpos=(56, 14)),
                   gameobjs.Wall(startpos=(55, 14)),
                   gameobjs.Wall(startpos=(54, 14)),
                   gameobjs.Wall(startpos=(52, 14)),
                   gameobjs.Wall(startpos=(53, 14)),
                   gameobjs.Wall(startpos=(56, 15)),
                   gameobjs.Wall(startpos=(55, 15)),
                   gameobjs.Wall(startpos=(54, 15)),
                   gameobjs.Wall(startpos=(53, 15)),
                   gameobjs.Wall(startpos=(59, 11)),
                   gameobjs.Wall(startpos=(58, 11)),
                   gameobjs.Wall(startpos=(57, 11)),
                   gameobjs.Wall(startpos=(56, 11)),
                   gameobjs.Wall(startpos=(55, 11)),
                   gameobjs.Wall(startpos=(54, 11)),
                   gameobjs.Wall(startpos=(53, 11)),
                   gameobjs.Wall(startpos=(52, 11)),
                   gameobjs.Wall(startpos=(53, 10)),
                   gameobjs.Wall(startpos=(54, 10)),
                   gameobjs.Wall(startpos=(55, 10)),
                   gameobjs.Wall(startpos=(56, 10)),
                   gameobjs.Wall(startpos=(57, 10)),
                   gameobjs.Wall(startpos=(58, 10)),
                   gameobjs.Wall(startpos=(52, 15)),
                   gameobjs.Wall(startpos=(53, 16)),
                   gameobjs.Wall(startpos=(48, 13)),
                   gameobjs.Wall(startpos=(48, 12)),
                   gameobjs.Wall(startpos=(48, 11)),
                   gameobjs.Wall(startpos=(48, 10)),
                   gameobjs.Wall(startpos=(48, 9)),
                   gameobjs.Wall(startpos=(48, 8)),
                   gameobjs.Wall(startpos=(49, 8)),
                   gameobjs.Wall(startpos=(50, 8)),
                   gameobjs.Wall(startpos=(51, 8)),
                   gameobjs.Wall(startpos=(52, 8)),
                   gameobjs.Wall(startpos=(53, 8)),
                   gameobjs.Wall(startpos=(54, 8)),
                   gameobjs.Wall(startpos=(55, 8)),
                   gameobjs.Wall(startpos=(56, 8)),
                   gameobjs.Wall(startpos=(57, 8)),
                   gameobjs.Wall(startpos=(58, 8)),
                   gameobjs.Wall(startpos=(59, 8)),
                   gameobjs.Wall(startpos=(60, 8)),
                   gameobjs.Wall(startpos=(62, 8)),
                   gameobjs.Wall(startpos=(63, 8)),
                   gameobjs.Wall(startpos=(63, 13)),
                   gameobjs.Wall(startpos=(63, 12)),
                   gameobjs.Wall(startpos=(63, 11)),
                   gameobjs.Wall(startpos=(63, 10)),
                   gameobjs.Wall(startpos=(63, 9)),
                   gameobjs.Wall(startpos=(61, 8)),
                   gameobjs.Wall(startpos=(63, 14)),
                   gameobjs.Wall(startpos=(63, 15)),
                   gameobjs.Wall(startpos=(63, 16)),
                   gameobjs.Wall(startpos=(63, 17)),
                   gameobjs.Wall(startpos=(63, 18)),
                   gameobjs.Wall(startpos=(62, 18)),
                   gameobjs.Wall(startpos=(61, 18)),
                   gameobjs.Wall(startpos=(60, 18)),
                   gameobjs.Wall(startpos=(59, 18)),
                   gameobjs.Wall(startpos=(58, 18)),
                   gameobjs.Wall(startpos=(57, 18)),
                   gameobjs.Wall(startpos=(56, 18)),
                   gameobjs.Wall(startpos=(55, 18)),
                   gameobjs.Wall(startpos=(54, 18)),
                   gameobjs.Wall(startpos=(53, 18)),
                   gameobjs.Wall(startpos=(52, 18)),
                   gameobjs.Wall(startpos=(51, 18)),
                   gameobjs.Wall(startpos=(50, 18)),
                   gameobjs.Wall(startpos=(49, 18)),
                   gameobjs.Wall(startpos=(48, 18)),
                   gameobjs.Wall(startpos=(48, 14)),
                   gameobjs.Wall(startpos=(48, 15)),
                   gameobjs.Wall(startpos=(48, 16)),
                   gameobjs.Wall(startpos=(48, 17)),
                   gameobjs.Wall(startpos=(30, 8)),
                   gameobjs.Wall(startpos=(31, 8)),
                   gameobjs.Wall(startpos=(31, 9)),
                   gameobjs.Wall(startpos=(31, 10)),
                   gameobjs.Wall(startpos=(31, 11)),
                   gameobjs.Wall(startpos=(31, 12)),
                   gameobjs.Wall(startpos=(31, 13)),
                   gameobjs.Wall(startpos=(31, 14)),
                   gameobjs.Wall(startpos=(31, 15)),
                   gameobjs.Wall(startpos=(31, 16)),
                   gameobjs.Wall(startpos=(31, 17)),
                   gameobjs.Wall(startpos=(31, 18)),
                   gameobjs.Wall(startpos=(30, 18)),
                   gameobjs.Wall(startpos=(29, 18)),
                   gameobjs.Wall(startpos=(28, 18)),
                   gameobjs.Wall(startpos=(27, 18)),
                   gameobjs.Wall(startpos=(26, 18)),
                   gameobjs.Wall(startpos=(25, 18)),
                   gameobjs.Wall(startpos=(24, 18)),
                   gameobjs.Wall(startpos=(23, 18)),
                   gameobjs.Wall(startpos=(22, 18)),
                   gameobjs.Wall(startpos=(21, 18)),
                   gameobjs.Wall(startpos=(20, 18)),
                   gameobjs.Wall(startpos=(19, 18)),
                   gameobjs.Wall(startpos=(18, 18)),
                   gameobjs.Wall(startpos=(17, 18)),
                   gameobjs.Wall(startpos=(17, 17)),
                   gameobjs.Wall(startpos=(17, 16)),
                   gameobjs.Wall(startpos=(17, 15)),
                   gameobjs.Wall(startpos=(17, 14)),
                   gameobjs.Wall(startpos=(17, 13)),
                   gameobjs.Wall(startpos=(17, 12)),
                   gameobjs.Wall(startpos=(17, 11)),
                   gameobjs.Wall(startpos=(17, 10)),
                   gameobjs.Wall(startpos=(17, 9)),
                   gameobjs.Wall(startpos=(17, 8)),
                   gameobjs.Wall(startpos=(18, 8)),
                   gameobjs.Wall(startpos=(19, 8)),
                   gameobjs.Wall(startpos=(20, 8)),
                   gameobjs.Wall(startpos=(21, 8)),
                   gameobjs.Wall(startpos=(22, 8)),
                   gameobjs.Wall(startpos=(23, 8)),
                   gameobjs.Wall(startpos=(24, 8)),
                   gameobjs.Wall(startpos=(25, 8)),
                   gameobjs.Wall(startpos=(26, 8)),
                   gameobjs.Wall(startpos=(27, 8)),
                   gameobjs.Wall(startpos=(28, 8)),
                   gameobjs.Wall(startpos=(29, 8)),
                   gameobjs.GravPad(startpos=(62, 19), gravity_direction=(-1, 0), range=2),
                   gameobjs.GravPad(startpos=(62, 7), gravity_direction=(0, 1), range=-2),
                   gameobjs.GravPad(startpos=(49, 7), gravity_direction=(1, 0), range=-2),
                   gameobjs.Boulder(startpos=(62, 9)),
                   gate1,
                   gameobjs.Levelporter(startpos=(24, 10), targetlevel='Missiles'),
                   gameobjs.PressurePlate(startpos=(49, 13), target=gate1),]
level4.hints = [
    "The gravity pad can push you around too, by the way.",
    "But not while it's over there. You might consider storing the original location before you move it, though. 'oldloc = objs[0].pos' should do it.",
    "The door isn't open long enough for you to run through it on your own power.",
    "But the boulder running over the pressure plate is what opens the door. You'll have to carefully time moving the gravpad.",
    "The next hint is the complete solution.",
    "Position yourself in front of the gate. "
    "objs[0].pos=(24,19)"
    "objs[0].range=10"
]

#LEVEL 5 : THE LONG ARM
#Player learns how missiles and missile launchers work. He sees that the missile destroys the boulder, but not the wall,
#and figures out that he can re-target it to destroy the gate.
#Player is also introduced to attribute enforcers if he tries to get cute and move the gate.


gate1=gameobjs.Gate((30,15))
plate=gameobjs.SafePressurePlate((21,18),gate1)
launcher=gameobjs.MissileLauncher((18,15),None)

attr_ef_dict={gate1:("pos",), launcher:("pos",), plate:("pos","target","update")}
enforcer=gameobjs.AttributeEnforcer((35,12),attr_ef_dict)

class Level5(Level):
    def attrenftripped(self):
        return enforcer.graphicreset != 0

    def missileintercept(self):
        for theobj in self.objlist:
            if isinstance(theobj, gameobjs.Missile) and gameobjs.distance(theobj.pos, self.redasgoldenrod.pos) < 2.2:
                theobj.target = None
                return True

    def earlycompletioncheck(self):
        """Returns True if the player has destroyed the gate."""
        return not bool(self.get_at((30, 15)))

    def enforcerdestroyedcheck(self):
        return not bool(self.get_at((35, 12)))

    def playerescapedcheck(self):
        # The "player left the game area" check should only trip if EGR is actually watching
        return (self.redasgoldenrod.pos[0] < 80) and \
               not (10 < self.plyobj.pos[0] < 32 and 10 < self.plyobj.pos[1] < 40)

level5=Level5("LEVEL 5: THE LONG ARM",[],gameobjs.Player((11,15)),playlists["Intense"])

level5.redasgoldenrod=gameobjs.NonPlayerCharacter("G",(81,10),"goldenrod")
redasgoldenrod = level5.redasgoldenrod

launcher.target = level5.plyobj

redasgoldenrod.queue_many_actions([ #There's no reason you can't set interrupts outside of a queue_many_actions, probably should really
    (redasgoldenrod.set_interrupt,"missileintercept",level5.missileintercept,[ #I just wanted this here to demonstrate nested lists of NPC commands
        (redasgoldenrod.set_interrupt,"missileintercept",level5.missileintercept,[(redasgoldenrod.pop_stack,)],True),
        (redasgoldenrod.wait,0.3),
        (redasgoldenrod.say,"WOW."),
        (redasgoldenrod.say,"RUDE."),
        (redasgoldenrod.say,"Anyway..."),
        (redasgoldenrod.pop_stack,)
    ]),
    (redasgoldenrod.wait,3),
    (redasgoldenrod.say,"BWAHAHAHAHAHAHAHAHAHA!!!",3.5, False)
] + [(redasgoldenrod.move,(-1,0),level5)] * 45 + [
    (redasgoldenrod.say,"YOUR FOOLISH TRICKS ARE OVER MR. AT-SIGN!!!"),
    (redasgoldenrod.move,(-1,1),level5),
    (redasgoldenrod.say,"You see, this INGENIOUS DEVICE here..."),
    (redasgoldenrod.say,"...is an ATTRIBUTE ENFORCER!!!"),
    (redasgoldenrod.say,"Even if you COULD evade the deadly missile launcher turret-"),
    (redasgoldenrod.say,"The pressure plate that MIGHT have let you escape..."),
    (redasgoldenrod.wait,1),
    (redasgoldenrod.say,"... has been PERMANENTLY MOVED AWAY FROM THE DOOR!"),
    (redasgoldenrod.move,(-1,0),level5),
    (redasgoldenrod.say,"Just TRY moving it!",3,False),
    (redasgoldenrod.move,(-1,1),level5),
    (redasgoldenrod.wait,1),
    (redasgoldenrod.say,"Just TRY moving it! Go ahead!",3,False),
    (redasgoldenrod.move,(-1,1),level5),
    (redasgoldenrod.wait,1),
    (redasgoldenrod.say,"Just TRY moving it! Go ahead! TRY!!!",3,False),
    (redasgoldenrod.move,(-1,0),level5),
    (redasgoldenrod.wait_trigger,level5.attrenftripped),
    (redasgoldenrod.move,(-1,-1),level5),
    (redasgoldenrod.move,(0,-1),level5),
    (redasgoldenrod.say,"HAHA! You see! The enforcer turns RED!"),
    (redasgoldenrod.say,"To signify that you are WRONG! AND DUMB!"),
    (redasgoldenrod.say,"BWAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHA-"),
    (redasgoldenrod.say,"*cough*",0.25),
    (redasgoldenrod.unset_all_interrupts,),
    (redasgoldenrod.wait,0.5),
    (redasgoldenrod.say,"*cough*",0.25),
    (redasgoldenrod.wait,1),
    (redasgoldenrod.say,"*cough*",0.75),
    (redasgoldenrod.wait,3),
    (redasgoldenrod.say,"...",4),
    (redasgoldenrod.wait,2)
] + ([(redasgoldenrod.move,(1,-1),level5)] * 60) )

redasgoldenrod.set_interrupt("earlycompletion",level5.earlycompletioncheck,[
    (redasgoldenrod.wait,3),
    (redasgoldenrod.say,"Oh."),
    (redasgoldenrod.say,"Well."),
    (redasgoldenrod.say,"Never mind then.",5,False),
    (redasgoldenrod.move,(1,-1),level5),
    (redasgoldenrod.move,(1,-1),level5),
    (redasgoldenrod.move,(1,-1),level5)
]+([(redasgoldenrod.move,(1,0),level5)]*60))

redasgoldenrod.set_interrupt("enforcerdestroyed",level5.enforcerdestroyedcheck,[
    (redasgoldenrod.wait,1.5),
    (redasgoldenrod.say,"Oh wow, I didn't think of that..."),
    (redasgoldenrod.say,"Uhh..."),
    (redasgoldenrod.say,"I'll...",5,False),
    (redasgoldenrod.wait,3),
    (redasgoldenrod.say,"I'll... get you next time?",5,False),
    (redasgoldenrod.wait,2),
    (redasgoldenrod.say,"I'll... get you next time? I guess?",False),
    (redasgoldenrod.wait,1),
    (redasgoldenrod.move,(1,-1),level5),
    (redasgoldenrod.move,(1,-1),level5),
    (redasgoldenrod.move,(1,-1),level5)
]+([(redasgoldenrod.move,(1,0),level5)]*60))

redasgoldenrod.set_interrupt("playerescaped",level5.playerescapedcheck,[
    (redasgoldenrod.say,"Hey!"),
    (redasgoldenrod.say,"Get back in there!"),
    (setattr,level5.plyobj,"pos",(11,15)),
    (redasgoldenrod.set_interrupt,"playerescaped",level5.playerescapedcheck,[
        (redasgoldenrod.say,"Seriously!"),
        (setattr,level5.plyobj,"pos",(11,15)),
        (redasgoldenrod.set_interrupt,"playerescaped",level5.playerescapedcheck,[
            (setattr,level5.plyobj,"pos",(11,15)),
            (redasgoldenrod.pop_stack,)
        ],True),
        (redasgoldenrod.pop_stack,)
    ]),
    (redasgoldenrod.pop_stack,)
])

level5.objlist=[launcher,plate]#,gameobjs.MissileLauncher((48,15),redasgoldenrod)]
level5.safe_objlist=(
                     wall_room((10,10),20)+
                     [gameobjs.Wall((12,15)), gameobjs.Wall((32,15)),gameobjs.Wall((31,14)),gameobjs.Wall((31,16))]+
                     [gameobjs.Boulder((14,12)), gameobjs.Wall((14,16))]+
                     [gameobjs.Levelporter((31,15),"Force"), enforcer,
                      redasgoldenrod])
level5.safe_objlist.remove(level5.get_at((30,15)))
level5.safe_objlist.append(gate1)
level5.unimportant.update((level5.get_at((14,12)),gate1,redasgoldenrod))
level5.hints=["Boy, that boulder didn't last long.",
              "And neither will you if that missile hits you. Lucky thing it can't get through the walls.",
              "You can't move that boulder, especially not before it gets blowed up. And you can't stand on the pressure plate and walk through the door at the same time.",
              "So maybe try moving the pressure plate?",
              "Yup. That ☑ over there stops you. His job is to prevent obvious solutions and just generally spoil your day.",
              "But you can get the gate object out of the pressure plate's .target parameter.",
              "BUT the pressure plate itself stops you from jamming it open, and that tickbox jerk stops you from moving the gate out of the way. You're gonna have to go THROUGH it somehow.",
              "Doesn't that just make you mad? Like you wanna, say, shoot something with a robotic target-seeking rocket launcher turret?",
              "Hey, that rocket launcher thing has a .target attribute too...",
              "Just like the pressure plate does.",
              "The pressure plate's .target being the gate, the gate being the thing that's in your way.",
              "Explosions tend to make things stop being in your way. Make the connection.",
              "The next hint is the complete solution.",
              "Enter objs[0].target=objs[1].target. Wait for the missile launcher to destroy the gate. Enter objs[0].target=objs[0]. Walk into the levelporter."]

#LEVEL 6 : SUPERIOR (EN)FORCE
#Player is formally introduced to attribute enforcers, and learns that they can be destroyed with missiles
#Also the first "action-y" level.
level6=Level("LEVEL 6: SUPERIOR (EN)FORCE",[],gameobjs.Player( (25, 10) ),playlists["Intense"])
gravgen1=gameobjs.GravPad((52, 12),(0,1),4)
#portergate=gameobjs.Gate((18, 10))
pbe=gameobjs.ProtonBeamEmitter((20,8))
holdupgate=gameobjs.Gate((54, 9))
launcher=gameobjs.MissileLauncher((36, 18),level6.plyobj.proxy)
level6.objlist= [gravgen1,
                 gameobjs.Boulder((47, 2))]
level6.safe_objlist=[
                    gameobjs.Wall((19,8)),
                    gameobjs.Wall((21,8)),
                    gameobjs.Wall((19,7)),
                    gameobjs.Wall((21,7)),
                    gameobjs.Wall((20,7)),
                    gameobjs.Wall((20,12)),
                    gameobjs.Wall((19,12)),
                    gameobjs.Wall((21,12)),
                    gameobjs.Wall((24, 11)),
                    gameobjs.Wall((25, 11)),
                    gameobjs.Wall((26, 11)),
                    gameobjs.Wall((35, 11)),
                    gameobjs.Wall((36, 11)),
                    gameobjs.Wall((37, 11)),
                    gameobjs.Wall((45, 11)),
                    gameobjs.Wall((46, 11)),
                    gameobjs.Wall((47, 11)),
                    gameobjs.Wall((49, 11)),
                    gameobjs.Wall((50, 11)),
                    gameobjs.Wall((51, 11)),
                    gameobjs.Wall((52, 11)),
                    gameobjs.Wall((49, 7)),
                    gameobjs.Wall((51, 7)),
                    gameobjs.Wall((50, 7)),
                    gameobjs.Wall((52, 7)),
                    gameobjs.Wall((53, 7)),
                    gameobjs.Wall((53, 8)),
                    gameobjs.Wall((53, 9)),
                    gameobjs.Wall((53, 10)),
                    gameobjs.Wall((53, 11)),
                    gameobjs.PressurePlate((52, 8),pbe),
                    gameobjs.Wall((22, 11)),
                    gameobjs.Wall((22, 9)),
                    gameobjs.Wall((21, 9)),
                    gameobjs.Wall((19, 9)),
                    gameobjs.Wall((18, 9)),
                    gameobjs.Wall((21, 11)),
                    gameobjs.Wall((19, 11)),
                    gameobjs.Wall((18, 11)),
                    gameobjs.Wall((17, 9)),
                    gameobjs.Wall((17, 11)),
                    gameobjs.Wall((17, 8)),
                    gameobjs.Wall((14, 11)),
                    gameobjs.Wall((16, 11)),
                    gameobjs.Wall((15, 11)),
                    gameobjs.Wall((13, 11)),
                    gameobjs.Wall((13, 10)),
                    gameobjs.Wall((13, 9)),
                    gameobjs.Wall((13, 8)),
                    gameobjs.Wall((13, 7)),
                    gameobjs.Wall((14, 7)),
                    gameobjs.Wall((16, 7)),
                    gameobjs.Wall((15, 7)),
                    gameobjs.Wall((17, 7)),
                    gameobjs.Levelporter((14, 8),'DroneSpiral'),
                    gameobjs.AttributeEnforcer((46, 4),{gravgen1:("gravity_direction","pos","range")}),
                    gameobjs.Boulder((52, 9)),
                    gameobjs.Wall((26, 10)),
                    pbe,
                    launcher]
level6.safe_objlist.extend(wall_room((23,7),25))
level6.safe_objlist.extend(wall_room((41, 0),7))
level6.safe_objlist.extend(wall_line((51,8),(0,1),3))
level6.remove_obj(level6.get_at((23,10)))
level6.remove_obj(level6.get_at((48,9)))
level6.remove_obj(level6.get_at((42,7)))
level6.remove_obj(level6.get_at((42,7)))
level6.unimportant.add(level6.get_at((46, 4)))
level6.hints=["Objs isn't static. Things changing in the game world can change what's available in the console.",
              "Try looking at it when there's a missile on the screen.",
              "So obviously you need to get a boulder on that pressureplate, and the gravity pad is the way to do that...",
              "... but the attribute enforcer is stopping you. So you'll have to get rid of that.",
              "So what did we learn can destroy objects in the world?",
              "The missiles have to know what to go after too, of course. Don't forget about info().",
              "But of course you don't have access to the enforcer, so you can't make a missile go after it directly.",
              "So what else can you access?",
              "You can try targeting the boulder.",
              "And if it's behind the attributeenforcer when there's a missile in the room, the enforcer will get blown up.",
              "So the problem then is getting a missile into the room with you.",
              "If only you had a red flag. Or maybe a barrel and a clown outfit.",
              "The missiles are going after you. Stand in the room just above the opening, and they'll follow you in.",
              "The next hint is the complete solution.",
              """Go to the room containing the attribute enforcer. Stand right above the square behind it.
              Enter objs[1].momentum=(0,-1). Exit the injection console and let the boulder roll up until it
              stops against you. 
              Stand in front of the opening to the room. Wait for a missile to come in. 
              Enter objs[2].target=objs[1]. Wait for a missile to destroy the enforcer.
              Enter objs[0].gravity_direction=(0,-1). Walk into the levelporter."""
              ]

#LEVEL 7 : SPIRAL POWER
#Player is introduced to drones/droneupload, programming a drone to escape a simple spiral "maze".
level7=Level("LEVEL 7: SPIRAL POWER/GETTIN FUNC-Y",[],gameobjs.Player( (25, 15) ),playlists["Calm"])
gate1=gameobjs.Gate((27, 3))
gate2=gameobjs.Gate((30, 15))
drone=gameobjs.Drone((35, 7))
level7.objlist=[]
level7.extralocals={"upload":drone.upload}
level7.safe_objlist=[ gameobjs.Wall((35, 6)),
                   gameobjs.Wall((34, 6)),
                   gameobjs.Wall((34, 7)),
                   gameobjs.Wall((34, 8)),
                   gameobjs.Wall((35, 8)),
                   gameobjs.Wall((36, 8)),
                   gameobjs.Wall((37, 8)),
                   gameobjs.Wall((37, 7)),
                   gameobjs.Wall((37, 6)),
                   gameobjs.Wall((37, 5)),
                   gameobjs.Wall((37, 4)),
                   gameobjs.Wall((36, 4)),
                   gameobjs.Wall((34, 4)),
                   gameobjs.Wall((35, 4)),
                   gameobjs.Wall((33, 4)),
                   gameobjs.Wall((32, 4)),
                   gameobjs.Wall((32, 5)),
                   gameobjs.Wall((32, 6)),
                   gameobjs.Wall((32, 7)),
                   gameobjs.Wall((32, 8)),
                   gameobjs.Wall((32, 9)),
                   gameobjs.Wall((32, 10)),
                   gameobjs.Wall((33, 10)),
                   gameobjs.Wall((34, 10)),
                   gameobjs.Wall((35, 10)),
                   gameobjs.Wall((37, 10)),
                   gameobjs.Wall((36, 10)),
                   gameobjs.Wall((38, 10)),
                   gameobjs.Wall((39, 10)),
                   gameobjs.Wall((39, 9)),
                   gameobjs.Wall((39, 8)),
                   gameobjs.Wall((39, 7)),
                   gameobjs.Wall((39, 6)),
                   gameobjs.Wall((39, 5)),
                   gameobjs.Wall((39, 4)),
                   gameobjs.Wall((39, 3)),
                   gameobjs.Wall((39, 2)),
                   gameobjs.Wall((38, 2)),
                   gameobjs.Wall((36, 2)),
                   gameobjs.Wall((37, 2)),
                   gameobjs.Wall((35, 2)),
                   gameobjs.Wall((34, 2)),
                   gameobjs.Wall((32, 2)),
                   gameobjs.Wall((33, 2)),
                   gameobjs.Wall((31, 2)),
                   gameobjs.Wall((31, 4)),
                   gameobjs.Wall((30, 4)),
                   gameobjs.Wall((30, 2)),
                   gameobjs.Wall((29, 2)),
                   gameobjs.Wall((29, 4)),
                   gameobjs.Wall((28, 4)),
                   gameobjs.Wall((28, 2)),
                   gameobjs.Wall((27, 2)),
                   gameobjs.Wall((27, 4)),
                   gate1,
                   gameobjs.PressurePlate((28, 3),gate1),
                   gameobjs.Wall((26, 2)),
                   gameobjs.Wall((26, 4)),
                   gameobjs.Wall((25, 3)),
                   gameobjs.Wall((25, 14)),
                   gameobjs.Wall((26, 14)),
                   gameobjs.Wall((27, 14)),
                   gameobjs.Wall((28, 14)),
                   gameobjs.Wall((29, 14)),
                   gameobjs.Wall((30, 14)),
                   gameobjs.Wall((31, 14)),
                   gameobjs.Wall((32, 14)),
                   gameobjs.Wall((33, 14)),
                   gameobjs.Wall((34, 14)),
                   gameobjs.Wall((35, 14)),
                   gameobjs.Wall((37, 14)),
                   gameobjs.Wall((36, 14)),
                   gameobjs.Wall((38, 14)),
                   gameobjs.Wall((39, 14)),
                   gameobjs.Wall((39, 16)),
                   gameobjs.Wall((38, 16)),
                   gameobjs.Wall((37, 16)),
                   gameobjs.Wall((36, 16)),
                   gameobjs.Wall((35, 16)),
                   gameobjs.Wall((34, 16)),
                   gameobjs.Wall((33, 16)),
                   gameobjs.Wall((31, 16)),
                   gameobjs.Wall((32, 16)),
                   gameobjs.Wall((30, 16)),
                   gameobjs.Wall((29, 16)),
                   gameobjs.Wall((28, 16)),
                   gameobjs.Wall((27, 16)),
                   gameobjs.Wall((26, 16)),
                   gameobjs.Wall((25, 16)),
                   gameobjs.Wall((24, 15)),
                   gameobjs.Wall((40, 15)),
                   gameobjs.Wall((40, 14)),
                   gameobjs.Wall((40, 16)),
                   gameobjs.Wall((24, 14)),
                   gameobjs.Wall((24, 16)),
                   gameobjs.Wall((25, 2)),
                   gameobjs.Wall((25, 4)),
                   gameobjs.Levelporter((39, 15),"DroneMissile"),
                   gate2,
                   gameobjs.PressurePlate((26, 3),gate2),
                   drone]
level7.hints=["There's nothing in objs... but there's something new if you call dir().",
              "info() works on other functions too.",
              "Specifically, it'll show you the API you need to use in the function you define and pass to upload().",
              """You define a function with the def keyword. "def myfunc(self):" defines a function called 'myfunc'.
              You then put an indented code block after that - put a space before each line so Python knows they're part
              of the function.""",
              "You can also import standard library modules too. Try 'import random', then calling info() on random.",
              "And of course if something moves randomly it'll eventually get to wherever it needs to go.",
              "The next hint is the complete solution.",
              """
              moves = [(1,0), (0, -1), (-1, 0), (0, 1)]
              def think(self):
               if self.bumped: moves.append(moves.pop(0))
               self.move(*moves[0])
              upload(think)
              """]

level8=Level("LEVEL 8: REPROGRAM",[],gameobjs.Player( (19, 11) ),playlists["Intense"])
drone=gameobjs.Drone((56, 10))
launcher=gameobjs.MissileLauncher((39, 20),level8.plyobj,addsafe=True)
level8.extralocals={"upload":drone.upload}
level8.safe_objlist=[ gameobjs.Wall((19, 12)),
                   gameobjs.Wall((18, 12)),
                   gameobjs.Wall((20, 12)),
                   gameobjs.Wall((17, 12)),
                   gameobjs.Wall((17, 11)),
                   gameobjs.Wall((17, 10)),
                   gameobjs.Wall((17, 9)),
                   gameobjs.Wall((18, 9)),
                   gameobjs.Wall((20, 9)),
                   gameobjs.Wall((19, 9)),
                   gameobjs.Wall((22, 9)),
                   gameobjs.Wall((21, 9)),
                   gameobjs.Wall((23, 9)),
                   gameobjs.Wall((24, 9)),
                   gameobjs.Wall((25, 9)),
                   gameobjs.Wall((26, 9)),
                   gameobjs.Wall((27, 9)),
                   gameobjs.Wall((29, 9)),
                   gameobjs.Wall((28, 9)),
                   gameobjs.Wall((30, 9)),
                   gameobjs.Wall((31, 9)),
                   gameobjs.Wall((33, 9)),
                   gameobjs.Wall((32, 9)),
                   gameobjs.Wall((34, 9)),
                   gameobjs.Wall((36, 9)),
                   gameobjs.Wall((35, 9)),
                   gameobjs.Wall((37, 9)),
                   gameobjs.Wall((38, 9)),
                   gameobjs.Wall((39, 9)),
                   gameobjs.Wall((40, 9)),
                   gameobjs.Wall((42, 9)),
                   gameobjs.Wall((41, 9)),
                   gameobjs.Wall((44, 9)),
                   gameobjs.Wall((43, 9)),
                   gameobjs.Wall((45, 9)),
                   gameobjs.Wall((47, 9)),
                   gameobjs.Wall((46, 9)),
                   gameobjs.Wall((48, 9)),
                   gameobjs.Wall((49, 9)),
                   gameobjs.Wall((50, 9)),
                   gameobjs.Wall((51, 9)),
                   gameobjs.Wall((53, 9)),
                   gameobjs.Wall((52, 9)),
                   gameobjs.Wall((54, 9)),
                   gameobjs.Wall((55, 9)),
                   gameobjs.Wall((56, 9)),
                   gameobjs.Wall((57, 9)),
                   gameobjs.Wall((58, 9)),
                   gameobjs.Wall((60, 9)),
                   gameobjs.Wall((59, 9)),
                   gameobjs.Wall((60, 10)),
                   gameobjs.Wall((60, 11)),
                   gameobjs.Wall((60, 12)),
                   gameobjs.Wall((59, 12)),
                   gameobjs.Wall((58, 12)),
                   gameobjs.Wall((57, 12)),
                   gameobjs.Wall((56, 12)),
                   gameobjs.Levelporter((59, 11),"Rescue"),
                   gameobjs.Wall((21, 13)),
                   gameobjs.Wall((21, 14)),
                   gameobjs.Wall((21, 15)),
                   gameobjs.Wall((21, 16)),
                   gameobjs.Wall((21, 17)),
                   gameobjs.Wall((21, 18)),
                   gameobjs.Wall((21, 19)),
                   gameobjs.Wall((21, 20)),
                   gameobjs.Wall((21, 21)),
                   gameobjs.Wall((21, 22)),
                   gameobjs.Wall((21, 23)),
                   gameobjs.Wall((22, 23)),
                   gameobjs.Wall((24, 23)),
                   gameobjs.Wall((23, 23)),
                   gameobjs.Wall((25, 23)),
                   gameobjs.Wall((26, 23)),
                   gameobjs.Wall((27, 23)),
                   gameobjs.Wall((29, 23)),
                   gameobjs.Wall((28, 23)),
                   gameobjs.Wall((30, 23)),
                   gameobjs.Wall((31, 23)),
                   gameobjs.Wall((33, 23)),
                   gameobjs.Wall((32, 23)),
                   gameobjs.Wall((34, 23)),
                   gameobjs.Wall((36, 23)),
                   gameobjs.Wall((35, 23)),
                   gameobjs.Wall((37, 23)),
                   gameobjs.Wall((38, 23)),
                   gameobjs.Wall((39, 23)),
                   gameobjs.Wall((40, 23)),
                   gameobjs.Wall((42, 23)),
                   gameobjs.Wall((41, 23)),
                   gameobjs.Wall((56, 13)),
                   gameobjs.Wall((56, 14)),
                   gameobjs.Wall((56, 15)),
                   gameobjs.Wall((56, 16)),
                   gameobjs.Wall((56, 18)),
                   gameobjs.Wall((56, 17)),
                   gameobjs.Wall((56, 19)),
                   gameobjs.Wall((56, 20)),
                   gameobjs.Wall((56, 21)),
                   gameobjs.Wall((56, 22)),
                   gameobjs.Wall((54, 23)),
                   gameobjs.Wall((56, 23)),
                   gameobjs.Wall((55, 23)),
                   gameobjs.Wall((53, 23)),
                   gameobjs.Wall((52, 23)),
                   gameobjs.Wall((51, 23)),
                   gameobjs.Wall((50, 23)),
                   gameobjs.Wall((48, 23)),
                   gameobjs.Wall((49, 23)),
                   gameobjs.Wall((47, 23)),
                   gameobjs.Wall((46, 23)),
                   gameobjs.Wall((45, 23)),
                   gameobjs.Wall((44, 23)),
                   gameobjs.Wall((43, 23)),
                   gameobjs.Wall((21, 12)),
                   gameobjs.Wall((21, 11)),
                   gameobjs.Wall((56, 11)),
                   drone,
                   launcher]
level8.unimportant.add(launcher)
level8.hints=["So, when a drone walks in to something, that object gets put into\n its .bumped attribute, right?",
              "And to get across this room you'll need to make that launcher stop firing at you.",
              "And one way to do that would be to set its .target to something invalid, like None.",
              "And bumping something with a drone allows the drone access to it.",
              "So bump the missile launcher so that .bumped is the launcher,\n then set self.bumped.target to None."]

redguy=gameobjs.NonPlayerCharacter("G",(19,16),"red") # type: gameobjs.NonPlayerCharacter
class LevelIMZ1(Level):
    def intermezzo1trigger(self):
        return not gameobjs.movement_check((22, 14), self, None)

    def imz1missileintercept(self):
        for theobj in self.safe_objlist:
            if isinstance(theobj, gameobjs.Missile) and gameobjs.distance(theobj.pos, redguy.pos) < 2.2:
                theobj.target=None
                self.get_at((40, 14)).target = self.get_at((40, 14))

    def launcherbroke(self):
        return self.get_at((40,14)) is None

    def spawnlauncher(self):
        launcher=gameobjs.MissileLauncher((40, 14), None)
        self.objlist.append(launcher);
        self.unimportant.add(launcher)

    def killlauncher(self):
        try:
            self.get_at((40, 14)).target = self.get_at((40, 14))
        except AttributeError:
            pass

    def opendoor(self):
        self.get_at((22, 14)).open = True

    def makeexit(self):
        self.objlist.append(gameobjs.Levelporter((40, 14),"Linefinder"))

intermezzo1=LevelIMZ1("INTERMEZZO I: THE RESCUE",[],gameobjs.Player((19, 5)),playlists["Calm"])
intermezzo1.objlist=[gameobjs.Boulder(startpos=(17, 14))]
gate1=gameobjs.Gate(startpos=(22, 14))
intermezzo1.unimportant.add(gate1)

redguy.queue_many_actions([
    (redguy.wait,3),
    (redguy.say,"Hey!",0.75),
    (redguy.say,"Hey! Get me out of here!",5),
    (redguy.say,"Hang on a second, I, uhh, I'll...",5,False),
    (redguy.wait,2),
    (redguy.say,"Hang on a second, I, uhh, I'll... find...",5,False),
    (redguy.wait,1),
    (redguy.say,"Hang on a second, I, uhh, I'll... find... a missile launcher turret real quick.",8,False),
    (redguy.wait,5),
    (intermezzo1.spawnlauncher),
    (redguy.set_interrupt,"playerbrokelauncher",intermezzo1.launcherbroke,[
        (redguy.wait,0.25),
        (redguy.say,"Okay, uhh...",5,False),
        (redguy.wait, 3),
        (redguy.say,"Okay, uhh... you broke it."),
        (redguy.say,"Oh! I found another one!"),
        (intermezzo1.spawnlauncher),
        (redguy.say,"Now just use that to bust me out, and don't, you know, blow it up.",8),
        (redguy.set_interrupt,"playerbrokelauncher",intermezzo1.launcherbroke,[
            (redguy.say,"Okay, uhh..."),
            (redguy.say,"Oh look! The door was unlocked all along!"),
            (intermezzo1.killlauncher),
            (intermezzo1.opendoor),
            (redguy.say,"And I never would've noticed if it hadn't been for you. Boy is my face red."),
            (redguy.say,"But seriously:"),
            (redguy.pop_stack,)
        ]),
        (redguy.pop_stack,)
    ]),
    (redguy.wait,0.5),
    (redguy.say,"There we go."),
    (redguy.say,"Just set it to shoot at that boulder there..."),
    (redguy.wait_trigger,intermezzo1.intermezzo1trigger),
    (redguy.unset_interrupt,"playerbrokelauncher"),
    (intermezzo1.killlauncher),
    (redguy.wait,0.75),
    (redguy.say,"Thanks! I've been trapped in there for, like, days.",6,False),
    (redguy.move,(0,-1),intermezzo1),
    (redguy.move,(1,-1),intermezzo1),
    (redguy.move,(1,0),intermezzo1),
    (redguy.move,(1,0),intermezzo1),
    (redguy.move,(1,0),intermezzo1),
    (redguy.wait,6),
    (redguy.say,"So, uhh... you've met that other guy too, right?"),
    (redguy.say,"Yeah, the really evil sounding guy?"),
    (redguy.say,"He's the one that trapped me in there."),
    (redguy.say,"Actually, he um...", 5, False),
    (redguy.wait,2),
    (redguy.say,"Actually, he um... well."),
    (redguy.say,"He's actually my evil alternate-universe clone..."),
    (redguy.wait,2),
    (redguy.say,"Uh...", 5, False),
    (redguy.wait,0.75),
    (redguy.say, "Uhh... I'll explain the rest on the way. For now let's just get out of here."),
    (intermezzo1.makeexit), #this method should work too - Dill will update the locals dict to the clone level
    (redguy.move,(1,-1),intermezzo1),
    (redguy.say,"After you.",8,False),
    (redguy.move,(1,0),intermezzo1),(redguy.move,(1,0),intermezzo1),(redguy.move,(1,0),intermezzo1),(redguy.move,(1,0),intermezzo1),
    (redguy.move,(1,0),intermezzo1),(redguy.move,(1,0),intermezzo1),(redguy.move,(1,0),intermezzo1),(redguy.move,(1,0),intermezzo1),
    (redguy.move,(1,0),intermezzo1),(redguy.move,(1,0),intermezzo1),(redguy.move,(1,0),intermezzo1),(redguy.move,(1,0),intermezzo1),
    (redguy.move,(1,0),intermezzo1),(redguy.move,(1,0),intermezzo1),(redguy.move,(1,0),intermezzo1),(redguy.move,(1,0),intermezzo1),
])

redguy.set_interrupt("missileintercept",intermezzo1.imz1missileintercept,[(redguy.pop_stack,),True])

intermezzo1.safe_objlist=[gameobjs.Wall(startpos=(18, 16)),
                   gameobjs.Wall(startpos=(18, 15)),
                   gameobjs.Wall(startpos=(18, 14)),
                   gameobjs.Wall(startpos=(18, 13)),
                   gameobjs.Wall(startpos=(18, 12)),
                   gameobjs.Wall(startpos=(19, 12)),
                   gameobjs.Wall(startpos=(20, 12)),
                   gameobjs.Wall(startpos=(21, 12)),
                   gameobjs.Wall(startpos=(22, 12)),
                   gameobjs.Wall(startpos=(22, 13)),
                   gameobjs.Wall(startpos=(22, 15)),
                   gameobjs.Wall(startpos=(22, 16)),
                   gameobjs.Wall(startpos=(22, 17)),
                   gameobjs.Wall(startpos=(21, 17)),
                   gameobjs.Wall(startpos=(20, 17)),
                   gameobjs.Wall(startpos=(19, 17)),
                   gameobjs.Wall(startpos=(18, 17)),
                   gate1,
                   gameobjs.Wall(startpos=(17,15)),
                   redguy]


level9=Level("LEVEL 9: UNISELECTOR",[],gameobjs.Player( (16, 25) ),playlists["Calm"])
drone=gameobjs.Drone((36, 14))
level9.extralocals={"upload":drone.upload}

agates=[gameobjs.Gate((20, 25)),
        gameobjs.Gate((30, 25)),
        gameobjs.Gate((40, 25))]
bgates=[gameobjs.Gate((25, 25)),
        gameobjs.Gate((35, 25)),
        gameobjs.Gate((45, 25))]

spltr1=gameobjs.SignalSplitter(agates)
spltr2=gameobjs.SignalSplitter(bgates)
retrigger1=gameobjs.SignalRetriggerDelay(spltr1,3)
retrigger2=gameobjs.SignalRetriggerDelay(spltr2,3)
cyclingpad=gameobjs.GravPad((37, 14),(0,-1),2)
cycler=gameobjs.AttributeCycler(cyclingpad,"gravity_direction",[(1,0),(0,-1)])
redguy=gameobjs.NonPlayerCharacter("G",(-1,-2),"red") #type: gameobjs.NonPlayerCharacter
redguy.queue_many_actions([
    (redguy.wait_trigger,lambda: level9.plyobj.pos==(20,25)),
    (redguy.wait,4),
    (redguy.move,(0,1),level9),
    (redguy.wait,1),
    (redguy.say,"Sorry, just had to, uhh, make a call."),
    (redguy.say,"I called up a buddy of mine on the outside, he's gonna help us get out of here."),
    (redguy.say,"So, uh, this super-evil dictator guy that locked us in here..."),
    (redguy.say,"Whom as I've mentioned is actually my evil alternate-universe twin..."),
    (redguy.say,"Yeah, he's totally an evil despot. Rules my homeland and everything."),
    (redguy.say,"Iron fist and all that."),
    (redguy.say,"But see, we've got the power to stop him."),
    (redguy.say,"Well, by we I mean you."),
    (redguy.say,"And by stop, I mean, yanno...",5,False),
    (redguy.wait,2),
    (redguy.say,"And by stop, I mean, yanno... kill."),
    (redguy.say,"Anyway, I've got, uhh... stuff. So I'm gonna go. Yeah."),
])
for t in range(14):
    redguy.queue_action(redguy.move,(-1,0),level9)
redguy.queue_action(redguy.wait,1)
for t in range(5):
    redguy.queue_action(redguy.move,(1,0),level9)
redguy.queue_action(redguy.say,"Oh, and thanks for busting me out of prison!")
for t in range(6):
    redguy.queue_action(redguy.move,(-1,0),level9)

#We do this because apparently closed-over variables update when they're changed after the function is defined. Huh.
def robotresponse(drone=drone,redguy=redguy):
    if gameobjs.distance(drone.pos,redguy.pos) < 5: return True

redguy.set_interrupt("robotresponse",robotresponse, [
    (exec,"drone.pos=(36, 14)",globals(),{"drone":drone}),
    (redguy.say,"Woah, careful with that thing."),
    (redguy.set_interrupt,"robotresponse",robotresponse, [
        (exec,"drone.pos=(36, 14)",globals(),{"drone": drone}),
        (redguy.pop_stack,)
    ]),
    (redguy.pop_stack,)
])

level9.safe_objlist=[ gameobjs.Wall((14, 25)),
                   gameobjs.Wall((14, 25)),
                   gameobjs.Wall((14, 24)),
                   gameobjs.Wall((14, 26)),
                   gameobjs.Wall((14, 23)),
                   gameobjs.Wall((14, 27)),
                   gameobjs.Wall((15, 27)),
                   gameobjs.Wall((16, 27)),
                   gameobjs.Wall((17, 27)),
                   gameobjs.Wall((18, 27)),
                   gameobjs.Wall((19, 27)),
                   gameobjs.Wall((20, 27)),
                   gameobjs.Wall((15, 23)),
                   gameobjs.Wall((16, 23)),
                   gameobjs.Wall((17, 23)),
                   gameobjs.Wall((18, 23)),
                   gameobjs.Wall((19, 23)),
                   gameobjs.Wall((20, 23)),
                   gameobjs.Wall((20, 24)),
                   gameobjs.Wall((20, 26)),
                   gameobjs.Wall((21, 23)),
                   gameobjs.Wall((22, 23)),
                   gameobjs.Wall((23, 23)),
                   gameobjs.Wall((24, 23)),
                   gameobjs.Wall((26, 23)),
                   gameobjs.Wall((25, 23)),
                   gameobjs.Wall((25, 24)),
                   gameobjs.Wall((25, 26)),
                   gameobjs.Wall((25, 27)),
                   gameobjs.Wall((24, 27)),
                   gameobjs.Wall((23, 27)),
                   gameobjs.Wall((22, 27)),
                   gameobjs.Wall((21, 27)),
                   gameobjs.Wall((26, 27)),
                   gameobjs.Wall((27, 23)),
                   gameobjs.Wall((28, 23)),
                   gameobjs.Wall((29, 23)),
                   gameobjs.Wall((27, 27)),
                   gameobjs.Wall((29, 27)),
                   gameobjs.Wall((28, 27)),
                   gameobjs.Wall((30, 27)),
                   gameobjs.Wall((30, 26)),
                   gameobjs.Wall((30, 24)),
                   gameobjs.Wall((30, 23)),
                   gameobjs.Wall((32, 23)),
                   gameobjs.Wall((31, 23)),
                   gameobjs.Wall((33, 23)),
                   gameobjs.Wall((34, 23)),
                   gameobjs.Wall((35, 23)),
                   gameobjs.Wall((35, 24)),
                   gameobjs.Wall((35, 26)),
                   gameobjs.Wall((35, 27)),
                   gameobjs.Wall((34, 27)),
                   gameobjs.Wall((33, 27)),
                   gameobjs.Wall((32, 27)),
                   gameobjs.Wall((31, 27)),
                   gameobjs.Wall((36, 23)),
                   gameobjs.Wall((38, 23)),
                   gameobjs.Wall((37, 23)),
                   gameobjs.Wall((39, 23)),
                   gameobjs.Wall((40, 23)),
                   gameobjs.Wall((40, 24)),
                   gameobjs.Wall((40, 26)),
                   gameobjs.Wall((40, 27)),
                   gameobjs.Wall((39, 27)),
                   gameobjs.Wall((38, 27)),
                   gameobjs.Wall((37, 27)),
                   gameobjs.Wall((36, 27)),
                   gameobjs.Wall((41, 23)),
                   gameobjs.Wall((42, 23)),
                   gameobjs.Wall((43, 23)),
                   gameobjs.Wall((44, 23)),
                   gameobjs.Wall((45, 23)),
                   gameobjs.Wall((45, 24)),
                   gameobjs.Wall((45, 26)),
                   gameobjs.Wall((45, 27)),
                   gameobjs.Wall((44, 27)),
                   gameobjs.Wall((43, 27)),
                   gameobjs.Wall((42, 27)),
                   gameobjs.Wall((41, 27)),
                   gameobjs.Wall((46, 23)),
                   gameobjs.Wall((47, 23)),
                   gameobjs.Wall((48, 23)),
                   gameobjs.Wall((49, 23)),
                   gameobjs.Wall((50, 23)),
                   gameobjs.Wall((51, 23)),
                   gameobjs.Wall((46, 27)),
                   gameobjs.Wall((47, 27)),
                   gameobjs.Wall((48, 27)),
                   gameobjs.Wall((49, 27)),
                   gameobjs.Wall((50, 27)),
                   gameobjs.Wall((51, 27)),
                   gameobjs.Wall((52, 27)),
                   gameobjs.Wall((52, 26)),
                   gameobjs.Wall((52, 25)),
                   gameobjs.Wall((52, 24)),
                   gameobjs.Wall((52, 23)),
                   gameobjs.Levelporter((50, 25),"Whoyougonnacall"),
                   gameobjs.Wall((32, 11)),
                   gameobjs.Wall((33, 11)),
                   gameobjs.Wall((34, 11)),
                   gameobjs.Wall((35, 11)),
                   gameobjs.Wall((36, 11)),
                   gameobjs.Wall((38, 11)),
                   gameobjs.Wall((39, 11)),
                   gameobjs.Wall((40, 11)),
                   gameobjs.Wall((41, 11)),
                   gameobjs.Wall((37, 13)),
                   gameobjs.Wall((36, 13)),
                   gameobjs.Wall((34, 13)),
                   gameobjs.Wall((35, 13)),
                   gameobjs.Wall((38, 13)),
                   gameobjs.Wall((39, 13)),
                   gameobjs.Wall((40, 13)),
                   gameobjs.Wall((41, 13)),
                   gameobjs.Wall((41, 9)),
                   gameobjs.Wall((40, 9)),
                   gameobjs.Wall((39, 9)),
                   gameobjs.Wall((38, 9)),
                   gameobjs.Wall((37, 9)),
                   gameobjs.Wall((35, 9)),
                   gameobjs.Wall((36, 9)),
                   gameobjs.Wall((34, 9)),
                   gameobjs.Wall((33, 9)),
                   gameobjs.Wall((32, 9)),
                   gameobjs.Wall((33, 13)),
                   gameobjs.Wall((32, 13)),
                   gameobjs.GravPad((37, 11),(1,0),1),
                   cyclingpad,
                   gameobjs.PressurePlate((34, 14),cycler),
                   gameobjs.Wall((31, 13)),
                   gameobjs.Wall((31, 12)),
                   gameobjs.Wall((31, 11)),
                   gameobjs.Wall((31, 10)),
                   gameobjs.Wall((31, 9)),
                   gameobjs.Wall((41, 9)),
                   gameobjs.Wall((41, 11)),
                   gameobjs.Wall((41, 13)),
                   gameobjs.Wall((42, 9)),
                   gameobjs.Wall((42, 11)),
                   gameobjs.Wall((42, 13)),
                   gameobjs.Wall((42, 10)),
                   gameobjs.Wall((42, 12)),
                   gameobjs.Boulder((32, 12),(1,0)),
                   gameobjs.Teleporter((41, 12),(32,12)),
                   gameobjs.Teleporter((41, 10),(32,12)),
                   drone,
                   retrigger1,
                   retrigger2,
                   gameobjs.PressurePlate((39, 12),retrigger1,singleimpulse=True),
                   gameobjs.PressurePlate((39, 10),retrigger2,singleimpulse=True),
                   redguy,
                   gameobjs.Teleporter((-1,-1),(13,23)) #This teleporter is used to teleport Red onto the screen
                   ]
level9.safe_objlist.extend(agates)
level9.safe_objlist.extend(bgates)
level9.unimportant.update((redguy,level9.get_at((-1,-1))))
level9.hints=["A uniselector, or linefinder, was a device used in the first telephone systems; it consisted of a set of"
              " electrical contacts and a stepper motor that rotated the head, which was connected to the dialing "
              "party's telephone. Each number on the spinning dial on the phone kept the motor spinning for a certain"
              " amount of time, bringing the head in contact with the wire that corresponded to that number; you'd have "
              "a handful of uniselectors chained together, each taking direction and input from the last one."
              " That's how dialing in the number actually electrically connected the two telephones together!",
              "So similarly, you have to 'dial' the pressureplate that controls the doors you want to go through by"
              " stepping the drone onto that pressure plate there.",
              "You COULD carefully time how many turns it takes for the boulder to get around to the gravpad...",
              "Did you do know that stuff you define in the console is available to functions you define in the console, "
              "even if they get called from somewhere else? I.e. a drone think?",
              "So if you define a list called direction, you can put things into it and pull them out inside the drone"
              "function.",
              "Things such as for example direction vectors.",
              "The next hint is the complete solution.",
              """
              directions=[]

              def mythink(self):
               if directions:
                self.move(*directions.pop())

              upload(mythink)

              directions.append((-1,0))

              Then alternate between (-1,0) and (1,0) as you proceed through the gates.
              """]

level10=Level("LEVEL 10: CROSS THE STREAM",[],gameobjs.Player((1,12)),playlists["Super-intense"])
pbe=gameobjs.ProtonBeamEmitter((41,11),0)
level10.safe_objlist=[gameobjs.Wall((0,11)),
                     gameobjs.Wall((1,11)),
                     gameobjs.Wall((2,11)),
                     gameobjs.Wall((3,11)),
                     gameobjs.Wall((4,11)),
                     gameobjs.Wall((5,11)),
                     gameobjs.Wall((6,11)),
                     gameobjs.Wall((7,11)),
                     gameobjs.Wall((8,11)),
                     gameobjs.Wall((9,11)),
                     gameobjs.Wall((10,11)),
                     gameobjs.Wall((11,11)),
                     gameobjs.Wall((12,11)),
                     gameobjs.Wall((13,11)),
                     gameobjs.Wall((14,11)),
                     gameobjs.Wall((15,11)),
                     gameobjs.Wall((16,11)),
                     gameobjs.Wall((18,11)),
                     gameobjs.Wall((17,11)),
                     gameobjs.Wall((19,11)),
                     gameobjs.Wall((20,11)),
                     gameobjs.Wall((21,11)),
                     gameobjs.Wall((22,11)),
                     gameobjs.Wall((23,11)),
                     gameobjs.Wall((24,11)),
                     gameobjs.Wall((25,11)),
                     gameobjs.Wall((26,11)),
                     gameobjs.Wall((27,11)),
                     gameobjs.Wall((28,11)),
                     gameobjs.Wall((29,11)),
                     gameobjs.Wall((30,11)),
                     gameobjs.Wall((31,11)),
                     gameobjs.Wall((32,11)),
                     gameobjs.Wall((33,11)),
                     gameobjs.Wall((34,11)),
                     gameobjs.Wall((35,11)),
                     gameobjs.Wall((36,11)),
                     gameobjs.Wall((37,11)),
                     gameobjs.Wall((38,11)),
                     gameobjs.Wall((39,11)),
                     gameobjs.Wall((40,11)),
                     gameobjs.Wall((42,11)),
                     gameobjs.Wall((43,11)),
                     gameobjs.Wall((44,11)),
                     gameobjs.Wall((45,11)),
                     gameobjs.Wall((47,9)),
                     gameobjs.Wall((48,11)),
                     gameobjs.Wall((49,11)),
                     gameobjs.Wall((50,11)),
                     gameobjs.Wall((51,11)),
                     gameobjs.Wall((52,11)),
                     gameobjs.Wall((53,11)),
                     gameobjs.Wall((54,11)),
                     gameobjs.Wall((55,11)),
                     gameobjs.Wall((55,12)),
                     gameobjs.Wall((55,13)),
                     gameobjs.Wall((55,14)),
                     gameobjs.Wall((55,15)),
                     gameobjs.Wall((55,16)),
                     gameobjs.Wall((55,19)),
                     gameobjs.Wall((54,19)),
                     gameobjs.Wall((53,19)),
                     gameobjs.Wall((52,19)),
                     gameobjs.Wall((51,19)),
                     gameobjs.Wall((50,19)),
                     gameobjs.Wall((49,19)),
                     gameobjs.Wall((48,19)),
                     gameobjs.Wall((47,19)),
                     gameobjs.Wall((46,19)),
                     gameobjs.Wall((45,19)),
                     gameobjs.Wall((44,19)),
                     gameobjs.Wall((43,19)),
                     gameobjs.Wall((42,19)),
                     gameobjs.Wall((41,19)),
                     gameobjs.Wall((40,19)),
                     gameobjs.Wall((39,19)),
                     gameobjs.Wall((38,19)),
                     gameobjs.Wall((36,19)),
                     gameobjs.Wall((37,19)),
                     gameobjs.Wall((35,19)),
                     gameobjs.Wall((34,19)),
                     gameobjs.Wall((33,19)),
                     gameobjs.Wall((32,19)),
                     gameobjs.Wall((31,19)),
                     gameobjs.Wall((30,19)),
                     gameobjs.Wall((29,19)),
                     gameobjs.Wall((28,19)),
                     gameobjs.Wall((28,17)),
                     gameobjs.Wall((28,16)),
                     gameobjs.Wall((28,15)),
                     gameobjs.Wall((28,14)),
                     gameobjs.Wall((28,13)),
                     gameobjs.Wall((27,13)),
                     gameobjs.Wall((26,13)),
                     gameobjs.Wall((25,13)),
                     gameobjs.Wall((24,13)),
                     gameobjs.Wall((55,17)),
                     gameobjs.Wall((56,17)),
                     gameobjs.Wall((57,17)),
                     gameobjs.Wall((58,17)),
                     gameobjs.Wall((59,17)),
                     gameobjs.Wall((60,17)),
                     gameobjs.Wall((61,17)),
                     gameobjs.Wall((62,17)),
                     gameobjs.Wall((63,17)),
                     gameobjs.Wall((64,17)),
                     gameobjs.Wall((65,17)),
                     gameobjs.Wall((66,17)),
                     gameobjs.Wall((68,17)),
                     gameobjs.Wall((67,17)),
                     gameobjs.Wall((69,17)),
                     gameobjs.Wall((70,17)),
                     gameobjs.Wall((71,17)),
                     gameobjs.Wall((72,17)),
                     gameobjs.Wall((73,17)),
                     gameobjs.Wall((74,17)),
                     gameobjs.Wall((75,17)),
                     gameobjs.Wall((77,17)),
                     gameobjs.Wall((78,17)),
                     gameobjs.Wall((79,17)),
                     gameobjs.Wall((76,17)),
                     gameobjs.Wall((57,19)),
                     gameobjs.Wall((56,19)),
                     gameobjs.Wall((58,19)),
                     gameobjs.Wall((59,19)),
                     gameobjs.Wall((60,19)),
                     gameobjs.Wall((61,19)),
                     gameobjs.Wall((62,19)),
                     gameobjs.Wall((63,19)),
                     gameobjs.Wall((64,19)),
                     gameobjs.Wall((65,19)),
                     gameobjs.Wall((66,19)),
                     gameobjs.Wall((67,19)),
                     gameobjs.Wall((68,19)),
                     gameobjs.Wall((69,19)),
                     gameobjs.Wall((70,19)),
                     gameobjs.Wall((71,19)),
                     gameobjs.Wall((72,19)),
                     gameobjs.Wall((73,19)),
                     gameobjs.Wall((75,19)),
                     gameobjs.Wall((74,19)),
                     gameobjs.Wall((76,19)),
                     gameobjs.Wall((78,19)),
                     gameobjs.Wall((79,19)),
                     gameobjs.Wall((77,19)),
                     gameobjs.Wall((23,13)),
                     gameobjs.Wall((22,13)),
                     gameobjs.Wall((21,13)),
                     gameobjs.Wall((20,13)),
                     gameobjs.Wall((19,13)),
                     gameobjs.Wall((18,13)),
                     gameobjs.Wall((17,13)),
                     gameobjs.Wall((16,13)),
                     gameobjs.Wall((15,13)),
                     gameobjs.Wall((14,13)),
                     gameobjs.Wall((13,13)),
                     gameobjs.Wall((12,13)),
                     gameobjs.Wall((11,13)),
                     gameobjs.Wall((10,13)),
                     gameobjs.Wall((9,13)),
                     gameobjs.Wall((8,13)),
                     gameobjs.Wall((7,13)),
                     gameobjs.Wall((6,13)),
                     gameobjs.Wall((5,13)),
                     gameobjs.Wall((4,13)),
                     gameobjs.Wall((3,13)),
                     gameobjs.Wall((2,13)),
                     gameobjs.Wall((1,13)),
                     gameobjs.Wall((0,13)),
                     gameobjs.Wall((40,10)),
                     gameobjs.Wall((41,10)),
                     gameobjs.Wall((42,10)),
                     pbe,
                     gameobjs.Wall((0,12)),
                     gameobjs.Levelporter((79,18),"Run"),
                     gameobjs.MissileLauncher((28,18),level10.plyobj.proxy),
                     gameobjs.Wall((27,17)),
                     gameobjs.Wall((27,19)),
                     gameobjs.ArbitraryCodeExecutor((39,10),"pbe.angle+=x\nif abs(pbe.angle) >= 85:\n  x=-x",{"pbe": pbe,"x": 2}),
                     gameobjs.Wall((51,15)),
                     gameobjs.Wall((50,15)),
                     gameobjs.Wall((49,15)),
                     gameobjs.Wall((34,14)),
                     gameobjs.Wall((32,14)),
                     gameobjs.Wall((32,15)),
                     gameobjs.MissileLauncher((46,10),level10.plyobj.proxy),
                     gameobjs.Wall((45,10)),
                     gameobjs.Wall((45,9)),
                     gameobjs.Wall((46,9)),
                     gameobjs.Wall((48,9)),
                     gameobjs.Wall((48,10)),
                     gameobjs.Wall((27,18)),
                     gameobjs.Wall((34,15))]
level10.hints=["Nothin' to see in here. Those yellow wigglies are proton beams, by the way, and the ¥s are particle beam"
               " emitters. The beams destroy most anything they touch, especially you.",
               "You're gonna have to take cover behind those walls from the proton beams and the missiles. "
               "There's a pattern.",
               "Actually, there's almost always a clever way around these twitch-reflex challenges.",
               "Hey look, you can access the missiles!",
               "What happens if you stick something stupid in their target parameter?",
               "Oh. An exception. Huh... wonder what int_pos() does?",
               "Hey, maybe it returns a position! As ints! In a tuple!",
               "Look, I'm not going to spell this out for you. You need to make a new class with an int_pos() method.",
               "Seriously. Maybe give the running and ducking another try. It's fun.",
               "Okay, FINE. The next hint is the complete solution.",
               """
               class Decoy:
                def __init__(self,pos):
                 self.pos=pos
                def int_pos(self):
                 return self.pos
                ;

                Wait for a round of missiles to fire...

                objs[0].target=Decoy((28,18))
                objs[1].target=Decoy((46,10))
                ;

                You still have to hide from the lasers, but that's easy.
                """]

level11=Level("LEVEL 11: RUN.",[],gameobjs.Player((18,7)),playlists["Super-intense"])
pbe=gameobjs.ProtonBeamEmitter((25,3),0)
level11.safe_objlist=[gameobjs.Wall((18,6)),
                     gameobjs.Wall((19,6)),
                     #gameobjs.Wall((20,6)),
                     gameobjs.Wall((17,6)),
                     gameobjs.Wall((16,6)),
                     gameobjs.Wall((15,6)),
                     gameobjs.Wall((14,6)),
                     gameobjs.Wall((13,6)),
                     gameobjs.Wall((12,6)),
                     gameobjs.Wall((12,7)),
                     gameobjs.Wall((12,8)),
                     gameobjs.Wall((13,8)),
                     gameobjs.Wall((14,8)),
                     gameobjs.Wall((15,8)),
                     gameobjs.Wall((16,8)),
                     gameobjs.Wall((17,8)),
                     gameobjs.Wall((18,8)),
                     gameobjs.Wall((19,8)),
                     gameobjs.Wall((20,8)),
                     gameobjs.Wall((22,6)),
                     gameobjs.Wall((24,6)),
                     gameobjs.Wall((26,6)),
                     gameobjs.Wall((28,6)),
                     gameobjs.Wall((21,8)),
                     gameobjs.Wall((22,8)),
                     gameobjs.Wall((24,8)),
                     gameobjs.Wall((23,8)),
                     gameobjs.Wall((25,8)),
                     gameobjs.Wall((26,8)),
                     gameobjs.Wall((27,8)),
                     gameobjs.Wall((28,8)),
                     gameobjs.Wall((29,8)),
                     gameobjs.Wall((30,8)),
                     gameobjs.Wall((31,8)),
                     gameobjs.Wall((32,8)),
                     gameobjs.Wall((33,8)),
                     #gameobjs.Wall((30,6)),
                     gameobjs.Wall((31,6)),
                     gameobjs.Wall((32,6)),
                     gameobjs.Wall((33,6)),
                     gameobjs.Wall((34,6)),
                     gameobjs.Wall((34,8)),
                     gameobjs.Wall((35,8)),
                     gameobjs.Wall((36,8)),
                     gameobjs.Wall((35,6)),
                     gameobjs.Wall((36,6)),
                     gameobjs.Wall((37,6)),
                     gameobjs.Wall((37,8)),
                     gameobjs.Wall((38,6)),
                     gameobjs.Wall((38,8)),
                     gameobjs.Wall((39,6)),
                     gameobjs.Wall((39,8)),
                     gameobjs.Wall((40,6)),
                     gameobjs.Wall((41,6)),
                     gameobjs.Wall((41,7)),
                     gameobjs.Wall((41,8)),
                     gameobjs.Wall((41,9)),
                     gameobjs.Wall((41,10)),
                     gameobjs.Wall((40,10)),
                     gameobjs.Wall((39,10)),
                     gameobjs.Wall((38,10)),
                     gameobjs.Wall((37,10)),
                     gameobjs.Wall((35,9)),
                     gameobjs.Wall((35,10)),
                     gameobjs.Wall((35,11)),
                     gameobjs.Wall((35,12)),
                     gameobjs.Wall((36,12)),
                     gameobjs.Wall((37,12)),
                     gameobjs.Wall((39,12)),
                     gameobjs.Wall((38,12)),
                     gameobjs.Wall((41,11)),
                     gameobjs.Wall((41,12)),
                     gameobjs.Wall((41,14)),
                     gameobjs.Wall((40,14)),
                     gameobjs.Wall((39,14)),
                     gameobjs.Wall((37,14)),
                     gameobjs.Wall((38,14)),
                     gameobjs.Wall((36,14)),
                     gameobjs.Wall((35,14)),
                     gameobjs.Wall((34,14)),
                     gameobjs.Wall((33,14)),
                     gameobjs.Wall((33,12)),
                     gameobjs.Wall((34,12)),
                     gameobjs.Wall((32,12)),
                     gameobjs.Wall((31,12)),
                     gameobjs.Wall((29,12)),
                     gameobjs.Wall((32,14)),
                     gameobjs.Wall((30,14)),
                     gameobjs.Wall((31,14)),
                     gameobjs.Wall((29,14)),
                     gameobjs.Wall((27,12)),
                     gameobjs.Wall((25,12)),
                     gameobjs.Wall((23,12)),
                     gameobjs.Wall((21,12)),
                     gameobjs.Wall((19,12)),
                     gameobjs.Wall((18,12)),
                     gameobjs.Wall((17,12)),
                     gameobjs.Wall((16,12)),
                     gameobjs.Wall((15,12)),
                     gameobjs.Wall((14,12)),
                     gameobjs.Wall((28,14)),
                     gameobjs.Wall((27,14)),
                     gameobjs.Wall((25,14)),
                     gameobjs.Wall((26,14)),
                     gameobjs.Wall((24,14)),
                     gameobjs.Wall((23,14)),
                     gameobjs.Wall((22,14)),
                     gameobjs.Wall((21,14)),
                     gameobjs.Wall((20,14)),
                     gameobjs.Wall((19,14)),
                     gameobjs.Wall((18,14)),
                     gameobjs.Wall((17,14)),
                     gameobjs.Wall((16,14)),
                     gameobjs.Wall((15,14)),
                     gameobjs.Wall((14,14)),
                     gameobjs.Wall((12,13)),
                     gameobjs.Wall((12,12)),
                     gameobjs.Wall((12,14)),
                     gameobjs.Wall((13,14)),
                     gameobjs.Wall((13,12)),
                     pbe,
                     gameobjs.GravPad((22,2),(1,0),1),
                     gameobjs.GravPad((28,2),(-1,0),1),
                     gameobjs.PressurePlate((25,1),gameobjs.AttributeCycler(pbe,"angle",[-62,-42,0,42,62,42,0,-42]),singleimpulse=True),
                     gameobjs.Boulder((28,0)),
                     gameobjs.Levelporter((13,13),"Hub"),
                     gameobjs.Wall((20,12)),
                     gameobjs.Wall((22,12)),
                     gameobjs.Wall((24,12)),
                     gameobjs.Wall((26,12)),
                     gameobjs.Wall((28,12)),
                     gameobjs.Wall((42,12)),
                     gameobjs.Wall((43,12)),
                     gameobjs.Wall((43,14)),
                     gameobjs.Wall((42,14)),
                     gameobjs.Wall((44,12)),
                     gameobjs.Wall((45,12)),
                     gameobjs.Wall((45,13)),
                     gameobjs.Wall((45,14)),
                     gameobjs.Wall((44,14)),
                     gameobjs.MissileLauncher((44,13),level11.plyobj,rate=40,addsafe=True),
                     gameobjs.MissileLauncher((13,7),level11.plyobj,rate=40,addsafe=True)]
level11.objlist=[gameobjs.ProtonBeamEmitter((30,9),0)]
level11.hints=["Run."]

level12=Level("LEVEL 12: THE MIXER",[],gameobjs.Player((40,8)),playlists["Calm"])
pbe=gameobjs.ProtonBeamEmitter((40,11),45)
drone=gameobjs.Drone((40,5))
level12.objlist=[gameobjs.Boulder((25,11))]
level12.safe_objlist=[gameobjs.Wall((30,6)),
                     gameobjs.Wall((31,6)),
                     gameobjs.Wall((32,6)),
                     gameobjs.Wall((33,6)),
                     gameobjs.Wall((34,6)),
                     gameobjs.Wall((35,6)),
                     gameobjs.Wall((36,6)),
                     gameobjs.Wall((37,6)),
                     gameobjs.Wall((38,6)),
                     gameobjs.Wall((39,6)),
                     gameobjs.Wall((41,6)),
                     gameobjs.Wall((42,6)),
                     gameobjs.Wall((43,6)),
                     gameobjs.Wall((44,6)),
                     gameobjs.Wall((45,6)),
                     gameobjs.Wall((46,6)),
                     gameobjs.Wall((47,6)),
                     gameobjs.Wall((48,6)),
                     gameobjs.Wall((49,6)),
                     gameobjs.Wall((50,6)),
                     gameobjs.Wall((51,6)),
                     gameobjs.Wall((51,7)),
                     gameobjs.Wall((51,8)),
                     gameobjs.Wall((51,9)),
                     gameobjs.Wall((51,10)),
                     gameobjs.Wall((51,12)),
                     gameobjs.Wall((51,13)),
                     gameobjs.Wall((51,14)),
                     gameobjs.Wall((51,15)),
                     gameobjs.Wall((51,16)),
                     gameobjs.Wall((50,16)),
                     gameobjs.Wall((49,16)),
                     gameobjs.Wall((47,16)),
                     gameobjs.Wall((48,16)),
                     gameobjs.Wall((46,16)),
                     gameobjs.Wall((45,16)),
                     gameobjs.Wall((43,16)),
                     gameobjs.Wall((44,16)),
                     gameobjs.Wall((42,16)),
                     gameobjs.Wall((41,16)),
                     gameobjs.Wall((39,16)),
                     gameobjs.Wall((38,16)),
                     gameobjs.Wall((37,16)),
                     gameobjs.Wall((36,16)),
                     gameobjs.Wall((35,16)),
                     gameobjs.Wall((34,16)),
                     gameobjs.Wall((33,16)),
                     gameobjs.Wall((32,16)),
                     gameobjs.Wall((31,16)),
                     gameobjs.Wall((30,16)),
                     gameobjs.Wall((29,16)),
                     gameobjs.Wall((29,6)),
                     gameobjs.Wall((29,7)),
                     gameobjs.Wall((29,8)),
                     gameobjs.Wall((29,9)),
                     gameobjs.Wall((29,10)),
                     gameobjs.Wall((29,12)),
                     gameobjs.Wall((29,13)),
                     gameobjs.Wall((29,14)),
                     gameobjs.Wall((29,15)),
                     pbe,
                     gameobjs.Gate((40,16)),
                     gameobjs.Wall((39,17)),
                     gameobjs.Wall((39,18)),
                     gameobjs.Wall((39,19)),
                     gameobjs.Wall((41,17)),
                     gameobjs.Wall((41,18)),
                     gameobjs.Wall((41,19)),
                     gameobjs.Wall((39,20)),
                     gameobjs.Wall((41,20)),
                     gameobjs.Wall((41,21)),
                     gameobjs.Wall((39,21)),
                     gameobjs.Wall((40,21)),
                     gameobjs.Levelporter((40,20),"Amazeing"),
                     gameobjs.Gate((29,11)),
                     gameobjs.Gate((51,11)),
                     gameobjs.Gate((40,6)),
                     gameobjs.Wall((41,5)),
                     gameobjs.Wall((39,5)),
                     gameobjs.Wall((39,4)),
                     gameobjs.Wall((40,4)),
                     gameobjs.Wall((41,4)),
                     drone,
                     gameobjs.ProtonBeamEmitter((27,9),0),
                     gameobjs.Wall((26,9)),
                     gameobjs.Wall((26,8)),
                     gameobjs.Wall((27,8)),
                     gameobjs.Wall((28,8)),
                     gameobjs.Wall((26,10)),
                     gameobjs.Wall((26,12)),
                     gameobjs.Wall((26,13)),
                     gameobjs.Wall((28,14)),
                     gameobjs.Wall((27,14)),
                     gameobjs.Wall((26,14)),
                     gameobjs.Wall((25,10)),
                     gameobjs.Wall((24,10)),
                     gameobjs.Wall((24,11)),
                     gameobjs.Wall((24,12)),
                     gameobjs.Wall((25,12)),
                     gameobjs.Wall((52,10)),
                     gameobjs.Wall((53,10)),
                     gameobjs.Wall((53,12)),
                     gameobjs.Wall((52,12)),
                     gameobjs.Wall((54,10)),
                     gameobjs.Wall((55,10)),
                     gameobjs.Wall((55,11)),
                     gameobjs.Wall((55,12)),
                     gameobjs.Wall((54,12)),
                     gameobjs.Gate((40,17)),
                     gameobjs.Gate((40,18)),
                     gameobjs.ArbitraryCodeExecutor((34,5),"pbe.angle-=2\nif pbe.angle <= 0:\n pbe.angle=359",{"pbe":pbe})]
level12.safe_objlist.extend([gameobjs.ProtonBeamReceiver((30,15),level12.get_at((51,11))),
                             gameobjs.ProtonBeamReceiver((30,7),level12.get_at((40,16))),
                             gameobjs.ProtonBeamReceiver((50,7),level12.get_at((29,11))),
                             gameobjs.ProtonBeamReceiver((50,15),level12.get_at((40,6))),
                             gameobjs.SafePressurePlate((54,11),level12.get_at((40,17))),
                             gameobjs.SafePressurePlate((53,11),level12.get_at((40,18)))])
level12.extralocals={"upload": drone.upload}
level12.hints=["Oh boy. Look out for that proton beam.",
               "Honestly, this is pretty much just rote practice for stuff you've already done.",
               "Those copper-coloured ◘s are proton beam receivers; they trigger things when a proton beam touches them.",
               "The receivers all trigger the gate furthest from them.",
               "Okay, first you'll want to bust the drone out of its cell up there.",
               "Your ultimate objective is to get that boulder and the drone onto the pressure plates on the right.",
               "Oh, and there's some really cheap solutions to this one if you're a little bit clever.",
               "For example, you know how drones can change things about things that they touch?",
               "That might come in handy. That's all I'm gonna say.",
               "...",
               "Okay, FINE. The next hint is a drone think function that should be all you need to solve the level yourself.",
               """
#Control the drone by appending direction vectors to the moves list.
moves=[]
bumps=[]
def mythink(self):
if moves:
 self.move(*moves.pop(0))
if self.bumped:
 if self.bumped not in bumps:
  bumps.append(self.bumped)
 if "ProtonBeamEmitter" in str(type(self.bumped)):
  self.bumped.active=False"""]

level13=Level("LEVEL 13: ALL ALIKE / GO IN DEEP",[],gameobjs.Player((15, 5)),playlists["Calm"])
gate=gameobjs.Gate(startpos=(38, 5))
drone=gameobjs.Drone(startpos=(12, 23))
level13.objlist=[]
level13.safe_objlist=[gameobjs.Wall(startpos=(15, 6)),
                   gameobjs.Wall(startpos=(14, 6)),
                   gameobjs.Wall(startpos=(14, 5)),
                   gameobjs.Wall(startpos=(14, 4)),
                   gameobjs.Wall(startpos=(15, 4)),
                   gameobjs.Wall(startpos=(16, 4)),
                   gameobjs.Wall(startpos=(17, 4)),
                   gameobjs.Wall(startpos=(18, 4)),
                   gameobjs.Wall(startpos=(19, 4)),
                   gameobjs.Wall(startpos=(20, 4)),
                   gameobjs.Wall(startpos=(21, 4)),
                   gameobjs.Wall(startpos=(22, 4)),
                   gameobjs.Wall(startpos=(23, 4)),
                   gameobjs.Wall(startpos=(24, 4)),
                   gameobjs.Wall(startpos=(16, 6)),
                   gameobjs.Wall(startpos=(17, 6)),
                   gameobjs.Wall(startpos=(18, 6)),
                   gameobjs.Wall(startpos=(19, 6)),
                   gameobjs.Wall(startpos=(20, 6)),
                   gameobjs.Wall(startpos=(21, 6)),
                   gameobjs.Wall(startpos=(22, 6)),
                   gameobjs.Wall(startpos=(23, 6)),
                   gameobjs.Wall(startpos=(24, 6)),
                   gameobjs.Wall(startpos=(25, 6)),
                   gameobjs.Wall(startpos=(25, 4)),
                   gameobjs.Wall(startpos=(26, 4)),
                   gameobjs.Wall(startpos=(27, 4)),
                   gameobjs.Wall(startpos=(28, 4)),
                   gameobjs.Wall(startpos=(29, 4)),
                   gameobjs.Wall(startpos=(30, 4)),
                   gameobjs.Wall(startpos=(31, 4)),
                   gameobjs.Wall(startpos=(32, 4)),
                   gameobjs.Wall(startpos=(33, 4)),
                   gameobjs.Wall(startpos=(34, 4)),
                   gameobjs.Wall(startpos=(35, 4)),
                   gameobjs.Wall(startpos=(36, 4)),
                   gameobjs.Wall(startpos=(37, 4)),
                   gameobjs.Wall(startpos=(38, 4)),
                   gameobjs.Wall(startpos=(39, 4)),
                   gameobjs.Wall(startpos=(40, 4)),
                   gameobjs.Wall(startpos=(41, 4)),
                   gameobjs.Wall(startpos=(42, 4)),
                   gameobjs.Wall(startpos=(43, 4)),
                   gameobjs.Wall(startpos=(44, 4)),
                   gameobjs.Wall(startpos=(45, 4)),
                   gameobjs.Wall(startpos=(46, 4)),
                   gameobjs.Wall(startpos=(47, 4)),
                   gameobjs.Wall(startpos=(48, 4)),
                   gameobjs.Wall(startpos=(49, 4)),
                   gameobjs.Wall(startpos=(50, 4)),
                   gameobjs.Wall(startpos=(51, 4)),
                   gameobjs.Wall(startpos=(52, 4)),
                   gameobjs.Wall(startpos=(53, 4)),
                   gameobjs.Wall(startpos=(54, 4)),
                   gameobjs.Wall(startpos=(55, 4)),
                   gameobjs.Wall(startpos=(56, 4)),
                   gameobjs.Wall(startpos=(57, 4)),
                   gameobjs.Wall(startpos=(58, 4)),
                   gameobjs.Wall(startpos=(59, 4)),
                   gameobjs.Wall(startpos=(60, 4)),
                   gameobjs.Wall(startpos=(61, 4)),
                   gameobjs.Wall(startpos=(61, 5)),
                   gameobjs.Wall(startpos=(61, 6)),
                   gameobjs.Wall(startpos=(60, 6)),
                   gameobjs.Wall(startpos=(59, 6)),
                   gameobjs.Wall(startpos=(58, 6)),
                   gameobjs.Wall(startpos=(57, 6)),
                   gameobjs.Wall(startpos=(56, 6)),
                   gameobjs.Wall(startpos=(55, 6)),
                   gameobjs.Wall(startpos=(54, 6)),
                   gameobjs.Wall(startpos=(53, 6)),
                   gameobjs.Wall(startpos=(52, 6)),
                   gameobjs.Wall(startpos=(51, 6)),
                   gameobjs.Wall(startpos=(50, 6)),
                   gameobjs.Wall(startpos=(49, 6)),
                   gameobjs.Wall(startpos=(48, 6)),
                   gameobjs.Wall(startpos=(47, 6)),
                   gameobjs.Wall(startpos=(46, 6)),
                   gameobjs.Wall(startpos=(45, 6)),
                   gameobjs.Wall(startpos=(44, 6)),
                   gameobjs.Wall(startpos=(43, 6)),
                   gameobjs.Wall(startpos=(42, 6)),
                   gameobjs.Wall(startpos=(41, 6)),
                   gameobjs.Wall(startpos=(40, 6)),
                   gameobjs.Wall(startpos=(39, 6)),
                   gameobjs.Wall(startpos=(38, 6)),
                   gameobjs.Wall(startpos=(37, 6)),
                   gameobjs.Wall(startpos=(36, 6)),
                   gameobjs.Wall(startpos=(35, 6)),
                   gameobjs.Wall(startpos=(34, 6)),
                   gameobjs.Wall(startpos=(33, 6)),
                   gameobjs.Wall(startpos=(32, 6)),
                   gameobjs.Wall(startpos=(31, 6)),
                   gameobjs.Wall(startpos=(30, 6)),
                   gameobjs.Wall(startpos=(29, 6)),
                   gameobjs.Wall(startpos=(28, 6)),
                   gameobjs.Wall(startpos=(27, 6)),
                   gameobjs.Wall(startpos=(26, 6)),
                   gameobjs.Wall(startpos=(14, 8)),
                   gameobjs.Wall(startpos=(15, 8)),
                   gameobjs.Wall(startpos=(16, 8)),
                   gameobjs.Wall(startpos=(17, 8)),
                   gameobjs.Wall(startpos=(18, 8)),
                   gameobjs.Wall(startpos=(19, 8)),
                   gameobjs.Wall(startpos=(20, 8)),
                   gameobjs.Wall(startpos=(21, 8)),
                   gameobjs.Wall(startpos=(22, 8)),
                   gameobjs.Wall(startpos=(23, 8)),
                   gameobjs.Wall(startpos=(24, 8)),
                   gameobjs.Wall(startpos=(25, 8)),
                   gameobjs.Wall(startpos=(26, 8)),
                   gameobjs.Wall(startpos=(27, 8)),
                   gameobjs.Wall(startpos=(28, 8)),
                   gameobjs.Wall(startpos=(29, 8)),
                   gameobjs.Wall(startpos=(30, 8)),
                   gameobjs.Wall(startpos=(31, 8)),
                   gameobjs.Wall(startpos=(32, 8)),
                   gameobjs.Wall(startpos=(33, 8)),
                   gameobjs.Wall(startpos=(34, 8)),
                   gameobjs.Wall(startpos=(35, 8)),
                   gameobjs.Wall(startpos=(36, 8)),
                   gameobjs.Wall(startpos=(37, 8)),
                   gameobjs.Wall(startpos=(38, 8)),
                   gameobjs.Wall(startpos=(39, 8)),
                   gameobjs.Wall(startpos=(40, 8)),
                   gameobjs.Wall(startpos=(41, 8)),
                   gameobjs.Wall(startpos=(42, 8)),
                   gameobjs.Wall(startpos=(43, 8)),
                   gameobjs.Wall(startpos=(44, 8)),
                   gameobjs.Wall(startpos=(45, 8)),
                   gameobjs.Wall(startpos=(46, 8)),
                   gameobjs.Wall(startpos=(47, 8)),
                   gameobjs.Wall(startpos=(48, 8)),
                   gameobjs.Wall(startpos=(49, 8)),
                   gameobjs.Wall(startpos=(50, 8)),
                   gameobjs.Wall(startpos=(51, 8)),
                   gameobjs.Wall(startpos=(52, 8)),
                   gameobjs.Wall(startpos=(53, 8)),
                   gameobjs.Wall(startpos=(54, 8)),
                   gameobjs.Wall(startpos=(55, 8)),
                   gameobjs.Wall(startpos=(56, 8)),
                   gameobjs.Wall(startpos=(57, 8)),
                   gameobjs.Wall(startpos=(58, 8)),
                   gameobjs.Wall(startpos=(59, 8)),
                   gameobjs.Wall(startpos=(61, 8)),
                   gameobjs.Wall(startpos=(61, 9)),
                   gameobjs.Wall(startpos=(61, 10)),
                   gameobjs.Wall(startpos=(61, 11)),
                   gameobjs.Wall(startpos=(61, 12)),
                   gameobjs.Wall(startpos=(61, 13)),
                   gameobjs.Wall(startpos=(61, 14)),
                   gameobjs.Wall(startpos=(61, 15)),
                   gameobjs.Wall(startpos=(61, 16)),
                   gameobjs.Wall(startpos=(61, 17)),
                   gameobjs.Wall(startpos=(61, 18)),
                   gameobjs.Wall(startpos=(61, 19)),
                   gameobjs.Wall(startpos=(61, 20)),
                   gameobjs.Wall(startpos=(61, 21)),
                   gameobjs.Wall(startpos=(61, 22)),
                   gameobjs.Wall(startpos=(61, 23)),
                   gameobjs.Wall(startpos=(61, 24)),
                   gameobjs.Wall(startpos=(60, 24)),
                   gameobjs.Wall(startpos=(59, 24)),
                   gameobjs.Wall(startpos=(58, 24)),
                   gameobjs.Wall(startpos=(57, 24)),
                   gameobjs.Wall(startpos=(56, 24)),
                   gameobjs.Wall(startpos=(55, 24)),
                   gameobjs.Wall(startpos=(54, 24)),
                   gameobjs.Wall(startpos=(53, 24)),
                   gameobjs.Wall(startpos=(52, 24)),
                   gameobjs.Wall(startpos=(51, 24)),
                   gameobjs.Wall(startpos=(50, 24)),
                   gameobjs.Wall(startpos=(49, 24)),
                   gameobjs.Wall(startpos=(48, 24)),
                   gameobjs.Wall(startpos=(47, 24)),
                   gameobjs.Wall(startpos=(46, 24)),
                   gameobjs.Wall(startpos=(45, 24)),
                   gameobjs.Wall(startpos=(44, 24)),
                   gameobjs.Wall(startpos=(43, 24)),
                   gameobjs.Wall(startpos=(42, 24)),
                   gameobjs.Wall(startpos=(41, 24)),
                   gameobjs.Wall(startpos=(40, 24)),
                   gameobjs.Wall(startpos=(39, 24)),
                   gameobjs.Wall(startpos=(38, 24)),
                   gameobjs.Wall(startpos=(37, 24)),
                   gameobjs.Wall(startpos=(36, 24)),
                   gameobjs.Wall(startpos=(34, 24)),
                   gameobjs.Wall(startpos=(33, 24)),
                   gameobjs.Wall(startpos=(32, 24)),
                   gameobjs.Wall(startpos=(31, 24)),
                   gameobjs.Wall(startpos=(30, 24)),
                   gameobjs.Wall(startpos=(29, 24)),
                   gameobjs.Wall(startpos=(28, 24)),
                   gameobjs.Wall(startpos=(27, 24)),
                   gameobjs.Wall(startpos=(26, 24)),
                   gameobjs.Wall(startpos=(25, 24)),
                   gameobjs.Wall(startpos=(24, 24)),
                   gameobjs.Wall(startpos=(23, 24)),
                   gameobjs.Wall(startpos=(22, 24)),
                   gameobjs.Wall(startpos=(14, 9)),
                   gameobjs.Wall(startpos=(14, 10)),
                   gameobjs.Wall(startpos=(14, 11)),
                   gameobjs.Wall(startpos=(14, 12)),
                   gameobjs.Wall(startpos=(14, 13)),
                   gameobjs.Wall(startpos=(14, 14)),
                   gameobjs.Wall(startpos=(14, 15)),
                   gameobjs.Wall(startpos=(14, 16)),
                   gameobjs.Wall(startpos=(14, 17)),
                   gameobjs.Wall(startpos=(14, 18)),
                   gameobjs.Wall(startpos=(14, 19)),
                   gameobjs.Wall(startpos=(14, 20)),
                   gameobjs.Wall(startpos=(14, 21)),
                   gameobjs.Wall(startpos=(14, 22)),
                   gameobjs.Wall(startpos=(14, 24)),
                   gameobjs.Wall(startpos=(15, 24)),
                   gameobjs.Wall(startpos=(16, 24)),
                   gameobjs.Wall(startpos=(17, 24)),
                   gameobjs.Wall(startpos=(18, 24)),
                   gameobjs.Wall(startpos=(19, 24)),
                   gameobjs.Wall(startpos=(20, 24)),
                   gameobjs.Wall(startpos=(21, 24)),
                   gameobjs.Wall(startpos=(16, 22)),
                   gameobjs.Wall(startpos=(16, 21)),
                   gameobjs.Wall(startpos=(16, 20)),
                   gameobjs.Wall(startpos=(16, 19)),
                   gameobjs.Wall(startpos=(16, 16)),
                   gameobjs.Wall(startpos=(16, 15)),
                   gameobjs.Wall(startpos=(16, 14)),
                   gameobjs.Wall(startpos=(16, 13)),
                   gameobjs.Wall(startpos=(16, 10)),
                   gameobjs.Wall(startpos=(17, 10)),
                   gameobjs.Wall(startpos=(18, 10)),
                   gameobjs.Wall(startpos=(19, 10)),
                   gameobjs.Wall(startpos=(20, 10)),
                   gameobjs.Wall(startpos=(21, 10)),
                   gameobjs.Wall(startpos=(23, 10)),
                   gameobjs.Wall(startpos=(25, 10)),
                   gameobjs.Wall(startpos=(25, 9)),
                   gameobjs.Wall(startpos=(27, 10)),
                   gameobjs.Wall(startpos=(26, 10)),
                   gameobjs.Wall(startpos=(28, 10)),
                   gameobjs.Wall(startpos=(29, 10)),
                   gameobjs.Wall(startpos=(30, 10)),
                   gameobjs.Wall(startpos=(32, 10)),
                   gameobjs.Wall(startpos=(34, 10)),
                   gameobjs.Wall(startpos=(35, 10)),
                   gameobjs.Wall(startpos=(36, 10)),
                   gameobjs.Wall(startpos=(37, 10)),
                   gameobjs.Wall(startpos=(40, 10)),
                   gameobjs.Wall(startpos=(42, 10)),
                   gameobjs.Wall(startpos=(44, 10)),
                   gameobjs.Wall(startpos=(45, 10)),
                   gameobjs.Wall(startpos=(46, 10)),
                   gameobjs.Wall(startpos=(47, 10)),
                   gameobjs.Wall(startpos=(48, 10)),
                   gameobjs.Wall(startpos=(49, 10)),
                   gameobjs.Wall(startpos=(50, 10)),
                   gameobjs.Wall(startpos=(51, 10)),
                   gameobjs.Wall(startpos=(52, 10)),
                   gameobjs.Wall(startpos=(54, 10)),
                   gameobjs.Wall(startpos=(55, 10)),
                   gameobjs.Wall(startpos=(56, 10)),
                   gameobjs.Wall(startpos=(57, 10)),
                   gameobjs.Wall(startpos=(58, 10)),
                   gameobjs.Wall(startpos=(59, 10)),
                   gameobjs.Wall(startpos=(59, 12)),
                   gameobjs.Wall(startpos=(59, 13)),
                   gameobjs.Wall(startpos=(59, 14)),
                   gameobjs.Wall(startpos=(59, 15)),
                   gameobjs.Wall(startpos=(58, 15)),
                   gameobjs.Wall(startpos=(57, 15)),
                   gameobjs.Wall(startpos=(57, 14)),
                   gameobjs.Wall(startpos=(57, 13)),
                   gameobjs.Wall(startpos=(57, 12)),
                   gameobjs.Wall(startpos=(56, 12)),
                   gameobjs.Wall(startpos=(55, 12)),
                   gameobjs.Wall(startpos=(54, 12)),
                   gameobjs.Wall(startpos=(53, 12)),
                   gameobjs.Wall(startpos=(52, 12)),
                   gameobjs.Wall(startpos=(51, 12)),
                   gameobjs.Wall(startpos=(50, 12)),
                   gameobjs.Wall(startpos=(49, 12)),
                   gameobjs.Wall(startpos=(48, 12)),
                   gameobjs.Wall(startpos=(47, 12)),
                   gameobjs.Wall(startpos=(46, 12)),
                   gameobjs.Wall(startpos=(44, 11)),
                   gameobjs.Wall(startpos=(44, 12)),
                   gameobjs.Wall(startpos=(44, 13)),
                   gameobjs.Wall(startpos=(44, 14)),
                   gameobjs.Wall(startpos=(46, 14)),
                   gameobjs.Wall(startpos=(47, 14)),
                   gameobjs.Wall(startpos=(48, 14)),
                   gameobjs.Wall(startpos=(49, 14)),
                   gameobjs.Wall(startpos=(50, 14)),
                   gameobjs.Wall(startpos=(53, 14)),
                   gameobjs.Wall(startpos=(54, 14)),
                   gameobjs.Wall(startpos=(51, 14)),
                   gameobjs.Wall(startpos=(52, 14)),
                   gameobjs.Wall(startpos=(55, 14)),
                   gameobjs.Wall(startpos=(57, 16)),
                   gameobjs.Wall(startpos=(56, 16)),
                   gameobjs.Wall(startpos=(55, 16)),
                   gameobjs.Wall(startpos=(54, 16)),
                   gameobjs.Wall(startpos=(53, 16)),
                   gameobjs.Wall(startpos=(52, 16)),
                   gameobjs.Wall(startpos=(46, 15)),
                   gameobjs.Wall(startpos=(46, 16)),
                   gameobjs.Wall(startpos=(45, 16)),
                   gameobjs.Wall(startpos=(44, 16)),
                   gameobjs.Wall(startpos=(43, 16)),
                   gameobjs.Wall(startpos=(42, 16)),
                   gameobjs.Wall(startpos=(41, 16)),
                   gameobjs.Wall(startpos=(40, 16)),
                   gameobjs.Wall(startpos=(39, 16)),
                   gameobjs.Wall(startpos=(38, 16)),
                   gameobjs.Wall(startpos=(38, 14)),
                   gameobjs.Wall(startpos=(39, 14)),
                   gameobjs.Wall(startpos=(40, 14)),
                   gameobjs.Wall(startpos=(41, 14)),
                   gameobjs.Wall(startpos=(42, 14)),
                   gameobjs.Wall(startpos=(43, 14)),
                   gate,
                   gameobjs.Wall(startpos=(61, 7)),
                   gameobjs.PressurePlate(target=gate, startpos=(60, 7), singleimpulse=True),
                   gameobjs.Wall(startpos=(59, 7)),
                   gameobjs.Wall(startpos=(59, 16)),
                   gameobjs.Wall(startpos=(59, 17)),
                   gameobjs.Wall(startpos=(59, 18)),
                   gameobjs.Wall(startpos=(59, 19)),
                   gameobjs.Wall(startpos=(59, 21)),
                   gameobjs.Wall(startpos=(59, 22)),
                   gameobjs.Wall(startpos=(59, 23)),
                   gameobjs.Wall(startpos=(58, 18)),
                   gameobjs.Wall(startpos=(57, 18)),
                   gameobjs.Wall(startpos=(56, 18)),
                   gameobjs.Wall(startpos=(55, 18)),
                   gameobjs.Wall(startpos=(54, 18)),
                   gameobjs.Wall(startpos=(53, 18)),
                   gameobjs.Wall(startpos=(52, 18)),
                   gameobjs.Wall(startpos=(58, 21)),
                   gameobjs.Wall(startpos=(57, 21)),
                   gameobjs.Wall(startpos=(56, 21)),
                   gameobjs.Wall(startpos=(55, 21)),
                   gameobjs.Wall(startpos=(54, 21)),
                   gameobjs.Wall(startpos=(53, 21)),
                   gameobjs.Wall(startpos=(52, 21)),
                   gameobjs.Wall(startpos=(58, 19)),
                   gameobjs.Wall(startpos=(57, 19)),
                   gameobjs.Wall(startpos=(56, 19)),
                   gameobjs.Wall(startpos=(55, 19)),
                   gameobjs.Wall(startpos=(54, 19)),
                   gameobjs.Wall(startpos=(53, 19)),
                   gameobjs.Wall(startpos=(52, 19)),
                   gameobjs.Wall(startpos=(57, 23)),
                   gameobjs.Wall(startpos=(56, 23)),
                   gameobjs.Wall(startpos=(55, 23)),
                   gameobjs.Wall(startpos=(54, 23)),
                   gameobjs.Wall(startpos=(53, 23)),
                   gameobjs.Wall(startpos=(52, 23)),
                   gameobjs.Wall(startpos=(51, 23)),
                   gameobjs.Wall(startpos=(50, 23)),
                   gameobjs.Wall(startpos=(50, 21)),
                   gameobjs.Wall(startpos=(50, 20)),
                   gameobjs.Wall(startpos=(50, 19)),
                   gameobjs.Wall(startpos=(51, 19)),
                   gameobjs.Wall(startpos=(51, 16)),
                   gameobjs.Wall(startpos=(50, 16)),
                   gameobjs.Wall(startpos=(49, 16)),
                   gameobjs.Wall(startpos=(48, 16)),
                   gameobjs.Wall(startpos=(51, 18)),
                   gameobjs.Wall(startpos=(50, 18)),
                   gameobjs.Wall(startpos=(48, 18)),
                   gameobjs.Wall(startpos=(47, 18)),
                   gameobjs.Wall(startpos=(46, 18)),
                   gameobjs.Wall(startpos=(45, 18)),
                   gameobjs.Wall(startpos=(44, 18)),
                   gameobjs.Wall(startpos=(43, 18)),
                   gameobjs.Wall(startpos=(42, 18)),
                   gameobjs.Wall(startpos=(41, 18)),
                   gameobjs.Wall(startpos=(40, 18)),
                   gameobjs.Wall(startpos=(39, 18)),
                   gameobjs.Wall(startpos=(38, 18)),
                   gameobjs.Wall(startpos=(48, 19)),
                   gameobjs.Wall(startpos=(48, 20)),
                   gameobjs.Wall(startpos=(48, 21)),
                   gameobjs.Wall(startpos=(48, 22)),
                   gameobjs.Wall(startpos=(46, 23)),
                   gameobjs.Wall(startpos=(46, 22)),
                   gameobjs.Wall(startpos=(46, 21)),
                   gameobjs.Wall(startpos=(46, 20)),
                   gameobjs.Wall(startpos=(44, 19)),
                   gameobjs.Wall(startpos=(44, 20)),
                   gameobjs.Wall(startpos=(44, 21)),
                   gameobjs.Wall(startpos=(44, 22)),
                   gameobjs.Wall(startpos=(42, 23)),
                   gameobjs.Wall(startpos=(42, 22)),
                   gameobjs.Wall(startpos=(42, 21)),
                   gameobjs.Wall(startpos=(42, 20)),
                   gameobjs.Wall(startpos=(40, 19)),
                   gameobjs.Wall(startpos=(40, 20)),
                   gameobjs.Wall(startpos=(40, 21)),
                   gameobjs.Wall(startpos=(40, 22)),
                   gameobjs.Wall(startpos=(38, 23)),
                   gameobjs.Wall(startpos=(38, 22)),
                   gameobjs.Wall(startpos=(38, 21)),
                   gameobjs.Wall(startpos=(38, 20)),
                   gameobjs.Wall(startpos=(42, 11)),
                   gameobjs.Wall(startpos=(42, 12)),
                   gameobjs.Wall(startpos=(41, 12)),
                   gameobjs.Wall(startpos=(40, 12)),
                   gameobjs.Wall(startpos=(39, 12)),
                   gameobjs.Wall(startpos=(38, 12)),
                   gameobjs.Wall(startpos=(37, 12)),
                   gameobjs.Wall(startpos=(36, 12)),
                   gameobjs.Wall(startpos=(35, 12)),
                   gameobjs.Wall(startpos=(34, 12)),
                   gameobjs.Wall(startpos=(33, 12)),
                   gameobjs.Wall(startpos=(32, 12)),
                   gameobjs.Wall(startpos=(37, 14)),
                   gameobjs.Wall(startpos=(36, 14)),
                   gameobjs.Wall(startpos=(35, 14)),
                   gameobjs.Wall(startpos=(34, 14)),
                   gameobjs.Wall(startpos=(33, 14)),
                   gameobjs.Wall(startpos=(32, 14)),
                   gameobjs.Wall(startpos=(37, 16)),
                   gameobjs.Wall(startpos=(34, 16)),
                   gameobjs.Wall(startpos=(33, 16)),
                   gameobjs.Wall(startpos=(32, 16)),
                   gameobjs.Wall(startpos=(37, 18)),
                   gameobjs.Wall(startpos=(36, 16)),
                   gameobjs.Wall(startpos=(36, 18)),
                   gameobjs.Wall(startpos=(36, 17)),
                   gameobjs.Wall(startpos=(34, 17)),
                   gameobjs.Wall(startpos=(35, 24)),
                   gameobjs.Wall(startpos=(34, 18)),
                   gameobjs.Wall(startpos=(34, 19)),
                   gameobjs.Wall(startpos=(34, 20)),
                   gameobjs.Wall(startpos=(36, 19)),
                   gameobjs.Wall(startpos=(36, 20)),
                   gameobjs.Wall(startpos=(36, 21)),
                   gameobjs.Wall(startpos=(36, 22)),
                   gameobjs.Wall(startpos=(34, 21)),
                   gameobjs.Wall(startpos=(34, 22)),
                   gameobjs.Wall(startpos=(34, 23)),
                   gameobjs.Wall(startpos=(47, 16)),
                   gameobjs.Wall(startpos=(31, 12)),
                   gameobjs.Wall(startpos=(31, 14)),
                   gameobjs.Wall(startpos=(30, 14)),
                   gameobjs.Wall(startpos=(30, 15)),
                   gameobjs.Wall(startpos=(30, 16)),
                   gameobjs.Wall(startpos=(30, 17)),
                   gameobjs.Wall(startpos=(30, 18)),
                   gameobjs.Wall(startpos=(31, 18)),
                   gameobjs.Wall(startpos=(31, 19)),
                   gameobjs.Wall(startpos=(31, 20)),
                   gameobjs.Wall(startpos=(31, 21)),
                   gameobjs.Wall(startpos=(31, 22)),
                   gameobjs.Wall(startpos=(29, 22)),
                   gameobjs.Wall(startpos=(29, 21)),
                   gameobjs.Wall(startpos=(29, 20)),
                   gameobjs.Wall(startpos=(29, 18)),
                   gameobjs.Wall(startpos=(28, 18)),
                   gameobjs.Wall(startpos=(28, 20)),
                   gameobjs.Wall(startpos=(27, 20)),
                   gameobjs.Wall(startpos=(26, 19)),
                   gameobjs.Wall(startpos=(26, 20)),
                   gameobjs.Wall(startpos=(26, 17)),
                   gameobjs.Wall(startpos=(26, 18)),
                   gameobjs.Wall(startpos=(26, 16)),
                   gameobjs.Wall(startpos=(27, 16)),
                   gameobjs.Wall(startpos=(29, 16)),
                   gameobjs.Wall(startpos=(29, 17)),
                   gameobjs.Wall(startpos=(29, 14)),
                   gameobjs.Wall(startpos=(30, 12)),
                   gameobjs.Wall(startpos=(29, 12)),
                   gameobjs.Wall(startpos=(28, 12)),
                   gameobjs.Wall(startpos=(27, 12)),
                   gameobjs.Wall(startpos=(26, 12)),
                   gameobjs.Wall(startpos=(25, 12)),
                   gameobjs.Wall(startpos=(24, 12)),
                   gameobjs.Wall(startpos=(23, 12)),
                   gameobjs.Wall(startpos=(22, 12)),
                   gameobjs.Wall(startpos=(21, 12)),
                   gameobjs.Wall(startpos=(20, 12)),
                   gameobjs.Wall(startpos=(19, 12)),
                   gameobjs.Wall(startpos=(18, 12)),
                   gameobjs.Wall(startpos=(16, 12)),
                   gameobjs.Wall(startpos=(17, 12)),
                   gameobjs.Wall(startpos=(31, 10)),
                   gameobjs.Wall(startpos=(22, 10)),
                   gameobjs.Wall(startpos=(16, 11)),
                   gameobjs.Wall(startpos=(28, 14)),
                   gameobjs.Wall(startpos=(27, 14)),
                   gameobjs.Wall(startpos=(26, 14)),
                   gameobjs.Wall(startpos=(25, 14)),
                   gameobjs.Wall(startpos=(24, 14)),
                   gameobjs.Wall(startpos=(23, 14)),
                   gameobjs.Wall(startpos=(22, 14)),
                   gameobjs.Wall(startpos=(21, 14)),
                   gameobjs.Wall(startpos=(20, 14)),
                   gameobjs.Wall(startpos=(19, 14)),
                   gameobjs.Wall(startpos=(18, 14)),
                   gameobjs.Wall(startpos=(29, 15)),
                   gameobjs.Wall(startpos=(25, 16)),
                   gameobjs.Wall(startpos=(24, 16)),
                   gameobjs.Wall(startpos=(23, 16)),
                   gameobjs.Wall(startpos=(22, 16)),
                   gameobjs.Wall(startpos=(21, 16)),
                   gameobjs.Wall(startpos=(20, 16)),
                   gameobjs.Wall(startpos=(19, 16)),
                   gameobjs.Wall(startpos=(18, 16)),
                   gameobjs.Wall(startpos=(17, 16)),
                   gameobjs.Wall(startpos=(16, 18)),
                   gameobjs.Wall(startpos=(17, 18)),
                   gameobjs.Wall(startpos=(18, 18)),
                   gameobjs.Wall(startpos=(19, 18)),
                   gameobjs.Wall(startpos=(25, 18)),
                   gameobjs.Wall(startpos=(24, 18)),
                   gameobjs.Wall(startpos=(23, 18)),
                   gameobjs.Wall(startpos=(22, 18)),
                   gameobjs.Wall(startpos=(20, 18)),
                   gameobjs.Wall(startpos=(17, 22)),
                   gameobjs.Wall(startpos=(18, 22)),
                   gameobjs.Wall(startpos=(18, 21)),
                   gameobjs.Wall(startpos=(18, 20)),
                   gameobjs.Wall(startpos=(20, 22)),
                   gameobjs.Wall(startpos=(21, 22)),
                   gameobjs.Wall(startpos=(22, 22)),
                   gameobjs.Wall(startpos=(23, 22)),
                   gameobjs.Wall(startpos=(24, 22)),
                   gameobjs.Wall(startpos=(25, 22)),
                   gameobjs.Wall(startpos=(26, 22)),
                   gameobjs.Wall(startpos=(27, 22)),
                   gameobjs.Wall(startpos=(24, 19)),
                   gameobjs.Wall(startpos=(24, 20)),
                   gameobjs.Wall(startpos=(59, 11)),
                   gameobjs.Wall(startpos=(54, 9)),
                   gameobjs.Wall(startpos=(31, 16)),
                   gameobjs.Wall(startpos=(32, 22)),
                   gameobjs.Wall(startpos=(32, 21)),
                   gameobjs.Wall(startpos=(32, 20)),
                   gameobjs.Wall(startpos=(32, 19)),
                   gameobjs.Wall(startpos=(32, 18)),
                   gameobjs.Wall(startpos=(24, 21)),
                   gameobjs.Wall(startpos=(20, 19)),
                   gameobjs.Wall(startpos=(20, 20)),
                   gameobjs.Wall(startpos=(21, 20)),
                   gameobjs.Wall(startpos=(22, 20)),
                   gameobjs.Wall(startpos=(16, 23)),
                   gameobjs.Wall(startpos=(33, 10)),
                   gameobjs.Wall(startpos=(37, 11)),
                   gameobjs.Wall(startpos=(39, 10)),
                   gameobjs.Wall(startpos=(39, 9)),
                   gameobjs.Wall(startpos=(14, 7)),
                   gameobjs.Wall(startpos=(13, 22)),
                   gameobjs.Wall(startpos=(13, 24)),
                   gameobjs.Wall(startpos=(12, 24)),
                   gameobjs.Wall(startpos=(12, 22)),
                   gameobjs.Wall(startpos=(11, 24)),
                   gameobjs.Wall(startpos=(11, 23)),
                   gameobjs.Wall(startpos=(11, 22)),
                   drone,
                   gameobjs.Levelporter(targetlevel='Telemaze', startpos=(60, 5))]
level13.extralocals={"upload": drone.upload}
level13.hints=[
            "Look up maze solving algorithms.",
            "Yes, really. Open up Wikipedia and read. Go. I suggest Trémaux's algorithm, myself.",
            "Or you could just guide the drone manually by popping vectors off a stack but that's boring.",
            "Or just store all visited and blocked positions in a set and prioritize moving to cells that aren't in that set.",
            "...",
            "*sigh*\nThe next hint is the complete solution.",
            """
import collections
visits=collections.defaultdict(int)
blocked=set()

def vadd(a,b):
 return a[0]+b[0],a[1]+b[1]

def mythink(self):
    visits[self.pos]+=1
    try:
        blocked.remove(self.pos)
    except KeyError:
        pass
    dirs=[(x,y) for x in (-1,0,1) for y in (-1,0,1) if not (x==0 and y==0)]
    newposes={thedir:vadd(self.pos,thedir)
                    for thedir in dirs if vadd(self.pos,thedir) not in blocked}
    newdir=min(newposes,key=lambda x: visits[newposes[x]])
    self.move(*newdir)
    self.lastmove=newdir
    blocked.add(newposes[newdir])
upload(mythink)
            """]

class Level14(Level):
    def l14_get_at(self, pos):
        """Given a 2-tuple of ints representing a position, return the gameobj (other than walls and the player)
        at the given position in the level. Passes back a WallProxy in stead of walls."""
        theobj = self.get_at(pos)
        if isinstance(theobj, gameobjs.Wall):
            return gameobjs.WallProxy(theobj)
        return theobj

level14=Level14("LEVEL 14: DIJKSTRA'S REVENGE",[],gameobjs.Player((5, 5)),playlists["Calm"])
gate1=gameobjs.Gate(startpos=(64, 16))
gate2=gameobjs.Gate(startpos=(66, 13))
levelporter=gameobjs.Levelporter(startpos=(50, 8), targetlevel='Arbitrary')
level14.objlist=[]
level14.safe_objlist=[gameobjs.Wall(startpos=(4, 5)),
                   gameobjs.Wall(startpos=(4, 4)),
                   gameobjs.Wall(startpos=(5, 4)),
                   gameobjs.Wall(startpos=(6, 4)),
                   gameobjs.Wall(startpos=(6, 6)),
                   gameobjs.Wall(startpos=(5, 6)),
                   gameobjs.Wall(startpos=(4, 6)),
                   gameobjs.Wall(startpos=(7, 4)),
                   gameobjs.Wall(startpos=(8, 4)),
                   gameobjs.Wall(startpos=(9, 4)),
                   gameobjs.Wall(startpos=(7, 6)),
                   gameobjs.Wall(startpos=(8, 6)),
                   gameobjs.Wall(startpos=(9, 6)),
                   gameobjs.Wall(startpos=(9, 3)),
                   gameobjs.Wall(startpos=(9, 7)),
                   gameobjs.Wall(startpos=(10, 7)),
                   gameobjs.Wall(startpos=(11, 7)),
                   gameobjs.Wall(startpos=(11, 6)),
                   gameobjs.Wall(startpos=(11, 5)),
                   gameobjs.Wall(startpos=(11, 4)),
                   gameobjs.Wall(startpos=(11, 3)),
                   gameobjs.Wall(startpos=(10, 3)),
                   gameobjs.Wall(startpos=(29, 3)),
                   gameobjs.Wall(startpos=(29, 4)),
                   gameobjs.Wall(startpos=(27, 4)),
                   gameobjs.Wall(startpos=(27, 5)),
                   gameobjs.Wall(startpos=(29, 5)),
                   gameobjs.Wall(startpos=(29, 6)),
                   gameobjs.Wall(startpos=(28, 6)),
                   gameobjs.Wall(startpos=(27, 6)),
                   gameobjs.Wall(startpos=(27, 3)),
                   gameobjs.Wall(startpos=(27, 2)),
                   gameobjs.Wall(startpos=(28, 2)),
                   gameobjs.Wall(startpos=(29, 2)),
                   gameobjs.Wall(startpos=(69, 4)),
                   gameobjs.Wall(startpos=(67, 4)),
                   gameobjs.Wall(startpos=(68, 4)),
                   gameobjs.Wall(startpos=(66, 4)),
                   gameobjs.Wall(startpos=(65, 4)),
                   gameobjs.Wall(startpos=(64, 4)),
                   gameobjs.Wall(startpos=(63, 4)),
                   gameobjs.Wall(startpos=(62, 4)),
                   gameobjs.Wall(startpos=(62, 5)),
                   gameobjs.Wall(startpos=(62, 6)),
                   gameobjs.Wall(startpos=(63, 6)),
                   gameobjs.Wall(startpos=(64, 6)),
                   gameobjs.Wall(startpos=(65, 6)),
                   gameobjs.Wall(startpos=(66, 6)),
                   gameobjs.Wall(startpos=(68, 6)),
                   gameobjs.Wall(startpos=(67, 6)),
                   gameobjs.Wall(startpos=(70, 4)),
                   gameobjs.Wall(startpos=(71, 4)),
                   gameobjs.Wall(startpos=(71, 5)),
                   gameobjs.Wall(startpos=(71, 6)),
                   gameobjs.Wall(startpos=(71, 7)),
                   gameobjs.Wall(startpos=(71, 8)),
                   gameobjs.Wall(startpos=(70, 8)),
                   gameobjs.Wall(startpos=(69, 8)),
                   gameobjs.Wall(startpos=(68, 8)),
                   gameobjs.Wall(startpos=(67, 8)),
                   gameobjs.Wall(startpos=(66, 8)),
                   gameobjs.Wall(startpos=(65, 8)),
                   gameobjs.Wall(startpos=(64, 8)),
                   gameobjs.Wall(startpos=(63, 8)),
                   gameobjs.Wall(startpos=(62, 8)),
                   gameobjs.Wall(startpos=(62, 7)),
                   gameobjs.Wall(startpos=(51, 6)),
                   gameobjs.Wall(startpos=(49, 6)),
                   gameobjs.Wall(startpos=(50, 5)),
                   gameobjs.Wall(startpos=(48, 7)),
                   gameobjs.Wall(startpos=(52, 7)),
                   gameobjs.Wall(startpos=(47, 8)),
                   gameobjs.Wall(startpos=(53, 8)),
                   gameobjs.Wall(startpos=(53, 9)),
                   gameobjs.Wall(startpos=(52, 9)),
                   gameobjs.Wall(startpos=(51, 9)),
                   gameobjs.Wall(startpos=(50, 9)),
                   gameobjs.Wall(startpos=(49, 9)),
                   gameobjs.Wall(startpos=(48, 9)),
                   gameobjs.Wall(startpos=(47, 9)),
                   gameobjs.Wall(startpos=(47, 7)),
                   gameobjs.Wall(startpos=(48, 6)),
                   gameobjs.Wall(startpos=(49, 5)),
                   gameobjs.Wall(startpos=(50, 4)),
                   gameobjs.Wall(startpos=(51, 5)),
                   gameobjs.Wall(startpos=(52, 6)),
                   gameobjs.Wall(startpos=(53, 7)),
                   levelporter,
                   gameobjs.Teleporter(startpos=(63, 5), dest=(50, 6)),
                   gameobjs.Teleporter(startpos=(63, 7), dest=(5, 5)),
                   gameobjs.Teleporter(startpos=(28, 5), dest=(13, 11)),
                   gameobjs.Wall(startpos=(25, 13)),
                   gameobjs.Wall(startpos=(24, 13)),
                   gameobjs.Wall(startpos=(23, 13)),
                   gameobjs.Wall(startpos=(22, 13)),
                   gameobjs.Wall(startpos=(21, 13)),
                   gameobjs.Wall(startpos=(20, 13)),
                   gameobjs.Wall(startpos=(20, 14)),
                   gameobjs.Wall(startpos=(20, 15)),
                   gameobjs.Wall(startpos=(20, 16)),
                   gameobjs.Wall(startpos=(20, 17)),
                   gameobjs.Wall(startpos=(21, 17)),
                   gameobjs.Wall(startpos=(22, 17)),
                   gameobjs.Wall(startpos=(23, 17)),
                   gameobjs.Wall(startpos=(24, 17)),
                   gameobjs.Wall(startpos=(25, 17)),
                   gameobjs.Wall(startpos=(26, 17)),
                   gameobjs.Wall(startpos=(26, 16)),
                   gameobjs.Wall(startpos=(26, 15)),
                   gameobjs.Wall(startpos=(26, 14)),
                   gameobjs.Wall(startpos=(26, 13)),
                   gameobjs.Wall(startpos=(40, 22)),
                   gameobjs.Wall(startpos=(39, 21)),
                   gameobjs.Wall(startpos=(39, 20)),
                   gameobjs.Wall(startpos=(40, 19)),
                   gameobjs.Wall(startpos=(41, 19)),
                   gameobjs.Wall(startpos=(42, 19)),
                   gameobjs.Wall(startpos=(43, 20)),
                   gameobjs.Wall(startpos=(43, 21)),
                   gameobjs.Wall(startpos=(42, 22)),
                   gameobjs.Wall(startpos=(41, 22)),
                   gameobjs.Wall(startpos=(60, 14)),
                   gameobjs.Wall(startpos=(60, 15)),
                   gameobjs.Wall(startpos=(60, 16)),
                   gameobjs.Wall(startpos=(60, 17)),
                   gameobjs.Wall(startpos=(61, 17)),
                   gameobjs.Wall(startpos=(62, 17)),
                   gameobjs.Wall(startpos=(63, 17)),
                   gameobjs.Wall(startpos=(60, 13)),
                   gameobjs.Wall(startpos=(61, 13)),
                   gameobjs.Wall(startpos=(62, 13)),
                   gameobjs.Wall(startpos=(63, 13)),
                   gameobjs.Wall(startpos=(64, 15)),
                   gameobjs.Wall(startpos=(64, 17)),
                   gameobjs.Wall(startpos=(64, 13)),
                   gameobjs.Wall(startpos=(65, 15)),
                   gameobjs.Wall(startpos=(66, 14)),
                   gameobjs.Wall(startpos=(66, 15)),
                   gameobjs.Wall(startpos=(66, 16)),
                   gameobjs.Wall(startpos=(66, 17)),
                   gameobjs.Wall(startpos=(66, 18)),
                   gameobjs.Wall(startpos=(66, 19)),
                   gameobjs.Wall(startpos=(65, 19)),
                   gameobjs.Wall(startpos=(64, 19)),
                   gameobjs.Wall(startpos=(63, 18)),
                   gameobjs.Wall(startpos=(63, 19)),
                   gameobjs.Wall(startpos=(64, 12)),
                   gameobjs.Wall(startpos=(65, 12)),
                   gameobjs.Wall(startpos=(66, 12)),
                   gameobjs.Wall(startpos=(67, 12)),
                   gameobjs.Wall(startpos=(68, 12)),
                   gameobjs.Wall(startpos=(68, 13)),
                   gameobjs.Wall(startpos=(68, 14)),
                   gameobjs.Wall(startpos=(68, 15)),
                   gameobjs.Wall(startpos=(69, 15)),
                   gameobjs.Wall(startpos=(67, 17)),
                   gameobjs.Wall(startpos=(68, 17)),
                   gameobjs.Wall(startpos=(69, 17)),
                   gameobjs.Wall(startpos=(70, 16)),
                   gameobjs.Wall(startpos=(70, 15)),
                   gameobjs.Wall(startpos=(70, 17)),
                   gate1,
                   gate2,
                   gameobjs.PressurePlate(startpos=(58, 14), target=gate1, singleimpulse=True),
                   gameobjs.PressurePlate(startpos=(58, 16), target=gate2, singleimpulse=True),
                   gameobjs.Boulder(startpos=(58, 12)),
                   gameobjs.Teleporter(startpos=(58, 18), dest=(58, 12)),
                   gameobjs.Wall(startpos=(7, 18)),
                   gameobjs.Wall(startpos=(7, 19)),
                   gameobjs.Wall(startpos=(6, 19)),
                   gameobjs.Wall(startpos=(9, 18)),
                   gameobjs.Wall(startpos=(9, 19)),
                   gameobjs.Wall(startpos=(10, 19)),
                   gameobjs.Wall(startpos=(9, 21)),
                   gameobjs.Wall(startpos=(10, 21)),
                   gameobjs.Wall(startpos=(7, 21)),
                   gameobjs.Wall(startpos=(6, 21)),
                   gameobjs.Wall(startpos=(7, 22)),
                   gameobjs.Wall(startpos=(9, 22)),
                   gameobjs.Wall(startpos=(5, 21)),
                   gameobjs.Wall(startpos=(5, 20)),
                   gameobjs.Wall(startpos=(5, 19)),
                   gameobjs.Wall(startpos=(7, 17)),
                   gameobjs.Wall(startpos=(8, 17)),
                   gameobjs.Wall(startpos=(9, 17)),
                   gameobjs.Wall(startpos=(11, 21)),
                   gameobjs.Wall(startpos=(11, 20)),
                   gameobjs.Wall(startpos=(11, 19)),
                   gameobjs.Wall(startpos=(7, 23)),
                   gameobjs.Wall(startpos=(9, 23)),
                   gameobjs.Wall(startpos=(8, 23)),
                   gameobjs.Wall(startpos=(22, 22)),
                   gameobjs.Wall(startpos=(22, 24)),
                   gameobjs.Wall(startpos=(23, 23)),
                   gameobjs.Wall(startpos=(24, 22)),
                   gameobjs.Wall(startpos=(24, 21)),
                   gameobjs.Wall(startpos=(24, 20)),
                   gameobjs.Wall(startpos=(23, 20)),
                   gameobjs.Wall(startpos=(22, 20)),
                   gameobjs.Wall(startpos=(21, 20)),
                   gameobjs.Wall(startpos=(20, 20)),
                   gameobjs.Wall(startpos=(19, 20)),
                   gameobjs.Wall(startpos=(18, 20)),
                   gameobjs.Wall(startpos=(18, 21)),
                   gameobjs.Wall(startpos=(18, 22)),
                   gameobjs.Wall(startpos=(18, 23)),
                   gameobjs.Wall(startpos=(18, 24)),
                   gameobjs.Wall(startpos=(18, 25)),
                   gameobjs.Wall(startpos=(20, 26)),
                   gameobjs.Wall(startpos=(21, 26)),
                   gameobjs.Wall(startpos=(22, 26)),
                   gameobjs.Wall(startpos=(23, 26)),
                   gameobjs.Wall(startpos=(24, 26)),
                   gameobjs.Wall(startpos=(25, 26)),
                   gameobjs.Wall(startpos=(21, 23)),
                   gameobjs.Wall(startpos=(24, 23)),
                   gameobjs.Wall(startpos=(19, 26)),
                   gameobjs.Wall(startpos=(18, 26)),
                   gameobjs.Wall(startpos=(19, 25)),
                   gameobjs.Wall(startpos=(19, 21)),
                   gameobjs.Wall(startpos=(26, 26)),
                   gameobjs.Wall(startpos=(27, 26)),
                   gameobjs.Wall(startpos=(28, 26)),
                   gameobjs.Wall(startpos=(29, 26)),
                   gameobjs.Wall(startpos=(29, 25)),
                   gameobjs.Wall(startpos=(29, 24)),
                   gameobjs.Wall(startpos=(28, 24)),
                   gameobjs.Wall(startpos=(27, 24)),
                   gameobjs.Wall(startpos=(26, 24)),
                   gameobjs.Wall(startpos=(25, 24)),
                   gameobjs.Wall(startpos=(24, 24)),
                   gameobjs.Wall(startpos=(22, 23)),
                   gameobjs.Wall(startpos=(35, 12)),
                   gameobjs.Wall(startpos=(36, 11)),
                   gameobjs.Wall(startpos=(37, 10)),
                   gameobjs.Wall(startpos=(38, 10)),
                   gameobjs.Wall(startpos=(39, 10)),
                   gameobjs.Wall(startpos=(40, 10)),
                   gameobjs.Wall(startpos=(41, 10)),
                   gameobjs.Wall(startpos=(42, 11)),
                   gameobjs.Wall(startpos=(43, 12)),
                   gameobjs.Wall(startpos=(44, 13)),
                   gameobjs.Wall(startpos=(43, 14)),
                   gameobjs.Wall(startpos=(42, 15)),
                   gameobjs.Wall(startpos=(41, 16)),
                   gameobjs.Wall(startpos=(40, 16)),
                   gameobjs.Wall(startpos=(39, 16)),
                   gameobjs.Wall(startpos=(38, 16)),
                   gameobjs.Wall(startpos=(36, 15)),
                   gameobjs.Wall(startpos=(37, 16)),
                   gameobjs.Wall(startpos=(34, 13)),
                   gameobjs.Wall(startpos=(35, 14)),
                   gameobjs.Wall(startpos=(56, 26)),
                   gameobjs.Wall(startpos=(57, 26)),
                   gameobjs.Wall(startpos=(58, 26)),
                   gameobjs.Wall(startpos=(59, 26)),
                   gameobjs.Wall(startpos=(60, 26)),
                   gameobjs.Wall(startpos=(58, 23)),
                   gameobjs.Wall(startpos=(59, 23)),
                   gameobjs.Wall(startpos=(60, 23)),
                   gameobjs.Wall(startpos=(61, 23)),
                   gameobjs.Wall(startpos=(62, 23)),
                   gameobjs.Wall(startpos=(63, 23)),
                   gameobjs.Wall(startpos=(63, 24)),
                   gameobjs.Wall(startpos=(62, 25)),
                   gameobjs.Wall(startpos=(61, 26)),
                   gameobjs.Wall(startpos=(55, 26)),
                   gameobjs.Wall(startpos=(55, 25)),
                   gameobjs.Wall(startpos=(56, 24)),
                   gameobjs.Wall(startpos=(57, 23)),
                   gameobjs.Wall(startpos=(0, 29)),
                   gameobjs.Wall(startpos=(0, 28)),
                   gameobjs.Wall(startpos=(0, 27)),
                   gameobjs.Wall(startpos=(0, 26)),
                   gameobjs.Wall(startpos=(1, 26)),
                   gameobjs.Wall(startpos=(2, 26)),
                   gameobjs.Wall(startpos=(3, 26)),
                   gameobjs.Wall(startpos=(4, 26)),
                   gameobjs.Wall(startpos=(6, 26)),
                   gameobjs.Wall(startpos=(6, 28)),
                   gameobjs.Wall(startpos=(5, 28)),
                   gameobjs.Wall(startpos=(4, 28)),
                   gameobjs.Wall(startpos=(1, 29)),
                   gameobjs.Wall(startpos=(2, 29)),
                   gameobjs.Wall(startpos=(3, 29)),
                   gameobjs.Wall(startpos=(4, 29)),
                   gameobjs.Wall(startpos=(5, 29)),
                   gameobjs.Wall(startpos=(6, 29)),
                   gameobjs.Wall(startpos=(7, 29)),
                   gameobjs.Wall(startpos=(8, 29)),
                   gameobjs.Wall(startpos=(7, 26)),
                   gameobjs.Wall(startpos=(8, 26)),
                   gameobjs.Wall(startpos=(9, 26)),
                   gameobjs.Wall(startpos=(9, 29)),
                   gameobjs.Wall(startpos=(10, 29)),
                   gameobjs.Wall(startpos=(10, 28)),
                   gameobjs.Wall(startpos=(10, 27)),
                   gameobjs.Wall(startpos=(10, 26)),
                   gameobjs.Wall(startpos=(4, 25)),
                   gameobjs.Wall(startpos=(5, 25)),
                   gameobjs.Wall(startpos=(6, 25)),
                   gameobjs.Wall(startpos=(14, 10)),
                   gameobjs.Wall(startpos=(14, 11)),
                   gameobjs.Wall(startpos=(14, 12)),
                   gameobjs.Wall(startpos=(14, 13)),
                   gameobjs.Wall(startpos=(14, 14)),
                   gameobjs.Wall(startpos=(13, 14)),
                   gameobjs.Wall(startpos=(12, 14)),
                   gameobjs.Wall(startpos=(11, 14)),
                   gameobjs.Wall(startpos=(10, 14)),
                   gameobjs.Wall(startpos=(11, 12)),
                   gameobjs.Wall(startpos=(10, 12)),
                   gameobjs.Wall(startpos=(13, 12)),
                   gameobjs.Wall(startpos=(8, 12)),
                   gameobjs.Wall(startpos=(9, 14)),
                   gameobjs.Wall(startpos=(8, 14)),
                   gameobjs.Wall(startpos=(7, 14)),
                   gameobjs.Wall(startpos=(7, 13)),
                   gameobjs.Wall(startpos=(7, 12)),
                   gameobjs.Wall(startpos=(7, 11)),
                   gameobjs.Wall(startpos=(7, 10)),
                   gameobjs.Wall(startpos=(8, 10)),
                   gameobjs.Wall(startpos=(9, 10)),
                   gameobjs.Wall(startpos=(10, 10)),
                   gameobjs.Wall(startpos=(11, 10)),
                   gameobjs.Wall(startpos=(12, 10)),
                   gameobjs.Wall(startpos=(13, 10)),
                   gameobjs.Teleporter(startpos=(69, 16), dest=(70, 6)),
                   gameobjs.Teleporter(startpos=(64, 18), dest=(57, 24)),
                   gameobjs.Teleporter(startpos=(61, 25), dest=(61, 15)),
                   gameobjs.Teleporter(startpos=(10, 20), dest=(40, 20)),
                   gameobjs.Teleporter(startpos=(56, 25), dest=(39, 11)),
                   gameobjs.Teleporter(startpos=(28, 25), dest=(70, 6)),
                   gameobjs.Teleporter(startpos=(21, 16), dest=(20, 25)),
                   gameobjs.Teleporter(startpos=(25, 16), dest=(39, 11)),
                   gameobjs.Teleporter(startpos=(13, 13), dest=(23, 14)),
                   gameobjs.Teleporter(startpos=(9, 28), dest=(40, 20)),
                   gameobjs.Teleporter(startpos=(42, 21), dest=(5, 5)),
                   gameobjs.Teleporter(startpos=(6, 20), dest=(5, 26)),
                   gameobjs.Teleporter(startpos=(1, 28), dest=(28, 3)),
                   gameobjs.Teleporter(startpos=(8, 22), dest=(5, 5)),
                   gameobjs.Teleporter(startpos=(8, 18), dest=(5, 5)),
                   gameobjs.Teleporter(startpos=(10, 6), dest=(8, 20)),
                   gameobjs.Teleporter(startpos=(41, 13), dest=(5, 5)),
                   gameobjs.Teleporter(startpos=(40, 14), dest=(23, 14)),
                   gameobjs.Teleporter(startpos=(39, 14), dest=(40, 20)),
                   gameobjs.Teleporter(startpos=(38, 14), dest=(21, 24)),
                   gameobjs.Teleporter(startpos=(37, 13), dest=(8, 11)),
                   gameobjs.Wall(startpos=(40, 12)),
                   gameobjs.Wall(startpos=(38, 12)),
                   gameobjs.Wall(startpos=(37, 14)),
                   gameobjs.Wall(startpos=(41, 14)),
                   gameobjs.Teleporter(startpos=(8, 13), dest=(61, 14)),
                   gameobjs.AttributeEnforcer((50,10),{levelporter:("pos",)})]

level14.extralocals={"get_at":level14.l14_get_at}

level14.hints=["Honestly, you COULD just solve the maze by running around through all the teleporters. But that's no fun.",
               "This is another one of those levels where there's something new in scope you can pick out with dir().",
               "Specifically, the get_at() function, which takes a position and returns either a game object, if there was one at that position, or None.",
               "So actually you could get every (non wall) object on the level just by scanning every position from (0,0) to (80,30).",
               "And then build a graph of teleporters by flood-filling rooms (i.e. scanning outwards 'till you see a wall).",
               "And then find a path from the northwest-most one to the one that's in the same room as the levelporter.",
               "I'll even give you the flood-fill function (it was a bit of a pain to write, actually). "
               "You can put it in your console scope with exec(hint.hintlist[7]). It's in the next hint.",
               """
def floodfill(x,y,results=None,visited=None,curdepth=0):
 if results is None:
  results=list()
 if visited is None:
  visited=set()

 if curdepth > 20:
  return

 if (x,y) in visited:
  return
 visited.add((x,y))

 theobj=get_at((x,y))
 if "WallProxy" in str(type(theobj)):
  return

 if theobj:
  results.append(theobj)

 for xofst in (-1,0,1):
  for yofst in (-1,0,1):
   if xofst==yofst==0:
    continue
   floodfill(x+xofst,y+yofst,results,visited,curdepth+1)

 if curdepth==0:
  return results""",
               "Once you have that, just write a function that maps a teleporter object to the objects in the room with "
               "it, then write a Djikstra's algorithm or DFS implementation that changes the cheapest path's teleporters .graphic.",
               "...",
               "Or you could skip all that and make all the teleporters in the level take you directly to the levelporter. Either or."]

#This is my solution. This is the first level that took me longer to solve than to make!
"""def makemap(startpos,map=None):
 if not map:
  map=dict()
 objs=floodfill(*startpos)
 map[startpos]=objs
 teles=[theobj for theobj in objs if "Teleporter" in str(type(theobj))]
 for thetele in teles:
  dest=thetele.dest
  if dest not in map:
   makemap(dest,map)
 return map
def markpath(origin,mapin,visited=None):
 if not visited:
  visited=set()

 if origin in visited:
  return False
 visited.add(origin)

 objs=map[origin.dest]
 for theobj in objs:
  if "Levelporter" in str(type(theobj)):
   if origin:
    origin.graphic='!'
   return True

 dests=[(theobj,theobj.dest) for theobj in objs if hasattr(theobj,"dest")]
 for theorigin, thedest in dests:
  if markpath(theorigin,map,visited):
   if origin:
    origin.graphic='!'
   return True"""

level15=Level("LEVEL 15: THE EXECUTOR",[],gameobjs.Player((33, 21)),playlists["Calm"])
launcher=gameobjs.MissileLauncher(startpos=(41, 5), target=level15.plyobj.proxy, rate=18)
level15.objlist.extend([gameobjs.ArbitraryCodeExecutor(startpos=(49, 21), code='pass', execlocals={"objs":level15.objlist})])
launcherprotector="""
for theobj in objs:
    if not "Missile" in str(type(theobj)):
        continue
    if not hasattr(theobj.target,"pos"):
        continue
    if distance(theobj.pos,launcher.pos) < 4 or distance(theobj.pos,self.pos) == 1:
        x,y=theobj.pos
        tx,ty=theobj.target.pos
        dx=tx-x
        dy=ty-y
        if dx != 0:
            dx=dx//abs(dx)
        if dy != 0:
            dy=dy//abs(dy)
        if (x+dx,y+dy)==launcher.pos or (x+(dx*2),y+(dy*2))==launcher.pos or (x+(dx*3),y+(dy*3))==launcher.pos or (x+(dx*4),y+(dy*4))==launcher.pos:
            theobj.target=None
        if (x+dx,y+dy)==self.pos:
            self.pos=(x-dx*2,y-dy*2)
"""
level15.safe_objlist=[gameobjs.Wall(startpos=(50, 19)),
                   gameobjs.Wall(startpos=(49, 19)),
                   gameobjs.Wall(startpos=(48, 19)),
                   gameobjs.Wall(startpos=(47, 19)),
                   gameobjs.Wall(startpos=(46, 19)),
                   gameobjs.Wall(startpos=(45, 19)),
                   gameobjs.Wall(startpos=(44, 19)),
                   gameobjs.Wall(startpos=(38, 19)),
                   gameobjs.Wall(startpos=(37, 19)),
                   gameobjs.Wall(startpos=(36, 19)),
                   gameobjs.Wall(startpos=(35, 19)),
                   gameobjs.Wall(startpos=(34, 19)),
                   gameobjs.Wall(startpos=(33, 19)),
                   gameobjs.Wall(startpos=(32, 19)),
                   gameobjs.Wall(startpos=(32, 22)),
                   gameobjs.Wall(startpos=(33, 22)),
                   gameobjs.Wall(startpos=(34, 22)),
                   gameobjs.Wall(startpos=(35, 22)),
                   gameobjs.Wall(startpos=(36, 22)),
                   gameobjs.Wall(startpos=(37, 22)),
                   gameobjs.Wall(startpos=(38, 22)),
                   gameobjs.Wall(startpos=(39, 22)),
                   gameobjs.Wall(startpos=(40, 22)),
                   gameobjs.Wall(startpos=(41, 22)),
                   gameobjs.Wall(startpos=(42, 22)),
                   gameobjs.Wall(startpos=(43, 22)),
                   gameobjs.Wall(startpos=(44, 22)),
                   gameobjs.Wall(startpos=(45, 22)),
                   gameobjs.Wall(startpos=(46, 22)),
                   gameobjs.Wall(startpos=(47, 22)),
                   gameobjs.Wall(startpos=(48, 22)),
                   gameobjs.Wall(startpos=(49, 22)),
                   gameobjs.Wall(startpos=(50, 22)),
                   gameobjs.Wall(startpos=(50, 21)),
                   gameobjs.Wall(startpos=(50, 20)),
                   gameobjs.Wall(startpos=(44, 20)),
                   gameobjs.Wall(startpos=(38, 20)),
                   gameobjs.Wall(startpos=(32, 20)),
                   gameobjs.Wall(startpos=(32, 21)),
                   gameobjs.Wall(startpos=(38, 18)),
                   gameobjs.Wall(startpos=(38, 17)),
                   gameobjs.Wall(startpos=(38, 16)),
                   gameobjs.Wall(startpos=(38, 15)),
                   gameobjs.Wall(startpos=(38, 14)),
                   gameobjs.Wall(startpos=(38, 13)),
                   gameobjs.Wall(startpos=(38, 12)),
                   gameobjs.Wall(startpos=(38, 11)),
                   gameobjs.Wall(startpos=(38, 10)),
                   gameobjs.Wall(startpos=(38, 9)),
                   gameobjs.Wall(startpos=(38, 8)),
                   gameobjs.Wall(startpos=(38, 7)),
                   gameobjs.Wall(startpos=(38, 2)),
                   gameobjs.Wall(startpos=(37, 2)),
                   gameobjs.Wall(startpos=(36, 2)),
                   gameobjs.Wall(startpos=(35, 2)),
                   gameobjs.Wall(startpos=(34, 2)),
                   gameobjs.Wall(startpos=(33, 2)),
                   gameobjs.Wall(startpos=(32, 2)),
                   gameobjs.Wall(startpos=(31, 2)),
                   gameobjs.Wall(startpos=(30, 2)),
                   gameobjs.Wall(startpos=(29, 2)),
                   gameobjs.Wall(startpos=(37, 7)),
                   gameobjs.Wall(startpos=(36, 7)),
                   gameobjs.Wall(startpos=(35, 7)),
                   gameobjs.Wall(startpos=(34, 7)),
                   gameobjs.Wall(startpos=(33, 7)),
                   gameobjs.Wall(startpos=(32, 7)),
                   gameobjs.Wall(startpos=(31, 7)),
                   gameobjs.Wall(startpos=(30, 7)),
                   gameobjs.Wall(startpos=(29, 7)),
                   gameobjs.Wall(startpos=(28, 7)),
                   gameobjs.Wall(startpos=(27, 7)),
                   gameobjs.Wall(startpos=(26, 7)),
                   gameobjs.Wall(startpos=(28, 2)),
                   gameobjs.Wall(startpos=(27, 2)),
                   gameobjs.Wall(startpos=(26, 2)),
                   gameobjs.Wall(startpos=(25, 2)),
                   gameobjs.Wall(startpos=(25, 7)),
                   gameobjs.Wall(startpos=(25, 8)),
                   gameobjs.Wall(startpos=(25, 9)),
                   gameobjs.Wall(startpos=(24, 9)),
                   gameobjs.Wall(startpos=(23, 9)),
                   gameobjs.Wall(startpos=(22, 9)),
                   gameobjs.Wall(startpos=(21, 9)),
                   gameobjs.Wall(startpos=(20, 9)),
                   gameobjs.Wall(startpos=(19, 9)),
                   gameobjs.Wall(startpos=(18, 9)),
                   gameobjs.Wall(startpos=(17, 9)),
                   gameobjs.Wall(startpos=(17, 8)),
                   gameobjs.Wall(startpos=(17, 7)),
                   gameobjs.Wall(startpos=(17, 6)),
                   gameobjs.Wall(startpos=(17, 5)),
                   gameobjs.Wall(startpos=(17, 4)),
                   gameobjs.Wall(startpos=(17, 3)),
                   gameobjs.Wall(startpos=(17, 2)),
                   gameobjs.Wall(startpos=(17, 1)),
                   gameobjs.Wall(startpos=(17, 0)),
                   gameobjs.Wall(startpos=(18, 0)),
                   gameobjs.Wall(startpos=(19, 0)),
                   gameobjs.Wall(startpos=(20, 0)),
                   gameobjs.Wall(startpos=(21, 0)),
                   gameobjs.Wall(startpos=(22, 0)),
                   gameobjs.Wall(startpos=(23, 0)),
                   gameobjs.Wall(startpos=(24, 0)),
                   gameobjs.Wall(startpos=(25, 0)),
                   gameobjs.Wall(startpos=(25, 1)),
                   gameobjs.Wall(startpos=(39, 2)),
                   gameobjs.Wall(startpos=(40, 2)),
                   gameobjs.Wall(startpos=(41, 2)),
                   gameobjs.Wall(startpos=(42, 2)),
                   gameobjs.Wall(startpos=(43, 2)),
                   gameobjs.Wall(startpos=(44, 2)),
                   gameobjs.Wall(startpos=(44, 3)),
                   gameobjs.Wall(startpos=(44, 4)),
                   gameobjs.Wall(startpos=(44, 5)),
                   gameobjs.Wall(startpos=(44, 6)),
                   gameobjs.Wall(startpos=(44, 7)),
                   gameobjs.Wall(startpos=(44, 8)),
                   gameobjs.Wall(startpos=(44, 9)),
                   gameobjs.Wall(startpos=(44, 10)),
                   gameobjs.Wall(startpos=(44, 11)),
                   gameobjs.Wall(startpos=(44, 12)),
                   gameobjs.Wall(startpos=(44, 13)),
                   gameobjs.Wall(startpos=(44, 14)),
                   gameobjs.Wall(startpos=(44, 15)),
                   gameobjs.Wall(startpos=(44, 16)),
                   gameobjs.Wall(startpos=(44, 17)),
                   gameobjs.Wall(startpos=(44, 18)),
                   launcher,
                   gameobjs.Levelporter(startpos=(18, 8), targetlevel='Mutex'),
                   gameobjs.ArbitraryCodeExecutor(startpos=(45, 5), code=launcherprotector, execlocals={"objs":level15.objlist,"distance":gameobjs.distance,"launcher":launcher})]
level15.hints=["Oh good. We're being shot at again. I guess you'll have to stop those. Try turning the missile around like last time, maybe?",
               "Huh. Any missiles that get close self-destruct - presumably that gizmo over to the right of it is doing it."
               "It'd be nice to have one of those for yourself, huh?",
               "Oh wait. You do.",
               "But you'll have to program it. ArbitraryCodeExecutors take strings, not functions, which means you can't "
               "use closures, but this one has access to objs anyway.",
               "Missiles self-destruct if their target becomes invalid, by the way, such as if you set it to None. "
               "Or you could just warp them off into the cornfield.",
               "The next hint is the complete solution.",
               "objs[0].code=\"for theobj in objs: setattr(theobj,'target',None)\""]

class Level16(Level):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.rdrone = gameobjs.Drone(startpos=(52, 4))
        self.rgate1 = gameobjs.Gate(startpos=(52, 9))
        self.rmutexgate = gameobjs.Gate(startpos=(42, 13))
        self.rmutexgate2 = gameobjs.Gate(startpos=(40, 18))

        self.ldrone = gameobjs.Drone(startpos=(20, 5))
        self.lgate1 = gameobjs.Gate(startpos=(25, 6))
        self.lmutexgate = gameobjs.Gate(startpos=(31, 11))
        self.lmutexgate2 = gameobjs.Gate(startpos=(36, 18))

        self.pmutexgate = gameobjs.Gate(startpos=(41, 11))
        self.pmutexgate2 = gameobjs.Gate(startpos=(38, 18))
        self.pgate2 = gameobjs.Gate(startpos=(38, 21))
        self.pgate3 = gameobjs.Gate(startpos=(38, 23))


    def l16shutdoors(self):
        self.rmutexgate.open = False
        self.rmutexgate2.open = False
        self.lmutexgate.open = False
        self.lmutexgate2.open = False
        self.pmutexgate.open = False
        self.pmutexgate2.open = False

    def l16rmutexfunc(self):
        self.l16shutdoors()
        self.rmutexgate.open = True
        self.rmutexgate2.open = True


    def l16pmutexfunc(self):
        self.l16shutdoors()
        self.pmutexgate.open = True
        self.pmutexgate2.open = True


    def l16lmutexfunc(self):
        self.l16shutdoors()
        self.lmutexgate.open = True
        self.lmutexgate2.open = True

    def l16lockplayer(self):
        self.consolelockedmessage = "[CONSOLE LOCKED BY <TriggerFunction object at (40, 8)>]"
        self.consolelockedtimer = 60
        self.plyobj.consolelocked = True
        self.important_obj_destroyed = True  # to display the "press R to reset"


level16=Level16("LEVEL 16: LOCK IT DOWN",[],gameobjs.Player((38, 7)),playlists["Calm"])

ptrig = gameobjs.TriggerFunction((29, 9), level16.l16pmutexfunc)
rtrig = gameobjs.TriggerFunction((27, 9), level16.l16rmutexfunc)
locktrig=gameobjs.TriggerFunction((40, 8), level16.l16lockplayer)
ltrig = gameobjs.TriggerFunction((31, 9), level16.l16lmutexfunc)

rmutexplate=gameobjs.PressurePlate(target=rtrig, startpos=(43, 13))
lmutexplate=gameobjs.PressurePlate(target=ltrig, startpos=(30, 11))
pmutexplate=gameobjs.PressurePlate(target=ptrig, startpos=(41, 10))

pbe=gameobjs.ProtonBeamEmitter(startpos=(55, 12), angle=-90)
pblockerpbe=gameobjs.ProtonBeamEmitter(startpos=(36, 21), angle=90)

level16.extralocals={"upload_left":level16.ldrone.upload, "upload_right": level16.rdrone.upload}

level16.objlist=[]
level16.safe_objlist=[
                    gameobjs.AttributeEnforcer((40, 6),{level16.rmutexgate:("pos",), level16.lmutexgate:("pos",), level16.rmutexgate2:("pos",), level16.lmutexgate2:("pos",), level16.lgate1:("pos",), level16.rgate1:("pos",)}),
                    rtrig, ptrig, ltrig, locktrig,
                   gameobjs.PressurePlate((38,8), target=locktrig, singleimpulse=True),
                   gameobjs.Wall(startpos=(37, 6)),
                   gameobjs.Wall(startpos=(38, 6)),
                   gameobjs.Wall(startpos=(39, 6)),
                   gameobjs.Wall(startpos=(39, 7)),
                   gameobjs.Wall(startpos=(39, 8)),
                   gameobjs.Wall(startpos=(37, 7)),
                   gameobjs.Wall(startpos=(39, 9)),
                   gameobjs.Wall(startpos=(38, 9)),
                   gameobjs.Wall(startpos=(37, 9)),
                   gameobjs.Wall(startpos=(36, 9)),
                   gameobjs.Wall(startpos=(36, 7)),
                   gameobjs.Wall(startpos=(35, 7)),
                   gameobjs.Wall(startpos=(34, 7)),
                   gameobjs.Wall(startpos=(34, 8)),
                   gameobjs.Wall(startpos=(34, 9)),
                   gameobjs.Wall(startpos=(34, 10)),
                   gameobjs.Wall(startpos=(34, 11)),
                   gameobjs.Wall(startpos=(35, 11)),
                   gameobjs.Wall(startpos=(36, 11)),
                   gameobjs.Wall(startpos=(37, 11)),
                   gameobjs.Wall(startpos=(38, 11)),
                   gameobjs.Wall(startpos=(39, 11)),
                   gameobjs.Wall(startpos=(40, 9)),
                   gameobjs.Wall(startpos=(41, 9)),
                   gameobjs.Wall(startpos=(42, 9)),
                   gameobjs.Wall(startpos=(42, 10)),
                   gameobjs.Wall(startpos=(42, 11)),
                   gameobjs.Wall(startpos=(42, 12)),
                   gameobjs.Wall(startpos=(42, 14)),
                   gameobjs.Wall(startpos=(42, 15)),
                   gameobjs.Wall(startpos=(34, 14)),
                   gameobjs.Wall(startpos=(34, 15)),
                   gameobjs.Wall(startpos=(34, 16)),
                   gameobjs.Wall(startpos=(34, 17)),
                   gameobjs.Wall(startpos=(42, 16)),
                   gameobjs.Wall(startpos=(42, 17)),
                   gameobjs.Wall(startpos=(35, 14)),
                   gameobjs.Wall(startpos=(41, 14)),
                   gameobjs.Wall(startpos=(40, 15)),
                   gameobjs.Wall(startpos=(36, 15)),
                   gameobjs.Wall(startpos=(37, 16)),
                   gameobjs.Wall(startpos=(35, 15)),
                   gameobjs.Wall(startpos=(36, 16)),
                   gameobjs.Wall(startpos=(39, 16)),
                   gameobjs.Wall(startpos=(41, 15)),
                   gameobjs.Wall(startpos=(40, 16)),
                   gameobjs.Wall(startpos=(33, 14)),
                   gameobjs.Wall(startpos=(32, 13)),
                   gameobjs.Wall(startpos=(32, 14)),
                   gameobjs.Wall(startpos=(31, 13)),
                   gameobjs.Wall(startpos=(31, 12)),
                   gameobjs.Wall(startpos=(33, 10)),
                   gameobjs.Wall(startpos=(32, 10)),
                   gameobjs.Wall(startpos=(31, 10)),
                   gameobjs.Wall(startpos=(30, 10)),
                   gameobjs.Wall(startpos=(29, 10)),
                   gameobjs.Wall(startpos=(28, 10)),
                   gameobjs.Wall(startpos=(27, 10)),
                   gameobjs.Wall(startpos=(30, 12)),
                   gameobjs.Wall(startpos=(29, 12)),
                   gameobjs.Wall(startpos=(28, 12)),
                   gameobjs.Wall(startpos=(27, 12)),
                   gameobjs.Wall(startpos=(26, 12)),
                   gameobjs.Wall(startpos=(26, 10)),
                   gameobjs.Wall(startpos=(25, 12)),
                   gameobjs.Wall(startpos=(24, 12)),
                   gameobjs.Wall(startpos=(24, 11)),
                   gameobjs.Wall(startpos=(24, 10)),
                   gameobjs.Wall(startpos=(24, 9)),
                   gameobjs.Wall(startpos=(24, 8)),
                   gameobjs.Wall(startpos=(26, 9)),
                   gameobjs.Wall(startpos=(26, 8)),
                   gameobjs.Wall(startpos=(26, 7)),
                   gameobjs.Wall(startpos=(26, 6)),
                   gameobjs.Wall(startpos=(24, 7)),
                   gameobjs.Wall(startpos=(24, 6)),
                   gameobjs.Wall(startpos=(26, 5)),
                   gameobjs.Wall(startpos=(26, 4)),
                   gameobjs.Wall(startpos=(25, 4)),
                   gameobjs.Wall(startpos=(24, 4)),
                   gameobjs.Wall(startpos=(23, 4)),
                   gameobjs.Wall(startpos=(22, 4)),
                   gameobjs.Wall(startpos=(21, 4)),
                   gameobjs.Wall(startpos=(20, 4)),
                   gameobjs.Wall(startpos=(23, 6)),
                   gameobjs.Wall(startpos=(22, 6)),
                   gameobjs.Wall(startpos=(21, 6)),
                   gameobjs.Wall(startpos=(20, 6)),
                   gameobjs.Wall(startpos=(19, 6)),
                   gameobjs.Wall(startpos=(19, 5)),
                   gameobjs.Wall(startpos=(19, 4)),
                   gameobjs.Wall(startpos=(43, 12)),
                   gameobjs.Wall(startpos=(44, 12)),
                   gameobjs.Wall(startpos=(45, 12)),
                   gameobjs.Wall(startpos=(43, 14)),
                   gameobjs.Wall(startpos=(44, 14)),
                   gameobjs.Wall(startpos=(45, 14)),
                   gameobjs.Wall(startpos=(46, 14)),
                   gameobjs.Wall(startpos=(47, 14)),
                   gameobjs.Wall(startpos=(48, 14)),
                   gameobjs.Wall(startpos=(49, 14)),
                   gameobjs.Wall(startpos=(50, 14)),
                   gameobjs.Wall(startpos=(51, 14)),
                   gameobjs.Wall(startpos=(46, 12)),
                   gameobjs.Wall(startpos=(47, 12)),
                   gameobjs.Wall(startpos=(48, 12)),
                   gameobjs.Wall(startpos=(49, 12)),
                   gameobjs.Wall(startpos=(50, 12)),
                   gameobjs.Wall(startpos=(51, 12)),
                   gameobjs.Wall(startpos=(52, 14)),
                   gameobjs.Wall(startpos=(53, 14)),
                   gameobjs.Wall(startpos=(53, 12)),
                   gameobjs.Wall(startpos=(53, 11)),
                   gameobjs.Wall(startpos=(53, 10)),
                   gameobjs.Wall(startpos=(53, 9)),
                   gameobjs.Wall(startpos=(53, 8)),
                   gameobjs.Wall(startpos=(53, 7)),
                   gameobjs.Wall(startpos=(53, 6)),
                   gameobjs.Wall(startpos=(53, 5)),
                   gameobjs.Wall(startpos=(53, 4)),
                   gameobjs.Wall(startpos=(51, 11)),
                   gameobjs.Wall(startpos=(51, 10)),
                   gameobjs.Wall(startpos=(51, 9)),
                   gameobjs.Wall(startpos=(51, 8)),
                   gameobjs.Wall(startpos=(51, 7)),
                   gameobjs.Wall(startpos=(51, 6)),
                   gameobjs.Wall(startpos=(51, 5)),
                   gameobjs.Wall(startpos=(51, 4)),
                   gameobjs.Wall(startpos=(51, 3)),
                   gameobjs.Wall(startpos=(52, 3)),
                   gameobjs.Wall(startpos=(53, 3)),
                   gameobjs.Wall(startpos=(38, 12)),
                   gameobjs.Wall(startpos=(34, 18)),
                   gameobjs.Wall(startpos=(35, 18)),
                   gameobjs.Wall(startpos=(37, 18)),
                   gameobjs.Wall(startpos=(39, 18)),
                   gameobjs.Wall(startpos=(41, 18)),
                   gameobjs.Wall(startpos=(42, 18)),
                   gameobjs.Wall(startpos=(37, 19)),
                   gameobjs.Wall(startpos=(39, 19)),
                   gameobjs.Wall(startpos=(37, 20)),
                   gameobjs.Wall(startpos=(36, 20)),
                   gameobjs.Wall(startpos=(35, 20)),
                   gameobjs.Wall(startpos=(34, 20)),
                   gameobjs.Wall(startpos=(33, 20)),
                   gameobjs.Wall(startpos=(32, 20)),
                   gameobjs.Wall(startpos=(39, 20)),
                   gameobjs.Wall(startpos=(40, 20)),
                   gameobjs.Wall(startpos=(41, 20)),
                   gameobjs.Wall(startpos=(42, 20)),
                   gameobjs.Wall(startpos=(43, 20)),
                   gameobjs.Wall(startpos=(44, 20)),
                   gameobjs.Wall(startpos=(33, 18)),
                   gameobjs.Wall(startpos=(32, 18)),
                   gameobjs.Wall(startpos=(31, 18)),
                   gameobjs.Wall(startpos=(31, 20)),
                   gameobjs.Wall(startpos=(30, 20)),
                   gameobjs.Wall(startpos=(29, 20)),
                   gameobjs.Wall(startpos=(30, 18)),
                   gameobjs.Wall(startpos=(29, 18)),
                   gameobjs.Wall(startpos=(28, 18)),
                   gameobjs.Wall(startpos=(28, 20)),
                   gameobjs.Wall(startpos=(43, 18)),
                   gameobjs.Wall(startpos=(44, 18)),
                   gameobjs.Wall(startpos=(45, 18)),
                   gameobjs.Wall(startpos=(46, 18)),
                   gameobjs.Wall(startpos=(45, 20)),
                   gameobjs.Wall(startpos=(46, 20)),
                   gameobjs.Wall(startpos=(47, 20)),
                   gameobjs.Wall(startpos=(47, 18)),
                   gameobjs.Wall(startpos=(48, 20)),
                   gameobjs.Wall(startpos=(37, 21)),
                   gameobjs.Wall(startpos=(39, 21)),
                   gameobjs.Wall(startpos=(39, 22)),
                   gameobjs.Wall(startpos=(39, 23)),
                   gameobjs.Wall(startpos=(39, 24)),
                   gameobjs.SafePressurePlate(target=level16.rgate1, startpos=(35, 9)),
                   level16.pmutexgate,
                   gameobjs.Wall(startpos=(40, 11)),
                   level16.lmutexgate,
                   level16.rmutexgate,
                   level16.lmutexgate2,
                   level16.pmutexgate2,
                   level16.rmutexgate2,
                   gameobjs.Wall(startpos=(54, 12)),
                   gameobjs.Wall(startpos=(54, 14)),
                   gameobjs.Wall(startpos=(55, 14)),
                   gameobjs.Wall(startpos=(56, 12)),
                   gameobjs.Wall(startpos=(56, 13)),
                   gameobjs.Wall(startpos=(56, 14)),
                   pbe,
                   gameobjs.Wall(startpos=(54, 11)),
                   gameobjs.Wall(startpos=(55, 11)),
                   gameobjs.Wall(startpos=(56, 11)),
                   level16.lgate1,
                   gameobjs.SafePressurePlate(target=level16.lgate1, startpos=(52, 6)),
                   level16.rdrone,
                   level16.ldrone,
                   gameobjs.PressurePlate(target=pbe, startpos=(25, 11)),
                   gameobjs.Wall(startpos=(27, 18)),
                   gameobjs.Wall(startpos=(27, 20)),
                   level16.rgate1,
                   gameobjs.Wall(startpos=(37, 23)),
                   gameobjs.Wall(startpos=(37, 24)),
                   gameobjs.Wall(startpos=(37, 25)),
                   gameobjs.Wall(startpos=(38, 25)),
                   gameobjs.Wall(startpos=(39, 25)),
                   pblockerpbe,
                   gameobjs.Wall(startpos=(36, 23)),
                   gameobjs.Wall(startpos=(35, 21)),
                   gameobjs.Wall(startpos=(35, 22)),
                   gameobjs.Wall(startpos=(35, 23)),
                   gameobjs.Levelporter(targetlevel='Retarget', startpos=(38, 24)),
                   rmutexplate,
                   pmutexplate,
                   lmutexplate,
                   gameobjs.Wall(startpos=(48, 18)),
                   gameobjs.Wall(startpos=(49, 18)),
                   gameobjs.Wall(startpos=(49, 17)),
                   gameobjs.Wall(startpos=(49, 16)),
                   gameobjs.Wall(startpos=(50, 16)),
                   gameobjs.Wall(startpos=(51, 16)),
                   gameobjs.Wall(startpos=(51, 17)),
                   gameobjs.Wall(startpos=(51, 18)),
                   gameobjs.Wall(startpos=(51, 19)),
                   gameobjs.Wall(startpos=(51, 20)),
                   gameobjs.Wall(startpos=(51, 21)),
                   gameobjs.Wall(startpos=(51, 22)),
                   gameobjs.Wall(startpos=(51, 23)),
                   gameobjs.Wall(startpos=(50, 23)),
                   gameobjs.Wall(startpos=(49, 23)),
                   gameobjs.Wall(startpos=(49, 22)),
                   gameobjs.Wall(startpos=(49, 21)),
                   gameobjs.Wall(startpos=(49, 20)),
                   level16.pgate2,
                   gameobjs.PressurePlate(target=pblockerpbe, startpos=(49, 19)),
                   gameobjs.SafePressurePlate(target=level16.pgate2, startpos=(50, 22)),
                   level16.pgate3,
                   gameobjs.Wall(startpos=(27, 17)),
                   gameobjs.Wall(startpos=(27, 16)),
                   gameobjs.Wall(startpos=(26, 16)),
                   gameobjs.Wall(startpos=(25, 16)),
                   gameobjs.Wall(startpos=(25, 17)),
                   gameobjs.Wall(startpos=(25, 18)),
                   gameobjs.Wall(startpos=(25, 19)),
                   gameobjs.Wall(startpos=(25, 20)),
                   gameobjs.Wall(startpos=(25, 21)),
                   gameobjs.Wall(startpos=(25, 22)),
                   gameobjs.Wall(startpos=(25, 23)),
                   gameobjs.Wall(startpos=(26, 23)),
                   gameobjs.Wall(startpos=(27, 23)),
                   gameobjs.Wall(startpos=(27, 22)),
                   gameobjs.Wall(startpos=(27, 21)),
                   gameobjs.Boulder(startpos=(26, 17)),
                   gameobjs.SafePressurePlate(target=level16.pgate3, startpos=(26, 22))]
level16.hints=[
    "Walking on to that pressure plate right below you will lock you out of the console. So... don't do that yet.",
    "The first pressure plate on the rightmost path opens the door on the leftmost path. The first plate on your path opens the first door on the rightmost path.",
"""You and the other two all need to communicate and cooperate to get through.
I suggest you start by using a global variable like this:

sync="initial"

def left_think(self):
 global sync
 if sync == "initial":
  sync="ldrone_doing_things"
  ...

def right_think(self):
 global sync
 if sync == "initial":
  ...

Technically Python doesn't really have globals, it has "closures" - when you say "global sync," what you're really
saying is "I want to use the name "sync" as it was used in the context this function was defined." If you don't
use the = operator, you don't even have to use the "global" - it's only necessary because if you don't Python will think
you're declaring "sync" to mean whatever only in your function. If it didn't do that it'd be easy to change things
by accident!

Use that to have one global "state" that everyone is in instead of trying to synchronize three separate threads of
execution. Obviously you can't see it once the console is locked but you can just watch what they're doing.
""",
    "It's probably wise to 'hardcode' some movement sequences by storing a list of moves and then doing something like \nif moves: \n  self.move(*moves.pop(0))\n",
    "You also need to make sure only one drone is passing through the room in the middle at one time, otherwise the first drone to go through will be locked out of its destination.",
    "The next hint is the complete solution. \nYou might want to use exec(hints.hintlist[-1]) to just execute it AFTER you read and understand it of course.",
"""
sync = "gate1approach"

def ldrone(self):
	global sync
	if sync == "gate1approach":
		self.move(1,0)
		if self.bumped:
			sync="gate1"
			#We hit the wall, tell rdrone to head down
	elif sync == "gate1":
		self.move(0,1)
		if not self.bumped:
			sync="pbeshutdown"
			#rdrone hit the plate;
			#tell her to stop before she walks in to the beam!
			self.moves=[(0,1)] * 5
			#and plan out our next moves so we can get on the plate
	elif sync == "pbeshutdown":
			#move down on to the plate so rdrone can get out
			if self.moves:
				self.move(*self.moves.pop(0))
			else:
				#if we're done moving, we must be on the plate
				sync = "waitingfor@toopenrdoor"
				self.moves=[ (1, 0) ] * 8
				self.moves.extend([ (1, 1) ] * 5)
				self.moves.extend([ (0, 1) ] * 6)
				self.moves.extend([ (-1, 0) ] * 2)
				self.moves.extend([ (0, 1) ] * 3)
				#get ready to go through the mutex lock
	elif sync == "ldronemutex":
		#Time for ldrone to use the mutex lock!
		if self.moves:
			self.move(*self.moves.pop(0))
		else:
			sync="@mutex"
	elif sync == "@mutex":
		if self.bumped:
			if "Boulder" in str(self.bumped):
				if "rdronepos" in globals():
					x, y = rdronepos
					self.bumped.pos=(x+1, y)
					self.bumped.momentum=(0,1)
					self.move(0,1)
			else:
				self.move(-1, 0)
		else:
			self.move(0,1)

def rdrone(self):
	global sync
	if sync == "gate1":
		self.move(0,1)
		#Move down 'till ldrone reports the door has opened
		#Then stop because otherwise we'll hit the party beam!
	elif sync == "pbeshutdown":
		#We head on down to the door @ has to open
		#while ldrone gets on the PBE shutdown plate
		self.move(0,1)
	elif sync == "waitingfor@toopenrdoor":
		self.move(0,1)
		if not self.bumped:
			#if the door doesn't stop us @ must've opened it
			#head out into the lock
			sync="rdronemutex"
			self.moves=[(0,1), (0,1), (0,1), (-1, 1)]
			self.moves.extend( [ (-1, 0) ] * 14)
			self.moves.extend( [ (0, 1) ] * 4 )
			self.moves.extend( [ (1, 0), (1, 0), (0,1), (0,1) ] )
	elif sync == "rdronemutex":
		#pretty simple; just chew through our movelist to pass
		#the mutex lock area
		if self.moves:
			self.move(*self.moves.pop(0))
		else:
			#Once we're finished, let ldrone use the mutex
			sync="ldronemutex"
			self.moves=[ (1,0) ] * 9
	elif sync == "@mutex":
		#May as well head on over to that plate
		if self.moves:
			self.move(*self.moves.pop(0))
		else:
			#Tell ldrone where we are so he can put the boulder just
			#to the right
			global rdronepos
			rdronepos=self.pos

upload_left(ldrone)
upload_right(rdrone)

#When the others stop, that's your cue to move forward.
"""
]

#DEPRECATED LV17
#Just couldn't get it to work...

particleintercept="""
import math

pbe.active = distance(player.pos,pbe.pos) < 5.2

x, y = player.pos
ox, oy = pbe.pos
dx = x - ox
dy = y - (oy+1) #remember pbe-s fire from the square below them
theta=math.atan2(dx, dy)
pbe.angle=math.degrees(theta)
"""

#This is the abandoned second concept for level 17... still want to do something with this,
level17=Level("LEVEL 17: SIMULATED EMISSION",[],gameobjs.Player((36, 2)),playlists["Intense"])
pbe=gameobjs.ProtonBeamEmitter(startpos=(33, 7))
level17sign = gameobjs.Sign((60, 6), "EAT LASER YOU NERD", textfgcolor="maroon")
#This is for the player to find and use to put the boulders in the way of the laser shots.
def RECIEVE_LAZER_EVENT(origin_pos, target_pos):
    level17sign.textfgcolor = "red" if level17sign.textfgcolor == "maroon" else "maroon"

level17sign.RECIEVE_LAZER_EVENT=RECIEVE_LAZER_EVENT #deliberately misspelled, of course
level17.hooks.register(level17sign, "on_laser_fire", "RECIEVE_LAZER_EVENT")

level17.objlist=[gameobjs.ArbitraryCodeExecutor(code=particleintercept, startpos=(30, 3), execlocals={"pbe":pbe,"player":level17.plyobj.proxy,"distance":gameobjs.distance}),
                 pbe,
                 gameobjs.Boulder(startpos=(34,24)),
                 gameobjs.Boulder(startpos=(34,23)),
                 level17sign
                ]
level17.safe_objlist=[gameobjs.Wall(startpos=(36, 1)),
                   gameobjs.Wall(startpos=(35, 1)),
                   gameobjs.Wall(startpos=(37, 1)),
                   gameobjs.Wall(startpos=(38, 2)),
                   gameobjs.Wall(startpos=(39, 2)),
                   gameobjs.Wall(startpos=(39, 3)),
                   gameobjs.Wall(startpos=(39, 4)),
                   gameobjs.Wall(startpos=(39, 5)),
                   gameobjs.Wall(startpos=(39, 6)),
                   gameobjs.Wall(startpos=(39, 7)),
                   gameobjs.Wall(startpos=(39, 8)),
                   gameobjs.Wall(startpos=(39, 9)),
                   gameobjs.Wall(startpos=(39, 10)),
                   gameobjs.Wall(startpos=(39, 11)),
                   gameobjs.Wall(startpos=(39, 12)),
                   gameobjs.Wall(startpos=(39, 13)),
                   gameobjs.Wall(startpos=(39, 14)),
                   gameobjs.Wall(startpos=(39, 15)),
                   gameobjs.Wall(startpos=(39, 16)),
                   gameobjs.Wall(startpos=(39, 17)),
                   gameobjs.Wall(startpos=(39, 18)),
                   gameobjs.Wall(startpos=(39, 19)),
                   gameobjs.Wall(startpos=(39, 20)),
                   gameobjs.Wall(startpos=(39, 21)),
                   gameobjs.Wall(startpos=(39, 22)),
                   gameobjs.Wall(startpos=(39, 23)),
                   gameobjs.Wall(startpos=(39, 24)),
                   gameobjs.Wall(startpos=(39, 25)),
                   gameobjs.Wall(startpos=(39, 26)),
                   gameobjs.Wall(startpos=(39, 27)),
                   gameobjs.Wall(startpos=(34, 2)),
                   gameobjs.Wall(startpos=(33, 2)),
                   gameobjs.Wall(startpos=(33, 5)),
                   gameobjs.Wall(startpos=(33, 6)),
                   #gameobjs.Wall(startpos=(33, 7)),
                   gameobjs.Wall(startpos=(33, 9)),
                   gameobjs.Wall(startpos=(33, 11)),
                   gameobjs.Wall(startpos=(33, 12)),
                   gameobjs.Wall(startpos=(33, 14)),
                   gameobjs.Wall(startpos=(33, 15)),
                   gameobjs.Wall(startpos=(33, 17)),
                   gameobjs.Wall(startpos=(33, 19)),
                   gameobjs.Wall(startpos=(33, 20)),
                   gameobjs.Wall(startpos=(33, 21)),
                   gameobjs.Wall(startpos=(33, 24)),
                   gameobjs.Wall(startpos=(33, 25)),
                   gameobjs.Wall(startpos=(33, 26)),
                   gameobjs.Wall(startpos=(33, 27)),
                   gameobjs.Wall(startpos=(34, 27)),
                   gameobjs.Wall(startpos=(38, 27)),
                   gameobjs.Wall(startpos=(35, 27)),
                   gameobjs.Wall(startpos=(37, 27)),
                   gameobjs.Wall(startpos=(35, 28)),
                   gameobjs.Wall(startpos=(37, 28)),
                   gameobjs.Wall(startpos=(35, 29)),
                   gameobjs.Wall(startpos=(36, 29)),
                   gameobjs.Wall(startpos=(37, 29)),
                   gameobjs.Levelporter(targetlevel='Lasers', startpos=(36, 28)),
                   gameobjs.Wall(startpos=(33, 16)),
                   gameobjs.Wall(startpos=(33, 10)),
                   gameobjs.Wall(startpos=(33, 22)),
                   #gameobjs.LaserTurret(startpos=(33, 8)),
                   gameobjs.LaserTurret(startpos=(33, 13)),
                   gameobjs.LaserTurret(startpos=(33, 18)),
                   gameobjs.LaserTurret(startpos=(33, 23)),
                   gameobjs.Wall(startpos=(32, 2)),
                   gameobjs.Wall(startpos=(32, 4)),
                   gameobjs.Wall(startpos=(32, 5)),
                   gameobjs.Wall(startpos=(32, 6)),
                   gameobjs.Wall(startpos=(32, 7)),
                   gameobjs.Wall(startpos=(32, 8)),
                   gameobjs.Wall(startpos=(32, 9)),
                   gameobjs.Wall(startpos=(32, 10)),
                   gameobjs.Wall(startpos=(32, 11)),
                   gameobjs.Wall(startpos=(32, 12)),
                   gameobjs.Wall(startpos=(32, 13)),
                   gameobjs.Wall(startpos=(32, 14)),
                   gameobjs.Wall(startpos=(32, 15)),
                   gameobjs.Wall(startpos=(32, 16)),
                   gameobjs.Wall(startpos=(32, 17)),
                   gameobjs.Wall(startpos=(32, 18)),
                   gameobjs.Wall(startpos=(32, 19)),
                   gameobjs.Wall(startpos=(32, 20)),
                   gameobjs.Wall(startpos=(32, 21)),
                   gameobjs.Wall(startpos=(32, 22)),
                   gameobjs.Wall(startpos=(32, 23)),
                   gameobjs.Wall(startpos=(32, 24)),
                   gameobjs.Wall(startpos=(32, 25)),
                   gameobjs.Wall(startpos=(32, 26)),
                   gameobjs.Wall(startpos=(32, 27)),
                   gameobjs.Wall(startpos=(38, 1)),
                   gameobjs.Wall(startpos=(34, 1)),
                   gameobjs.Wall(startpos=(32, 3)),
                   gameobjs.AttributeEnforcer(inputdict={pbe:("pos",)}, startpos=(30, 9))]

level17.hints=[
    "That proton beam seems to be set up to zap you whenever you get too close. Probably the ACE that's making it do that.",
    "So blank out its code. Still have to deal with those laser turrets though. They don't seem to want to shoot when they don't have line-of-sight to you...",
    "Blocking them off with the boulders might do it?",
    "Nope; can't block them all with two boulders. If only there was some way to put them in the way just in time to block the shot...",
    "Did you notice the text over to the side flashes when the lasers fire?",
    "It's reacting to them somehow. Maybe however it's doing that could help you react to those laser shots?",
    "The, uh, 'RECIEVE_LAZER_EVENT' function (you can tell it's just a function stapled to the sign object and not a real method, because it doesn't take a self parameter) gets called every time a laser fires...",
    "And when it does, it's provided with the start and end position of the laser beam. Well, you already know where the end position is gonna be, but...",
    "But that's all the information you need to block the shot.",
    "Just putting it at the origin, on top of the turret, won't work - the turret must have some way of handling that, "
    "otherwise the beam would hit the turret itself.",
    "Some of the math you need was being done inside that PBE, by the way.",
    "Remember that these positions are packages of two numbers, the first representing how far you are along a horizontal line,"
    "the second representing how far you are along a vertical line. It might be easier to figure out the rules if you imagine one number and one line"
    "at first, though...",
    """Imagine the world we're working with is this line:

----------⌀---------@----
0    5    10   15   20   25

The turret is at 10, and you are at 20. Our goal is to find the number representing the spot right between you
    """
"""
objs[0].code=''
sign = objs[-1]
rock = objs[-2]
rock.gravity = (0,0)
def interdict(laser, target):
    lx, ly = laser
    tx, ty = target
    #Find the line from destination to origin;
    #think of it like, 5 - 3 gives you what you need to add to 3 to get 5
    #origin - destination gives you what you need to add to origin to get the destination
    dx, dy = tx - lx, ty - ly
    
    #We want the boulder to be roughly halfway between you and the turret
    dx, dy = dx // 5, dy // 5
    
    #And add that vector to your position to get a point offset that much in that direction
    rock.pos = (lx + dx, ly + dy)
sign.RECIEVE_LAZER_EVENT = interdict
"""
]

level17=Level("LEVEL 17: LASERDOME",[],gameobjs.Player((39, 3)),playlists["Intense"])
level17.objlist=[gameobjs.LaserTurret(startpos=(41, 16)),
                   gameobjs.LaserTurret(startpos=(37, 16))]
level17.safe_objlist=[gameobjs.Wall(startpos=(37, 7)),
                   gameobjs.Wall(startpos=(36, 7)),
                   gameobjs.Wall(startpos=(41, 18)),
                   gameobjs.Wall(startpos=(37, 18)),
                   gameobjs.Wall(startpos=(35, 8)),
                   gameobjs.Wall(startpos=(34, 9)),
                   gameobjs.Wall(startpos=(34, 10)),
                   gameobjs.Wall(startpos=(33, 11)),
                   gameobjs.Wall(startpos=(33, 13)),
                   gameobjs.Wall(startpos=(34, 14)),
                   gameobjs.Wall(startpos=(34, 15)),
                   gameobjs.Wall(startpos=(35, 16)),
                   gameobjs.Wall(startpos=(36, 17)),
                   gameobjs.Wall(startpos=(37, 17)),
                   gameobjs.Wall(startpos=(41, 17)),
                   gameobjs.Wall(startpos=(42, 17)),
                   gameobjs.Wall(startpos=(43, 16)),
                   gameobjs.Wall(startpos=(44, 15)),
                   gameobjs.Wall(startpos=(44, 14)),
                   gameobjs.Wall(startpos=(45, 13)),
                   gameobjs.Wall(startpos=(45, 11)),
                   gameobjs.Wall(startpos=(44, 10)),
                   gameobjs.Wall(startpos=(44, 9)),
                   gameobjs.Wall(startpos=(43, 8)),
                   gameobjs.Wall(startpos=(42, 7)),
                   gameobjs.Wall(startpos=(41, 7)),
                   gameobjs.Wall(startpos=(37, 6)),
                   gameobjs.Wall(startpos=(41, 6)),
                   gameobjs.Wall(startpos=(41, 5)),
                   gameobjs.Wall(startpos=(41, 4)),
                   gameobjs.Wall(startpos=(37, 4)),
                   gameobjs.Wall(startpos=(37, 5)),
                   gameobjs.Wall(startpos=(37, 3)),
                   gameobjs.Wall(startpos=(37, 2)),
                   gameobjs.Wall(startpos=(38, 2)),
                   gameobjs.Wall(startpos=(39, 2)),
                   gameobjs.Wall(startpos=(40, 2)),
                   gameobjs.Wall(startpos=(41, 2)),
                   gameobjs.Wall(startpos=(41, 3)),
                   gameobjs.Wall(startpos=(38, 22)),
                   gameobjs.Wall(startpos=(39, 22)),
                   gameobjs.Wall(startpos=(40, 22)),
                   gameobjs.Wall(startpos=(37, 17)),
                   gameobjs.Wall(startpos=(37, 19)),
                   gameobjs.Wall(startpos=(37, 20)),
                   gameobjs.Wall(startpos=(37, 21)),
                   gameobjs.Wall(startpos=(37, 22)),
                   gameobjs.Wall(startpos=(41, 17)),
                   gameobjs.Wall(startpos=(41, 19)),
                   gameobjs.Wall(startpos=(41, 20)),
                   gameobjs.Wall(startpos=(41, 21)),
                   gameobjs.Wall(startpos=(41, 22)),
                   gameobjs.Wall(startpos=(38, 20)),
                   gameobjs.Wall(startpos=(38, 21)),
                   gameobjs.Wall(startpos=(40, 21)),
                   gameobjs.Wall(startpos=(40, 20)),
                   gameobjs.Levelporter(targetlevel='Lasers', startpos=(39, 21)),
                   gameobjs.MissileLauncher(target=level17.plyobj.proxy, startpos=(33, 12), rate=15),
                   gameobjs.MissileLauncher(target=level17.plyobj.proxy, startpos=(45, 12), rate=15),
                   gameobjs.Wall(startpos=(32, 13)),
                   gameobjs.Wall(startpos=(32, 11)),
                   gameobjs.Wall(startpos=(31, 11)),
                   gameobjs.Wall(startpos=(31, 12)),
                   gameobjs.Wall(startpos=(31, 13)),
                   gameobjs.Wall(startpos=(46, 13)),
                   gameobjs.Wall(startpos=(47, 13)),
                   gameobjs.Wall(startpos=(47, 12)),
                   gameobjs.Wall(startpos=(47, 11)),
                   gameobjs.Wall(startpos=(46, 11)),
                   #gameobjs.Wall(startpos=(38, 7)), #Uncomment these to protect player when level starts
                   #gameobjs.Wall(startpos=(40, 7)),
                   gameobjs.Wall(startpos=(34, 16)),
                   gameobjs.Wall(startpos=(35, 17)),
                   gameobjs.Wall(startpos=(33, 10)),
                   gameobjs.Wall(startpos=(34, 8)),
                   gameobjs.Wall(startpos=(35, 7)),
                   gameobjs.Wall(startpos=(43, 7)),
                   gameobjs.Wall(startpos=(44, 8)),
                   gameobjs.Wall(startpos=(45, 10)),
                   gameobjs.Wall(startpos=(45, 14)),
                   gameobjs.Wall(startpos=(44, 16)),
                   gameobjs.Wall(startpos=(43, 17)),
                   gameobjs.Wall(startpos=(42, 17)),
                   gameobjs.Wall(startpos=(36, 17)),
                   gameobjs.Wall(startpos=(36, 6)),
                   gameobjs.Wall(startpos=(42, 6)),
                   gameobjs.Wall(startpos=(33, 14))]
level17.hints=[
    "Yeah, he's not messing around anymore, you're not safe where you arrive. Lucky thing you just... come back, like that.",
    "Those things shooting red lines at you are laser turrets. If you look, they've got an is_enemy method. Maybe try overriding that?",
    "It takes an object and returns True if it should be shot at, False otherwise.",
    "Shooting a missile with a laser probably doesn't end well for the missile.",
    "You can identify missiles because when you string-ify them there's the word \n\"Missile\" in the string. So, \nreturn \"Missile\" in str(theobj)\n... returns True if the object is a missile.",
    "Except if you just do that it seems to have trouble getting them all... are you sure it's targeting the right thing?",
    "The level also contains MissileLaunchers and MissileTrails left by the missile.",
    "So exclude objects that are \"Launcher\"s or \"Trail\"s.",
    "The next hint is the complete solution.",
"""
def is_missile(obj):
    typestr=str(type(obj))
    return ("Missile" in typestr and
        "Trail" not in typestr and "Launcher" not in typestr)

objs[0].is_enemy=is_missile
objs[1].is_enemy=is_missile
#It'll still miss occasionally, so get running!
"""
]

lv17levelintercept = """
for theobj in objs: 
    if type(theobj).__name__ = "Missile" and theobj.pos[0] < 30:
        theobj.target = None
"""

level18=Level("LEVEL 18: SIMULATED EMISSION",[],gameobjs.Player((39, 21)),playlists["Intense"])
pbe = gameobjs.ProtonBeamEmitter(startpos=(36, 6))
level18.objlist.extend([gameobjs.ArbitraryCodeExecutor(startpos=(35, 20), code='pass', execlocals = {"objs":level18.objlist}),
                   gameobjs.ArbitraryCodeExecutor(startpos=(35, 4), code=particleintercept, execlocals={"pbe":pbe,"player":level18.plyobj.proxy,"distance":gameobjs.distance, "objs":level18.objlist})])
level18.safe_objlist=[gameobjs.Wall(startpos=(39, 2)),
                   gameobjs.Wall(startpos=(38, 2)),
                   gameobjs.Wall(startpos=(38, 3)),
                   gameobjs.Wall(startpos=(40, 2)),
                   gameobjs.Wall(startpos=(40, 3)),
                   gameobjs.Wall(startpos=(38, 4)),
                   gameobjs.Wall(startpos=(40, 4)),
                   gameobjs.Wall(startpos=(38, 5)),
                   gameobjs.Wall(startpos=(37, 5)),
                   gameobjs.Wall(startpos=(36, 5)),
                   gameobjs.Wall(startpos=(35, 5)),
                   gameobjs.Wall(startpos=(34, 5)),
                   gameobjs.Wall(startpos=(33, 5)),
                   gameobjs.Wall(startpos=(31, 5)),
                   gameobjs.Wall(startpos=(30, 5)),
                   gameobjs.Wall(startpos=(29, 5)),
                   gameobjs.Wall(startpos=(28, 5)),
                   gameobjs.Wall(startpos=(27, 5)),
                   gameobjs.Wall(startpos=(26, 5)),
                   gameobjs.Wall(startpos=(25, 5)),
                   gameobjs.Wall(startpos=(24, 5)),
                   gameobjs.Wall(startpos=(23, 5)),
                   gameobjs.Wall(startpos=(22, 5)),
                   gameobjs.Wall(startpos=(21, 5)),
                   gameobjs.Wall(startpos=(20, 5)),
                   gameobjs.Wall(startpos=(19, 5)),
                   gameobjs.Wall(startpos=(18, 5)),
                   gameobjs.Wall(startpos=(18, 6)),
                   gameobjs.Wall(startpos=(18, 7)),
                   gameobjs.Wall(startpos=(18, 8)),
                   gameobjs.Wall(startpos=(18, 9)),
                   gameobjs.Wall(startpos=(18, 10)),
                   gameobjs.Wall(startpos=(18, 11)),
                   gameobjs.Wall(startpos=(18, 12)),
                   gameobjs.Wall(startpos=(18, 13)),
                   gameobjs.Wall(startpos=(18, 14)),
                   gameobjs.Wall(startpos=(18, 15)),
                   gameobjs.Wall(startpos=(18, 16)),
                   gameobjs.Wall(startpos=(18, 17)),
                   gameobjs.Wall(startpos=(18, 18)),
                   gameobjs.Wall(startpos=(18, 19)),
                   gameobjs.Wall(startpos=(19, 19)),
                   gameobjs.Wall(startpos=(20, 19)),
                   gameobjs.Wall(startpos=(21, 19)),
                   gameobjs.Wall(startpos=(22, 19)),
                   gameobjs.Wall(startpos=(23, 19)),
                   gameobjs.Wall(startpos=(24, 19)),
                   gameobjs.Wall(startpos=(25, 19)),
                   gameobjs.Wall(startpos=(26, 19)),
                   gameobjs.Wall(startpos=(27, 19)),
                   gameobjs.Wall(startpos=(28, 19)),
                   gameobjs.Wall(startpos=(29, 19)),
                   gameobjs.Wall(startpos=(30, 19)),
                   gameobjs.Wall(startpos=(31, 19)),
                   gameobjs.Wall(startpos=(32, 19)),
                   gameobjs.Wall(startpos=(33, 19)),
                   gameobjs.Wall(startpos=(34, 19)),
                   gameobjs.Wall(startpos=(35, 19)),
                   gameobjs.Wall(startpos=(36, 19)),
                   gameobjs.Wall(startpos=(37, 19)),
                   gameobjs.Wall(startpos=(38, 19)),
                   gameobjs.Wall(startpos=(40, 5)),
                   gameobjs.Wall(startpos=(41, 5)),
                   gameobjs.Wall(startpos=(42, 5)),
                   gameobjs.Wall(startpos=(42, 6)),
                   gameobjs.Wall(startpos=(42, 7)),
                   gameobjs.Wall(startpos=(42, 8)),
                   gameobjs.Wall(startpos=(42, 9)),
                   gameobjs.Wall(startpos=(42, 10)),
                   gameobjs.Wall(startpos=(42, 11)),
                   gameobjs.Wall(startpos=(42, 12)),
                   gameobjs.Wall(startpos=(42, 13)),
                   gameobjs.Wall(startpos=(42, 14)),
                   gameobjs.Wall(startpos=(42, 15)),
                   gameobjs.Wall(startpos=(42, 16)),
                   gameobjs.Wall(startpos=(42, 17)),
                   gameobjs.Wall(startpos=(42, 18)),
                   gameobjs.Wall(startpos=(42, 19)),
                   gameobjs.Wall(startpos=(41, 19)),
                   gameobjs.Wall(startpos=(40, 19)),
                   gameobjs.Wall(startpos=(38, 20)),
                   gameobjs.Wall(startpos=(38, 21)),
                   gameobjs.Wall(startpos=(40, 20)),
                   gameobjs.Wall(startpos=(40, 21)),
                   gameobjs.Wall(startpos=(38, 22)),
                   gameobjs.Wall(startpos=(39, 22)),
                   gameobjs.Wall(startpos=(40, 22)),
                   gameobjs.LaserTurret(startpos=(19, 18)),
                   gameobjs.LaserTurret(startpos=(19, 6)),
                   gameobjs.LaserTurret(startpos=(19, 12)),
                   gameobjs.MissileLauncher(startpos=(32, 5), target=level18.plyobj.proxy, addsafe=False),
                   gameobjs.Wall(startpos=(31, 4)),
                   gameobjs.Wall(startpos=(32, 4)),
                   gameobjs.Wall(startpos=(33, 4)),
                   gameobjs.Levelporter(startpos=(39, 3), targetlevel='LolRandom'),
                   pbe]
level18.hints = [
    "Well, the first thing to deal with is the executors, that particle beam would kill you if you could actually make it up there.",
    "So now the problem is the laser turrets over to the left, but I don't think there's any way to destroy them...",
    "Is there some other way you can protect yourself?",
    "Hide behind something, maybe?",
    "The laser beams can't pass through the smoke the missiles leave behind.",
    "But you need some way to get it to stick around longer...",
    "Try bouncing the missile between the two ACEs.",
    "The next hint is the complete solution.",
    '''
objs[0].code="""
missiles = [m for m in objs if "Missile" in repr(m)]
for m in missiles:
 if m.pos[1] > 17: m.target=objs[1]
"""

objs[1].code="""
missiles = [m for m in objs if "Missile" in repr(m)]
for m in missiles:
  if m.pos[1] < 7: m.target=objs[0]
"""

objs[0].pos = (24, 20)
#It may take you a couple tries to get to the exit
'''
]

level19=Level("LEVEL 19: RANDOM ACCESS",[],gameobjs.Player((21, 22)),playlists["Calm"])

level19randomizer="""
from random import randint
def randompos():
    from random import randint
    return randint(21,59), randint(3,16)

countdown -= 1
if countdown == 0:
    countdown=50
    for theplate in plates:
        theplate.pos=randompos()

if player.injectionconsole:
    consolelocals=player.injectionconsole.locals

    consolelocals["boulderpos"]=boulder.int_pos()
    consolelocals["bouldermomentum"]=boulder.momentum
    for t, theplate in enumerate(plates):
        consolelocals["plate{0}pos".format(t+1)]=theplate.int_pos()

bx, by = boulder.pos
if 0 < by >= 17:
 boulder.pos=(24,3)
"""

gate1=gameobjs.Gate(startpos=(28, 21),open=True)
gate2=gameobjs.Gate(startpos=(27, 22))
gate3=gameobjs.Gate(startpos=(36, 22))
gate4=gameobjs.Gate(startpos=(32, 24))
pp1=gameobjs.PressurePlate(target=gate3, startpos=(56, 6))
pp2=gameobjs.PressurePlate(target=gate2, startpos=(24, 6))
pp3=gameobjs.PressurePlate(target=gate4, startpos=(39, 12))
boulder=gameobjs.Boulder(startpos=(24, 3))
level19.objlist=[gameobjs.GravPad(gravity_direction=(0, 0), range=4, startpos=(24, 14)), gameobjs.ArbitraryCodeExecutor(code='pass', startpos=(61, 3))]
level19.safe_objlist=[gameobjs.Wall(startpos=(20, 2)),
                   gameobjs.Wall(startpos=(21, 2)),
                   gameobjs.Wall(startpos=(22, 2)),
                   gameobjs.Wall(startpos=(23, 2)),
                   gameobjs.Wall(startpos=(24, 2)),
                   gameobjs.Wall(startpos=(25, 2)),
                   gameobjs.Wall(startpos=(26, 2)),
                   gameobjs.Wall(startpos=(27, 2)),
                   gameobjs.Wall(startpos=(28, 2)),
                   gameobjs.Wall(startpos=(29, 2)),
                   gameobjs.Wall(startpos=(30, 2)),
                   gameobjs.Wall(startpos=(31, 2)),
                   gameobjs.Wall(startpos=(32, 2)),
                   gameobjs.Wall(startpos=(33, 2)),
                   gameobjs.Wall(startpos=(34, 2)),
                   gameobjs.Wall(startpos=(35, 2)),
                   gameobjs.Wall(startpos=(36, 2)),
                   gameobjs.Wall(startpos=(37, 2)),
                   gameobjs.Wall(startpos=(38, 2)),
                   gameobjs.Wall(startpos=(39, 2)),
                   gameobjs.Wall(startpos=(40, 2)),
                   gameobjs.Wall(startpos=(41, 2)),
                   gameobjs.Wall(startpos=(42, 2)),
                   gameobjs.Wall(startpos=(43, 2)),
                   gameobjs.Wall(startpos=(44, 2)),
                   gameobjs.Wall(startpos=(45, 2)),
                   gameobjs.Wall(startpos=(46, 2)),
                   gameobjs.Wall(startpos=(47, 2)),
                   gameobjs.Wall(startpos=(48, 2)),
                   gameobjs.Wall(startpos=(49, 2)),
                   gameobjs.Wall(startpos=(50, 2)),
                   gameobjs.Wall(startpos=(51, 2)),
                   gameobjs.Wall(startpos=(52, 2)),
                   gameobjs.Wall(startpos=(53, 2)),
                   gameobjs.Wall(startpos=(54, 2)),
                   gameobjs.Wall(startpos=(55, 2)),
                   gameobjs.Wall(startpos=(56, 2)),
                   gameobjs.Wall(startpos=(57, 2)),
                   gameobjs.Wall(startpos=(58, 2)),
                   gameobjs.Wall(startpos=(59, 2)),
                   gameobjs.Wall(startpos=(60, 2)),
                   gameobjs.Wall(startpos=(60, 3)),
                   gameobjs.Wall(startpos=(60, 4)),
                   gameobjs.Wall(startpos=(60, 5)),
                   gameobjs.Wall(startpos=(60, 6)),
                   gameobjs.Wall(startpos=(60, 7)),
                   gameobjs.Wall(startpos=(60, 8)),
                   gameobjs.Wall(startpos=(60, 9)),
                   gameobjs.Wall(startpos=(60, 10)),
                   gameobjs.Wall(startpos=(60, 11)),
                   gameobjs.Wall(startpos=(60, 12)),
                   gameobjs.Wall(startpos=(60, 13)),
                   gameobjs.Wall(startpos=(60, 14)),
                   gameobjs.Wall(startpos=(60, 15)),
                   gameobjs.Wall(startpos=(60, 16)),
                   gameobjs.Wall(startpos=(60, 17)),
                   gameobjs.Wall(startpos=(59, 17)),
                   gameobjs.Wall(startpos=(58, 17)),
                   gameobjs.Wall(startpos=(57, 17)),
                   gameobjs.Wall(startpos=(56, 17)),
                   gameobjs.Wall(startpos=(55, 17)),
                   gameobjs.Wall(startpos=(54, 17)),
                   gameobjs.Wall(startpos=(53, 17)),
                   gameobjs.Wall(startpos=(52, 17)),
                   gameobjs.Wall(startpos=(51, 17)),
                   gameobjs.Wall(startpos=(50, 17)),
                   gameobjs.Wall(startpos=(49, 17)),
                   gameobjs.Wall(startpos=(48, 17)),
                   gameobjs.Wall(startpos=(47, 17)),
                   gameobjs.Wall(startpos=(46, 17)),
                   gameobjs.Wall(startpos=(45, 17)),
                   gameobjs.Wall(startpos=(44, 17)),
                   gameobjs.Wall(startpos=(43, 17)),
                   gameobjs.Wall(startpos=(42, 17)),
                   gameobjs.Wall(startpos=(41, 17)),
                   gameobjs.Wall(startpos=(40, 17)),
                   gameobjs.Wall(startpos=(39, 17)),
                   gameobjs.Wall(startpos=(38, 17)),
                   gameobjs.Wall(startpos=(37, 17)),
                   gameobjs.Wall(startpos=(36, 17)),
                   gameobjs.Wall(startpos=(35, 17)),
                   gameobjs.Wall(startpos=(34, 17)),
                   gameobjs.Wall(startpos=(33, 17)),
                   gameobjs.Wall(startpos=(20, 3)),
                   gameobjs.Wall(startpos=(20, 4)),
                   gameobjs.Wall(startpos=(20, 5)),
                   gameobjs.Wall(startpos=(20, 6)),
                   gameobjs.Wall(startpos=(20, 7)),
                   gameobjs.Wall(startpos=(20, 8)),
                   gameobjs.Wall(startpos=(20, 9)),
                   gameobjs.Wall(startpos=(20, 10)),
                   gameobjs.Wall(startpos=(20, 11)),
                   gameobjs.Wall(startpos=(20, 12)),
                   gameobjs.Wall(startpos=(20, 13)),
                   gameobjs.Wall(startpos=(20, 14)),
                   gameobjs.Wall(startpos=(20, 15)),
                   gameobjs.Wall(startpos=(20, 16)),
                   gameobjs.Wall(startpos=(20, 17)),
                   gameobjs.Wall(startpos=(21, 17)),
                   gameobjs.Wall(startpos=(22, 17)),
                   gameobjs.Wall(startpos=(23, 17)),
                   gameobjs.Wall(startpos=(24, 17)),
                   gameobjs.Wall(startpos=(25, 17)),
                   gameobjs.Wall(startpos=(26, 17)),
                   gameobjs.Wall(startpos=(27, 17)),
                   gameobjs.Wall(startpos=(28, 17)),
                   gameobjs.Wall(startpos=(29, 17)),
                   gameobjs.Wall(startpos=(30, 17)),
                   gameobjs.Wall(startpos=(31, 17)),
                   gameobjs.Wall(startpos=(32, 17)),
                   gameobjs.Wall(startpos=(20, 21)),
                   gameobjs.Wall(startpos=(21, 21)),
                   gameobjs.Wall(startpos=(22, 21)),
                   gameobjs.Wall(startpos=(23, 21)),
                   gameobjs.Wall(startpos=(24, 21)),
                   gameobjs.Wall(startpos=(25, 21)),
                   gameobjs.Wall(startpos=(26, 21)),
                   gameobjs.Wall(startpos=(27, 21)),
                   gameobjs.Wall(startpos=(29, 21)),
                   gameobjs.Wall(startpos=(30, 21)),
                   gameobjs.Wall(startpos=(31, 21)),
                   gameobjs.Wall(startpos=(32, 21)),
                   gameobjs.Wall(startpos=(33, 21)),
                   gameobjs.Wall(startpos=(34, 21)),
                   gameobjs.Wall(startpos=(35, 21)),
                   gameobjs.Wall(startpos=(35, 22)),
                   gameobjs.Wall(startpos=(35, 23)),
                   gameobjs.Wall(startpos=(34, 23)),
                   gameobjs.Wall(startpos=(33, 23)),
                   gameobjs.Wall(startpos=(32, 23)),
                   gameobjs.Wall(startpos=(31, 23)),
                   gameobjs.Wall(startpos=(30, 23)),
                   gameobjs.Wall(startpos=(29, 23)),
                   gameobjs.Wall(startpos=(27, 23)),
                   gameobjs.Wall(startpos=(26, 23)),
                   gameobjs.Wall(startpos=(25, 23)),
                   gameobjs.Wall(startpos=(24, 23)),
                   gameobjs.Wall(startpos=(23, 23)),
                   gameobjs.Wall(startpos=(22, 23)),
                   gameobjs.Wall(startpos=(21, 23)),
                   gameobjs.Wall(startpos=(20, 23)),
                   gameobjs.Wall(startpos=(20, 22)),
                   gameobjs.Wall(startpos=(27, 20)),
                   gameobjs.Wall(startpos=(27, 19)),
                   gameobjs.Wall(startpos=(28, 19)),
                   gameobjs.Wall(startpos=(29, 19)),
                   gameobjs.Wall(startpos=(30, 19)),
                   gameobjs.Wall(startpos=(31, 19)),
                   gameobjs.Wall(startpos=(32, 19)),
                   gameobjs.Wall(startpos=(33, 19)),
                   gameobjs.Wall(startpos=(34, 19)),
                   gameobjs.Wall(startpos=(35, 19)),
                   gameobjs.Wall(startpos=(36, 19)),
                   gameobjs.Wall(startpos=(37, 19)),
                   gameobjs.Wall(startpos=(37, 20)),
                   gameobjs.Wall(startpos=(37, 21)),
                   gameobjs.Wall(startpos=(37, 22)),
                   gameobjs.Wall(startpos=(37, 23)),
                   gameobjs.Wall(startpos=(37, 24)),
                   gameobjs.Wall(startpos=(36, 25)),
                   gameobjs.Wall(startpos=(37, 25)),
                   gameobjs.Wall(startpos=(35, 25)),
                   gameobjs.Wall(startpos=(34, 25)),
                   gameobjs.Wall(startpos=(33, 25)),
                   gameobjs.Wall(startpos=(32, 25)),
                   gameobjs.Wall(startpos=(31, 25)),
                   gameobjs.Wall(startpos=(30, 25)),
                   gameobjs.Wall(startpos=(29, 25)),
                   gameobjs.Wall(startpos=(28, 25)),
                   gameobjs.Wall(startpos=(27, 25)),
                   gameobjs.Wall(startpos=(27, 24)),
                   gameobjs.Levelporter(targetlevel='ThatWasEasy', startpos=(34, 22)),
                   gameobjs.GravPad(gravity_direction=(0, -1), range=5, startpos=(28, 26)),
                   gate2,
                   gate1,
                   gate3,
                   gate4,
                   gameobjs.PressurePlate(target=gate1, startpos=(29, 24), singleimpulse=True),
                   pp2,
                   pp1,
                   pp3,
                   boulder,
                   gameobjs.ArbitraryCodeExecutor(code=level19randomizer, startpos=(61, 3), execlocals={
                       "countdown":150,"plates":(pp1,pp2,pp3),"boulder":boulder,"player":level19.plyobj})
                   ]

level19.hints=[
    "Well, obviously the goal is to get the boulder onto each of those pressureplates. You'll note that their positions are in the console namespace, along with some other stuff.",
    "And the only way to do that is by moving the gravpad around under them. Except they keep teleporting around. Doing it manually isn't going to work.",
    "So make the ACE do it instead. Anything you add to the ACE's .execlocals will be available as a variable in the .code string, by the way. So if I want 'plate1pos' to be accessible from the .code, I just have to set objs[1].execlocals[\"plate1pos\"]=plate1pos. This just makes the name inside the executor a name for whatever plate1pos is, though; updating plate1pos in the console won't update plate1pos in the ACE.",
    "The first thing you'll need to do is figure out a way to keep the gravpad under the boulder at all times.",
    "Setting its position to (boulderpos[0],18) every tick and its range to 15 or so will do that nicely.",
    "Here's a trick: if you have a variable myvar, you can make myvar's value accessible directly (so changes outside also happen inside) with something like:\nobjs[1].execlocals['get_myvar']=lambda x: myvar\nThat'll create a function called get_myvar() in the ACE that will give you the current value of myvar in the console.",
    "Maybe try setting it up so that it sets the pad's gravity_direction to (0,0) whenever the boulder's position equals one of the plate's positions?",
    "There's a lot of different ways you can do this. If you're having trouble you might want to do as much as you can manually and keep the ACE code simple.",
    "The next hint is the complete solution.",
"""
objs[1].execlocals["boulderpos"]=lambda: boulderpos
objs[1].execlocals["tgtpos"]=lambda: plate2pos
objs[1].execlocals["bouldermomentum"]=lambda: bouldermomentum
objs[1].execlocals["g"]=objs[0]

objs[1].code=\"\"\"
def sgn(x): return x//abs(x) if x else 0
tx, ty = tgtpos()
bx, by = boulderpos()
gx, gy = bouldermomentum()
bx+=sgn(gx)
by+=sgn(by)
dx = sgn(tx - bx)
dy = sgn(ty - by)
import sys
if boulderpos() != tgtpos():
    g.pos=(bx,18)
    g.gravity_direction=(dx,dy)
    g.range=15
\"\"\"

#Walk through the door once the boulder lands on the plate. Then execute:
#objs[1].execlocals["tgtpos"]=lambda: plate1pos

#and then
#objs[1].execlocals["tgtpos"]=lambda: plate3pos
"""]


level20=Level("LEVEL 20: PENULTIMATUM",[],gameobjs.Player((32, 12)),playlists["Calm"])
redguy=gameobjs.NonPlayerCharacter("G",(32, -2),"red")
redguy.queue_many_actions([
    (redguy.move,(0,1),level20),
    (redguy.move,(0,1),level20),
    (redguy.move,(0,1),level20),
    (redguy.say,"Oh, there you are.",5,False),
    (redguy.wait,2),
    (redguy.move,(0,1),level20),
    (redguy.move,(0,1),level20),
    (redguy.move,(0,1),level20),
    (redguy.say,"Okay, I've got the guy trapped.",5,False),
    (redguy.move,(0,1),level20),
    (redguy.move,(0,1),level20),
    (redguy.wait,4),
    (redguy.say,"This is the corridor to his evil lair."),
    (redguy.say,"Pretty light security, huh?",5,False),
    (redguy.move,(0,1),level20),
    (redguy.move,(0,1),level20),
    (redguy.move,(0,1),level20),
    (redguy.wait,5),
    (redguy.move,(0,1),level20),
    (redguy.say,"But yeah, guy's holed up in there."),
    (redguy.say,"You know, my evil twin."),
    (redguy.say,"Well. Evil alternate-universe clone. You know how it is."),
    (redguy.say,"Just make sure you don't,",5,False),
    (redguy.wait,1),
    (redguy.say,"Just make sure you don't, yanno,",5,False),
    (redguy.wait,1),
    (redguy.say,"Just make sure you don't, yanno, shoot me by mistake."),
    (redguy.wait,3),
    (redguy.say,"Well, actually, the game ends when you solve this level..."),
    (redguy.say,"Yanno, 'cos the next level isn't done yet."),
    (redguy.say,"The author is totally working on it, though."),
    (redguy.say,"Actually, why don't you give him a little feedback?"),
    (redguy.say,"Guy works hard, he could use some encouragement."),
    (redguy.say,"Or, yanno, don't. That's cool too."),
    (redguy.wait,4),
    (redguy.say,"I mean, I'm just sayin'."),
    (redguy.wait,2),
    (redguy.say,"It'd help."),
    (redguy.wait,4),
    (redguy.say,"All I'm sayin'.")
])

def level19stoptheplayer(theobj):
    return theobj.graphic == "@"
cgate1=gameobjs.ConditionalObstruction(startpos=(40, 12),be_solid_if=level19stoptheplayer)

level20.objlist=[cgate1]
level20.safe_objlist=[gameobjs.Wall(startpos=(40, 11)),
                   gameobjs.Wall(startpos=(39, 11)),
                   gameobjs.Wall(startpos=(38, 11)),
                   gameobjs.Wall(startpos=(37, 11)),
                   gameobjs.Wall(startpos=(36, 11)),
                   gameobjs.Wall(startpos=(35, 11)),
                   gameobjs.Wall(startpos=(34, 11)),
                   gameobjs.Wall(startpos=(33, 11)),
                   gameobjs.Wall(startpos=(32, 11)),
                   gameobjs.Wall(startpos=(31, 11)),
                   gameobjs.Wall(startpos=(31, 12)),
                   gameobjs.Wall(startpos=(31, 13)),
                   gameobjs.Wall(startpos=(32, 13)),
                   gameobjs.Wall(startpos=(33, 13)),
                   gameobjs.Wall(startpos=(34, 13)),
                   gameobjs.Wall(startpos=(35, 13)),
                   gameobjs.Wall(startpos=(36, 13)),
                   gameobjs.Wall(startpos=(37, 13)),
                   gameobjs.Wall(startpos=(38, 13)),
                   gameobjs.Wall(startpos=(39, 13)),
                   gameobjs.Wall(startpos=(40, 13)),
                   gameobjs.Wall(startpos=(41, 13)),
                   gameobjs.Wall(startpos=(42, 13)),
                   gameobjs.Wall(startpos=(43, 13)),
                   gameobjs.Wall(startpos=(44, 13)),
                   gameobjs.Wall(startpos=(45, 13)),
                   gameobjs.Wall(startpos=(46, 13)),
                   gameobjs.Wall(startpos=(47, 13)),
                   gameobjs.Wall(startpos=(48, 13)),
                   gameobjs.Wall(startpos=(49, 13)),
                   gameobjs.Wall(startpos=(49, 12)),
                   gameobjs.Wall(startpos=(49, 11)),
                   gameobjs.Wall(startpos=(48, 11)),
                   gameobjs.Wall(startpos=(47, 11)),
                   gameobjs.Wall(startpos=(46, 11)),
                   gameobjs.Wall(startpos=(45, 11)),
                   gameobjs.Wall(startpos=(44, 11)),
                   gameobjs.Wall(startpos=(43, 11)),
                   gameobjs.Wall(startpos=(42, 11)),
                   gameobjs.Wall(startpos=(41, 11)),
                   gameobjs.AttributeEnforcer((40, 15),{cgate1:("pos",)}),
                   gameobjs.Levelporter(targetlevel='END',startpos=(48,12)),
                   redguy]
level20.hints=[
    "Oh come ON. You're seriously having trouble with this?",
    "All you have to do is set the ConditionalObstruction's be_solid_if to a function that returns False.",
    "Seriously, quit stalling.",
    "Okay, FINE. Just put in \nobjs[0].be_solid_if=lambda x: False\nThen walk through it. Then let's GET THIS OVER WITH."
]

levels={
        "Start" : level0,
        "Shortest" : level1,
        "Shmokoban" : level2,
        "Grabitty" : level3,
        "Dejavu" : level4,
        "Missiles" : level5,
        "Force" : level6,
        "DroneSpiral" : level7,
        "DroneMissile" : level8,
        "Rescue" : intermezzo1,
        "Linefinder" : level9,
        "Whoyougonnacall" : level10,
        "Run" : level11,
        "Hub" : level12,
        "Amazeing": level13,
        "Telemaze": level14,
        "Arbitrary": level15,
        "Mutex": level16,
        "Retarget": level17,
        "Lasers": level18,
        "LolRandom": level19,
        "ThatWasEasy": level20
        }
levelkeys={v.name : k for k, v in levels.items()}

"""
LEVEL IDEAS:
Something based on sorting? You have to get colored boulders somewhere in a certain sequence?
Can't work out how to make it fun

More stuff around coordinating drones could be fun... it's tedious though, maybe something best saved for level packs

Revisit the second lv17 idea /w event handlers, event system is already implemented, just unused...



PLOT OUTLINE (spoilers here obvs.)




(spoiler barrier)



















Plot thrown away, new plot TBD
Ideas:
Nearly the same as before, except base it on that npm incident a while ago;
both universe-twins let go of a project that a lot of other critical infrastructure used,
in red-guy's universe a bad guy got a hold of it and triggered a cataclysmic chain of events,
wants "revenge" on alt-universe twin
    Cool reveal, but necessitates red-guy's motivation boil down to "he's nuts," need a reason
    for red guy to want to kill goldenrod guy. Tone may be too serious, red guy has to act
    silly for us to get away with the whole "the entire game is an elaborate murder plot" thing.
    Which means first problem is inevitable no matter what we do? Don't want to push it though.
    
You're an AI being created by an AI that's gone in to singularity mode, the game is it training you
    Too "it was all a dream!"-y. Need a way for what happens in the "simulation" to really matter.
    
Red-guy-vs-other-guy again, but red guy learned some kind of DEEP ELDRICH SECRETS (explains how he 
can Tron people!) and is now travelling the multiverse killing copies of himself for... some reason?
And he wants a more efficient means of doing so, hence the player character. Still need a reason
for him to want to do that...

Think I've got it!
Red guy is an AI, goldenrod is the creator; the AI found the above mentioned DEEP ELDRICH SECRETS
and Tron'd the creator. It has safety features built in that prevent it from directly harming anyone,
so it built you to do the job for it. Everything is as in the original; the game is Red trying to
train you up to take on Goldenrod, he's the one making the puzzles and writing the hint messages,
it acts a little silly because it's literally a few months old, etc etc...

Also sets us up for the sequel hook, maybe? Goldenrod rushed the production of the AI because of the
events on the ship for IJ 2? Maybe he's on the ship, maybe he's on Earth trying to help? Then the
PC for IJ 2 is actually the same character as IJ 1; he sends the PC instead of Red to save the ship.

Then again the PC for IJ 2 kinda had to be built by one faction on the ship... maybe Goldenrod archives
PC and he gets dug up by the scientists, sorta like Simon in SOMA? 
Or PC goes wandering at the end of IJ 1 and he just rolls up to the situation in IJ 2?

Questions to be answered:
    What does Red want? What is it programmed to want / do?
    
    Why did Red make the player and not just some rube goldberg machine with as many steps as necessary to break Red's
    "first law" programming?
        Because the player is sapient and can choose not to kill, so red isn't "responsible" as far as his programming
        is concerned. Rube Goldberg plan wouldn't work.
            But he doesn't just make @ and dump him in a room with goldenrod, he's actively manipulating the player...
                "I was just joking the entire time! Don't you know what a joke is? My silly behavior clearly indicates
                that I am the kind of person who tells jokes; I need to believe these things because my programming 
                forbids me from taking an action that would harm a human being. Saying I need to believe it doesn't mean
                I don't believe it! The system works!!!"
"""
